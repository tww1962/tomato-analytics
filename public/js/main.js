var Touch=false, csrf_token = '';

$(function(){
	csrf_token = $('meta[name="csrf-token"]').attr('content');
	if(('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch){Touch=true;}
	$(document).click(function(event) {
		if ($(event.target).closest(".delivery-filter").length) return;
		else $('.popup-filter').slideUp(150);
	});
	$('.user-exit').click(function(){ $.post('/logout',{_token: csrf_token},function(){location.href='/';}); });
	$('.for-burger').click(function(){
		$('.content-left').toggleClass('active');
		$('.window-load').toggleClass('active active-menu');
	});
	$('#Dialog').click(function(e){e.stopPropagation();});
	$('#BoxModalMain').click( function(e){e.stopPropagation(); CloseBoxModalMain(); });
	$('.for-burger').click(function(){
		$('.nav').toggleClass('active');
		$('body').toggleClass('over-hidden');
		$('.burger').toggleClass('close');
		return false;
	});
	$(window).scroll(function () { 
		var ss=$(document),ssh=ss.height(),p=ss.scrollTop();ww=$(window).width();hw=$(window).height();
		/*
		if(p>=300 && !Touch && ww>1024){$('.logo','header').addClass('logo-scrol');$('header').addClass('shadow');}
		else{$('.logo','header').removeClass('logo-scrol');$('header').removeClass('shadow');}
		*/
		if(p>=300){ $('.back-top').addClass('active'); } else { $('.back-top').removeClass('active');}
	});
	$('.back-top').click(function() { $('body,html').animate({scrollTop: 0}, 300); return false;});
	$('.home-form').keydown(function(event){if(event.keyCode == 13){event.preventDefault(); return false;}});
	$(document).on('click','.Err',function(){$(this).removeClass('Err');});
	$('.home-form').submit(function(){
		let o=$(this);
		if(ValidForm(o)){
			var dataObject = o.serialize();
			$.post('/login',dataObject,function(data){
				if(!data['success']) alert(data['msg']);
				else location.reload(true);
			});
		}
		return false;
	});
	/*
    $("#draggable").draggable({revert:true,axis:'x',containment: "parent"});
    $("#droppable").droppable({
 		drop: function( event, ui ) {
			var $this=$(this),o=$this.closest('form');
			if(ValidForm(o)){
				var dataObject = o.serialize();
				FullAjax(dataObject+"&flagMain=1");
			}
		}
    });
	*/
	$('.for-password>span').click(function(){
		var $this=$(this);
		if($this.hasClass('active')) $('.for-password>input').attr('type','password');
		else $('.for-password>input').attr('type','text');
		$this.toggleClass('active');
		
	});
	$( "#delivery_date1" ).datepicker({maxDate:"+0D"});
	$( "#delivery_date2" ).datepicker({maxDate:"+0D"});
	$('#delivery_date1').change(function(){
		var $d1=$('#delivery_date1').val(),$d2=$('#delivery_date2').val(),
		date_regex = /(\d\d)\.(\d\d)\.(\d\d\d\d)/,
		d1=date_regex.exec($d1),d2=date_regex.exec($d2);		
		if(new Date(d1[3],d1[2],d1[1])>new Date(d2[3],d2[2],d2[1])){
			$('#delivery_date2').val($d1);
		}
	});
	$('#delivery_date2').change(function(){
		var $d1=$('#delivery_date1').val(),$d2=$('#delivery_date2').val(),
		date_regex = /(\d\d)\.(\d\d)\.(\d\d\d\d)/,
		d1=date_regex.exec($d1),d2=date_regex.exec($d2);		
		if(new Date(d1[3],d1[2],d1[1])>new Date(d2[3],d2[2],d2[1])){
			$('#delivery_date1').val($d2);
		}
	});
	$('.content-left').on('click','.left-menu',function(){
		let $search=location.search, $this=$(this), $href=$this.attr('href');
		if($search.length<1) return true;
		console.log($href+$search);
		location.assign($href+$search);
		//location.href=$href+$search;
		return false;
	});
	if($('.delivery').length>0){ Delivery();}
	else if($('.persona').length>0){ User();}
	else if($('.rating').length>0){ Rating();}
	else if($('.analinik').length>0){ Analitik();}
	else if($('.accounting').length>0){ Accounting();}
	else if($('.sales-promo').length>0){ Sales();}
	else if($('.proceeds').length>0){ Proceeds();}

}); //END

function Sales(){
	$('.modal-active').click(function(){
		let d1=$('#delivery_date1').val(),d2=$('#delivery_date2').val(),point=[];
		$('.sales-point').find('input:checked').each(function(){point.push($(this).data('sid'));});
		if(point.length<1){
			errorSity("<h3>Внимание!</h3><div>Период и точка продаж обязательны!</div>");
			return false;
		}
		let s = point.join('#'), $data='_token='+csrf_token+'&flagMain=1000&d1='+d1+'&d2='+d2+'&point='+s;
		$.ajax({
			url: '/sales',
			async: false,
			type: 'POST',
			data:$data,
			success: function(data){ 
				$('.modal-sales_content').html(data);
				$('#Modal_Sales').addClass('active');
			},
			error: function(o,err) { alert(err);}
		});
	});
	$('.close-modal-sales').click(function(){$('#Modal_Sales').removeClass('active');});
	$('.goto-sales').click(function(){
		let flag=0;
		$('.modal-sales_content').find('input:checked').each(function(){
			let $this=$(this),$label=$this.next('label').text(),sid=$this.data('sid');
			$('.dish-selected').append("<div data-sid='"+sid+"'><span class='del-dish'></span><span>"+$label+"</div>");
			flag=1;
		});
		$('.dish-selected').on('click','.del-dish',function(){
			let $this=$(this);
			$this.closest('div').remove();	
		});
		if(flag==1) $('#Modal_Sales').removeClass('active');
	});
	$('#find_dish').keyup(function(){
		let s=$(this).val();
		$('label','.modal-sales_content').each(function(){
			let $this=$(this),$label=$this.text().toUpperCase(),div=$this.closest('div');
			if($label.indexOf(s.toUpperCase()) == -1) div.slideUp(150);
			else  div.slideDown(150);
		});
	});
	$('.analitik-reload').click(function(){
		let dish_=$('div','.dish-selected').length,point_=$('input:checked','.sales-point').length,
			$d1=$('#delivery_date1').val(),$d2=$('#delivery_date2').val(),
			date_regex = /(\d\d)\.(\d\d)\.(\d\d\d\d)/,
			d1=date_regex.exec($d1),d2=date_regex.exec($d2);
		if(dish_==0 || point_==0){
			errorSity("<h3>Внимание!</h3><div>Точка продаж и блюдо обязательны!</div>");
			return false;
		}
		if(new Date(d1[3],d1[2],d1[1])>new Date(d2[3],d2[2],d2[1])){
			errorSity("<h3>Внимание</h3><div>Не верно указан период</div>");
			return false;
		}

		let point=[],dish=[];
		$('.sales-point').find('input:checked').each(function(){point.push($(this).data('sid'));});
		$('div','.dish-selected').each(function(){dish.push($(this).data('sid'));});
		let $data='_token='+csrf_token+'&flagMain=1001&d1='+$d1+'&d2='+$d2+'&point='+point.join('#')+'&dish='+dish.join('#');
		$.ajax({
			url: '/sales',
			async: false,
			type: 'POST',
			data: $data,
			success: function(data){ 
				$('.sales').html(data);
				$('#count_flyer').keyup(function(){
					Sales_Conversion($(this));
				});
			},
			error: function(o,err) { alert(err);}
		});
	});
}
function Sales_Conversion($this){
	let $k=1*$this.val(),count_order=1*$('#count_order').text(),c=0;
	if($k==0){
		$('#conversion').text('');
	}else{
		c=(count_order*100)/$k;
		$('#conversion').text(c.toFixed(2)+"%");
	}
	return false;
}

function Accounting(){
	$('.accounting-reload').click(function(){
		var $d1=$('#delivery_date1').val(),$d2=$('#delivery_date2').val(),
		date_regex = /(\d\d)\.(\d\d)\.(\d\d\d\d)/,
		d1=date_regex.exec($d1),d2=date_regex.exec($d2),
		point=$('#list_full_point').val();
		if(new Date(d1[3],d1[2],d1[1])>new Date(d2[3],d2[2],d2[1])){
			errorSity("<h3>Внимание</h3><div>Не верно указан период</div>");
			return false;
		}
		if(point.length<1){
			errorSity("<h3>Внимание</h3><div>Укажите точку продаж</div>");
			return false;
		}
		$('#analinik_form').submit();
	});
	$(document).on('click','.refund',function(){
		let $this=$(this),$sid=$this.data('sid'),$type=1;
		if($this.hasClass('active')) $type=0;
		FullAjax('/uchyot-i-vedenie','_token='+csrf_token+'&type='+$type+'&flagMain=100&sid='+$sid);
		//FullAjax('flagMain=100&sid='+$sid+'&type='+$type);
		$this.toggleClass('active');
	});
	$('.delivery-filter').click(function(){
		$('.popup-filter').slideToggle(300);
	});
	$('.popup-filter').click(function(e){e.stopPropagation();});
	$(document).on('click','.delivery-person',function(){
		let $this=$(this),$sid=$this.data('sid'),point=$('#list_full_point').val(),
			d1=$('#delivery_date1').val(),d2=$('#delivery_date2').val();
		FullAjax('/uchyot-i-vedenie','_token='+csrf_token+'&point='+point+'&d1='+d1+'&d2='+d2+'&flagMain=101&sid='+$sid);
	});
}

function Proceeds(){
	$('.proceeds-reload').click(function(){
		var $d1=$('#delivery_date1').val(),$d2=$('#delivery_date2').val(),
			date_regex = /(\d\d)\.(\d\d)\.(\d\d\d\d)/,
			d1=date_regex.exec($d1),d2=date_regex.exec($d2), flag=false;
		if(new Date(d1[3],d1[2],d1[1])>new Date(d2[3],d2[2],d2[1])){
			errorSity("<h3>Внимание</h3><div>Не верно указан период</div>");
			return false;
		}
		$('.delivery-point').find('input').each(function(){
			if($(this).is(':checked')) flag=true;
		});
		if(!flag){
			errorSity("<h3>Внимание</h3><div>Укажите точку продаж</div>");
			return false;
		}
		$('#analinik_form').submit();
		return false;
	});
}

function Analitik(){
	$('.analitik-reload').click(function(){
		var $d1=$('#delivery_date1').val(),$d2=$('#delivery_date2').val(),
		date_regex = /(\d\d)\.(\d\d)\.(\d\d\d\d)/,
		d1=date_regex.exec($d1),d2=date_regex.exec($d2);
		if(new Date(d1[3],d1[2],d1[1])>new Date(d2[3],d2[2],d2[1])){
			errorSity("<h3>Внимание</h3><div>Не верно указан период</div>");
			return false;
		}
		let m=[];
		$('.delivery-point').find('input').each(function(){
			if($(this).is(':checked')) m.push('point_multi[]='+$(this).val());
		});
		if(m.length>0){
			FullAjax(
				'/vsya-analitika',
				'_token='+csrf_token+"&"+m.join('&') + "&date1="+$('#delivery_date1').val()+"&date2="+$('#delivery_date2').val()
			);
		}
		return false;
	});
	/*
	$('#multi__point').change(function(){ // несколько точек
		$('.delivery-point').toggleClass('multi');
		return false;
	});
	*/
}

function Rating(){
	$('.rating-reload').click(function(){
		var $d1=$('#delivery_date1').val(),$d2=$('#delivery_date2').val(),
			date_regex = /(\d\d)\.(\d\d)\.(\d\d\d\d)/,
			d1=date_regex.exec($d1),d2=date_regex.exec($d2),
			point=$('#list_full_point').val();
		if(new Date(d1[3],d1[2],d1[1])>new Date(d2[3],d2[2],d2[1])){
			errorSity("<h3>Внимание</h3><div>Не верно указан период</div>");
			return false;
		}
		if(point.length<1){
			errorSity("<h3>Внимание</h3><div>Укажите точку продаж</div>");
			return false;
		}
		$('#analinik_form').submit();
		return false;
	});
}

function User(){
	$( '#persona_datepicker').datepicker({
		firstDay:1,
		dateFormat:'dd.mm.yy',
		changeMonth: true,
		changeYear: true,
		yearRange: "-65:-16"
	});
	$('form','.content-right').keydown(function(event){if(event.keyCode==13){event.preventDefault(); return false;}});
	$('form','.content-right').on('keyup','input', function (e) {
		var $value = $(this).val().replace(/'/g, '').replace(/"/g, '');
		$(this).val($value);
		return;
	});
	$('#fotoInput').change(function(){$('#f2').submit();});
	$('form','.content-right').submit(function(){
		let o=$(this),$id=o.attr('id');
		if($id!='f2'){
			if(ValidForm(o)) FullAjax('/personal-area',o.serialize());
		}else{
			let input = $("#fotoInput"),fd = new FormData(o.get(0));
			fd.append('photo',input.prop('files')[0]);
			$.ajax({
				url: '/personal-area',
				async: false,
				type: 'POST',
				contentType: false,
				processData: false,
				data:fd,
				dataType: 'script',
				error: function(o,err) { alert(err);}
			});		
		}
		return false;
	});
	$('.persona-menu').on('click','.persona-menu_',function(){
		let $this=$(this),$id='#form'+$this.data('sid');
		$this.addClass('active').siblings('.persona-menu_').removeClass('active');
		$($id).show(300).siblings('.persona-for-form').hide(150);
		return false;
	});
}

function Delivery(){
	$('.delivery-reload').click(function(){
		var $d1=$('#delivery_date1').val(),$d2=$('#delivery_date2').val(),
			date_regex = /(\d\d)\.(\d\d)\.(\d\d\d\d)/,
			d1=date_regex.exec($d1),d2=date_regex.exec($d2),
			point=$('#list_full_point').val();
		if(new Date(d1[3],d1[2],d1[1])>new Date(d2[3],d2[2],d2[1])){
			errorSity("<h3>Внимание</h3><div>Не верно указан период</div>");
			return false;
		}
		if(point.length<1){
			errorSity("<h3>Внимание</h3><div>Укажите точку продаж</div>");
			return false;
		}
		$('#analinik_form').submit();
		return false;
	});
	$(document).on('click','.modal',function(){
		let $this=$(this),$sid=$this.data('sid');
		FullAjax('/dostavka-i-kurery-modal','_token='+csrf_token+'&sid='+$sid);
		return false;
	});
	$('.delivery-filter').click(function(){
		$('.popup-filter').slideToggle(300);
	});
	$('.popup-filter').click(function(e){e.stopPropagation();});
}

function FullAjax($url,$mass){
	$.ajax({
		url: $url,
		async: false,
		type: 'POST',
		data: $mass,
		dataType: 'script',
		error: function(o,err) { alert(err);}
	});
}

function ValidForm( $form ){
	var flag = 0, id = $form.attr('id'),msg='';
	$form.find('input,textarea,select').each( function(){
		if($(this).attr('type') != 'file'){
			var $this = $(this),s=$.trim($this.val()),name=$this.attr('name'),
			tip=$this.attr('type'),kol,pl=$this.attr('placeholder');
			$this.removeClass('Err');
			$this.val(s);
			if( $(this).attr('required') !== undefined ) {
				if( name === 'phone' ) {
					s=s.replace(/[^0-9]/g, '');
					$this.val(s);
					if( s.length < 11 || s.length > 11 ) {
						flag++; $this.addClass('Err');
						msg += "<p>Телефон должен содержать 11 цифр</p>";
					}
				}else if( name === 'email' ) { 
					kol = s.match(/@/gi);
					kol = ( kol == null ) ? 0 : kol.length;
					if(kol == 0 || kol > 1 ) {
						flag++; $this.addClass('Err');
						msg += "<p>Проверьте E-mail</p>";
					}
				}else if( s.length < 1 ) {
					flag++; $this.addClass('Err'); 
					msg += "<p>Нет "+pl+"</p>";
				}
			}
		}
	});
	if( flag > 0 ) {
		//if(id!='M') errorSity("<h3>ВНИМАНИЕ</h3>"+msg);
		return false; 
	}
	return true;
}

function CloseBoxModalMain() {
	$("#Dialog").removeClass('active viewing');
	$("#BoxModalMain").removeClass('active'); 
	$('body').css('overflowY','scroll');
	$(".dialog").empty(); 
	return false;
};

function errorSity(msg){
	modalSity(msg);
	setTimeout(function(){CloseBoxModalMain();},5000);
	return false;
};

function modalSity(msg){
	$("#Dialog").html("<img class='close' src='/img/x.png' alt='Закрыть'/>" + msg);
	$('#Dialog').on('click','.close',function(){ CloseBoxModalMain(); });
	$("#BoxModalMain").addClass('active');
}
