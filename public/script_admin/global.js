$( function() {
	$("#lang").change(function(){
		var $val=$(this).val();
		$.post("/admin/content/ajaxglobal.php",{flag:10000000,'lang':$val},function(){ 
			location.reload(true);
			return false;
		});
	});
	$("#anons").ckeditor({height:200 });
	$("#info").ckeditor({height:500 });
	$( '#datepicker').datepicker({ firstDay:1, dateFormat:'dd.mm.yy', changeMonth: true, changeYear: true });
	$( '#datepickerPo').datepicker({ firstDay:1, dateFormat:'dd.mm.yy', changeMonth: true, changeYear: true });

	$("#dialog").dialog({ draggable: false, autoOpen:false, modal: true }); 
	$("#dialog-message").dialog({ draggable: false, autoOpen:false, modal: true, buttons: { Ok: function() { $( this ).dialog( "close" ); } } });
	$("#dialog-form").dialog({width:400, autoOpen:false, modal: true });
	$( "#dialog-confirm" ).dialog({width:400, autoOpen:false, modal: true});
	/*
	$( document ).tooltip({
		position: {
			my: "center bottom-7",
			at: "center top",
			using: function( position, feedback ) {
				$( this ).css( position );
				$( "<div>" )
				.addClass( "arrow" )
				.addClass( feedback.vertical )
				.addClass( feedback.horizontal )
				.appendTo( this );
			}
		}
	});
	*/
	$('.navPage').on('change','#kolPanig', function(){
		var kol = $(this).val(), h = $(this).data('href');
		$.post("/admin/content/ajaxglobal.php",{flag:9999, kol:kol}, function(data){ 
			window.location.href = h;
			return false;
		});
	});

	
	$(document).on('click','.slider-razdel', function(e) {
		e.stopPropagation();
		e.preventDefault();
		var s = $(this);
		if( s.hasClass('current') ) { s.next('ul').slideUp(150); s.removeClass('current'); }
		else { s.next('ul').slideDown(150); s.addClass('current'); }
	});
	$(document).on('click', '#Cropp3', function (e) { e.preventDefault(); CroppFoto.destroy(); $("#CroppFoto").html(""); $('#Cropp1').slideUp(150); });
	$(document).on('click', '#cropp', function () {
		//console.log(foto);
		var papaLi = $(this).closest('li'), sid = $(papaLi).attr('sid'), foto = $('#foto_'+sid).attr('src'), w =  $('#divfoto_'+sid).attr('data-w'), h =  $('#divfoto_'+sid).attr('data-h');
		var s = foto.split('?');
		$.post('/admin/content/croppDannSlider.php', {sid:sid, foto:s[0], w:w, h:h}, function(data){
			$('#CroppFoto').html(data);
			$('#Cropp1').slideDown(300);
		});
	});
	
	$("#sortable").sortable({
		handle: '.sort',
		items: 'li',
		opacity: .6,
		update:function(event, ui) {
			var tables = $('#sortable').data('tables'), id, list = new Array(), n = 0;
			$('li','#sortable').each(function(i){
				id = $(this).attr('sid');
				i = i + 1;
				list[n] = id + ":" + i;
				n++;
			});
			var s = list.join('#');
			$.ajax({
				url : '/admin/ajaxfull/sorttables',
				type: 'POST',
				data: { m:s, flag: 32, tables:tables,'_token':csrf_token },
				async: false
			});
		}
	});
	//.all-list-item
	$('#sortableRazdel').nestedSortable({
		forcePlaceholderSize: true,
		handle: '.sort',
		helper: 'clone',
		items: 'li',
		opacity: .6,
		placeholder: 'placeholder',
		revert: 250,
		tabSize: 20,
		tolerance: 'pointer',
		toleranceElement: '> div',
		maxLevels: $('#sortableRazdel').data('first') * 1,
		isTree: true,
		expandOnHover: 300,
		startCollapsed: true,
		update:function(event, ui) {
			var e = $(ui.item), list = new Array(), n=0, sid = e.attr('sid'), parentEach = e.parent(), 
			papaSid = parentEach.data('sid'), id = parentEach.attr('id'),
			tables = $('#sortableRazdel').data('tables');
			if(papaSid === undefined) { 
				parentEach = parentEach.parent(); papaSid = parentEach.attr('sid'); id = parentEach.attr('id'); 
			}
			$.post('/admin/ajaxfull/newparent',{flag:63, sysid:sid, parent:papaSid, tables:tables,'_token':csrf_token});
			var x = (id == "sortableRazdel") ? "#sortableRazdel>li" : "#"+id+">ul>li";
			$(x).each(function(i){
				id = $(this).attr('sid');
				i = i + 1;
				list[n] = id + ":" + i;
				n++;
			});
			var s = list.join('#');
			console.log(s);
			$.ajax({
				url : '/admin/ajaxfull/sorttables',
				type: 'POST',
				data: { m:s, flag: 32, tables:tables,'_token':csrf_token },
				async: false
			});
		}
	});
	/*
	if( $('#sortableRazdel').is('ul') ) {
			var IdLi = $('#sortableRazdel').data('curent');
			$('#li' + IdLi+' > section').addClass('current');
			$('#li' + IdLi).parents('li').each(function(){ $(this).addClass('mjs-nestedSortable-expanded').removeClass('mjs-nestedSortable-collapsed'); });
			console.log(IdLi);
			console.log('#li' + IdLi+' > section');
	}
	*/
	$(document).on('click', '.disclose', function() {
		var cc = $(this).closest('li');
		cc.toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
		return false;
	});

	$("#sortableSlider").disableSelection();
	$('.general-ul').on('click','.fa-eye', function(){ delhideFotoFile( $(this), 0 ); return false; });
	$('.general-ul').on('click','.fa-eye-slash', function(){ delhideFotoFile( $(this), 1 ); return false; });
	$('.general-ul').on('click','.fa-trash-o', function(){ delhideFotoFile( $(this), 3 ); return false; });
	
	$('.sortableRazdel').on('click','strong.ui-icon-pencil', function(){ editName( $(this) ); return false; });
	$('.sortableRazdel').on('click','.many.ui-icon-pencil', function(){ 
		var $this = $(this), sec = $this.closest('section'); ss = sec.next('div');
		ss.slideDown(150);
		return false; 
	});
	$(document).on('click','div.ui-icon-pencil', function(){ editNameAdres( $(this) ); return false; });
	$(document).on('click','strong.ui-icon-plusthick', function(){ //  дата и время афиша
		var el = $(this), id = el.data('id'), t = $(id).val(), h = $('#hour').val(), m = $('#minutes').val();
		t = ( t.length < 1 ) ? h + ':' + m : t + '#' + h + ':' + m;
		$(id).val(t);
		return false;
	});
	
	$('.CategoriesSelect').on('click','#vizible-list', function(){
		$('.CategoriesSelect').children('ul').slideToggle(150);
		$('em', $(this)).toggleClass('ui-icon-folder-collapsed').toggleClass('ui-icon-folder-open');
	});
	/*
	$(document).on('click','span.ui-icon-trash', function(){ 
		var s = $(this), sid = s.data('sid'), tables = s.data('tables');
		$( "#dialog-confirm" ).html("<h3>Удалить ФОТО?</h3>");
		$( "#dialog-confirm" ).dialog({ buttons: { 
			Нет: function() { $( "#dialog-confirm" ).dialog('close'); },
			Да: function() {
				$.post('/admin/content/ajaxglobal.php',{sid : sid, flag:2, tables : tables},function(data){ 
					$('#delFotoLogo').css('display','none');
					$('#croppLogo').css('display','none');
					$('#fotoImg').empty();
					$( "#dialog-confirm" ).dialog('close');
					$('#errSity').html(data);
				}); 
			}
		}});
		$('#dialog-confirm').dialog('open');
		return false;
	});
	/*
	var mainHeight = $('.main').height(), windowHeight = $(window).height();
	if( $('#info').is('textarea') || $('#anons').is('textarea') ) { mainHeight += 500; }
	//if( mainHeight > windowHeight ) { $('.div-for-submit').addClass('fixed'); }
	$(window).resize(function(){
		mainHeight = $('.main').outerHeight(true), windowHeight = $(window).height();
		if( mainHeight > windowHeight ) { $('.div-for-submit').addClass('fixed'); }
		else { $('.div-for-submit').removeClass('fixed'); }
	});
	*/
	
	$(document).on('click', '.xit', function() {
		var e = $(this), sid = e.closest('li').attr('sid'),tables = $('.general-ul').data('tables'),pole = e.data('pole'),znak;
		if( pole === 'type_visible' ) { 
			znak = ( e.hasClass('current') ) ? 'none' : e.data('val');
			e.siblings('em.xit-type').removeClass('current');
		} else {
			znak = ( e.hasClass('current') ) ? 0 : 1;
		}
		$.post('/admin/content/ajaxglobal.php',{flag:777,sid:sid,pole:pole,tables:tables,znak:znak},function(data) { 
			$('#errSity').html(data); 
		});
		e.toggleClass('current');
		return false;
	});
	
} );

(function() {
	
var bar = $('.bar');
var percent = $('.percent');
var status = $('#status');
   
$('#newfoto').ajaxForm({
	beforeSend: function() {
		status.empty();
		var percentVal = '0%';
		bar.width(percentVal)
		percent.html(percentVal);
	},
	uploadProgress: function(event, position, total, percentComplete) {
		var percentVal = percentComplete + '%';
		bar.width(percentVal)
		percent.html(percentVal);
		//console.log(percentVal, position, total);
	},
	success: function() {
		var percentVal = '100%';
		bar.width(percentVal)
		percent.html(percentVal);
	},
	complete: function(xhr) {
		status.html(xhr.responseText);
	}
}); 
})();

function PdfToImg(sysid, kol, name){
	var h = window.location.protocol + '//' + window.location.hostname + "/img/loading6.gif";
	$('img.close','#BoxModalMain').css('display','none');
	$('body').css('overflow', 'hidden');
	$('#Dialog').html('<h2>Идет конвертация файла. Ожидайте.</h2><p><img src="' + h + '" /></p>');
	$("#BoxModalMain").slideDown(150, function(){
		$('#BoxModalMain1').addClass('current');
	});
	setTimeout(function(){ PdfToImgConvert(sysid, kol, name); } , 300);
}
function PdfToImgConvert(sysid, kol, name){
	for( var i = 0; i < kol; i++) {
		$.ajax({
			url : '/admin/content/ajaxglobal.php',
			data: { id:sysid, nomstr:i, pdf:name, flag: 5551, kol:kol },
			async: false,
			type: 'POST'
			/*
			success: function( data ){ 
				$('#Dialog').html(data);
				setTimeout(function(){}, 300);
			}
			*/
		});
	}
	window.location.reload(true);
}

function newFoto(kolMaxFile){
	var inputFile = document.getElementById('kolFile').files, kol = inputFile.length, str = "";
	if( $('#newfoto').hasClass('newfiles') ) {
		var name = $('#nameFile').val();
		$('#nameFile').removeClass("ui-state-error");
		$('#kolFile').removeClass("ui-state-error");
		if( name.length < 1 ) { $('#nameFile').addClass("ui-state-error"); errAjax("<p>Нет названия</p>"); $('#nameFile').focus(); return false; }
		if( kol < 1 ) { $('#kolFile').addClass("ui-state-error"); errAjax("<p>Файл не выбран</p>"); return false; }
	}
	$('#NameFile').empty();
	if(kol - kolMaxFile > 0) { 
		str = "<h3 style='color:red; text-align:center;'>Превышена квота на "+(kol - kolMaxFile)
			+" файл(а) ! <br />Будут обработаны первые " + kolMaxFile +" !</h3>"; 
		$("#NameFile").append(str);
		kol = kolMaxFile;
	}
	var file = "";
	for( var i = 0; i < kol; i++) {
		file = inputFile[i];
		str = "<p style='text-align:center;'>"+file['name']+" размер "+((file['size']/1024)/1024).toFixed(2)+" Mбайт</p>";
		$("#NameFile").append(str);
	}
	$('#mask').css('display','block');
	setTimeout(function(){$('#newfoto').submit();}, 1000);
}

function closeDialog() {
	$("#dialog").dialog("close"); 
}

function kolznak1(th){
	var ost = '#kol'+$(th).attr('name');
	var zz = $(th).val().length;
	if(ost === '#kolk') { ostz = 800-(zz*1); $(ost).html("<span style='color: #fe8d00'>"+ostz+"</span>"); }
	if(ost === '#kold') { ostz = 250-(zz*1); $(ost).html("<span style='color: #fe8d00'>"+ostz+"</span>"); }
	if(ost === '#kolinfo') { ostz = 60-(zz*1); $(ost).html("<span style='color: #fe8d00'>"+ostz+"</span>"); }
}

function delhideFotoFile(e,vid) {
	var vid = vid*1, tr = e.closest('div'), sid = tr.data('sid'), id = tr.data('parent'),
	tables = $('.general-ul').data('tables'),
	msg = "<p style='text-align: center;color: #fe8d00;'>При наличии вложенных элементов<br />"
	+"действие распространится и на них</p>", msg1;
	console.log(tables);
	console.log(sid);
	console.log(vid);
	switch(vid){
		case 0: msg += "<p style='text-align: center;'>Скрыть?</p>"; break;
		case 1: msg += "<p style='text-align: center;'>Отобразить?</p>"; break;
		case 3: msg += "<p style='text-align: center;'>Удалить?</p>"; break;
	}
	$( "#dialog-confirm" ).html(msg);
	$( "#dialog-confirm" ).dialog({ buttons: { 
		Нет: function() { $( "#dialog-confirm" ).dialog('close'); },
		Да: function() { 
			var flag = ( tables === 'bis_Tag' ) ? 333 : 33;
			$.post('/admin/ajaxfull/delhideshow',{'flag':flag,'id':sid,'vid':vid,'tables':tables,'_token':csrf_token},
			function(data){
				if( vid == 3) { $('#li'+sid).remove(); msg1 = 'Удалено'; }
				else { 
					e.toggleClass('fa-eye').toggleClass('fa-eye-slash'); 
					if( vid == 0 ) {
						$('#li'+sid).find('i.fa-eye').removeClass('fa-eye').addClass('fa-eye-slash'); 
						e.attr('title','Отобразить'); msg1 = 'Скрыто'; 
					} else { 
						$('#li'+sid).find('i.fa-eye-slash').removeClass('fa-eye-slash').addClass('fa-eye'); 
						e.attr('title','Скрыть'); msg1 = 'Отображено'; 
					}
				}
				$( "#dialog-confirm" ).dialog('close');
				MessageAct(msg1);
				//$('#errSity').html(data);
			});
		}
	}});
	$('#dialog-confirm').dialog('open');
}

function editName(e){
	var tr = e.closest('li'), sid = tr.attr('sid'), msg, name = $('#name'+sid).text(), tables = $('.general-ul').data('tables');
	$('#name').removeClass("ui-state-error");
	var s = "<p><input type='text' name='name' id='name' value='" + name + "' /></p>";
	$( "#dialog-form" ).html(s);
	$("#dialog-form").dialog({ buttons: { 
		Сохранить: function() {
			name = $('#name').val();
			if( name.length < 1 ) { 
				$('#name').addClass("ui-state-error");
				$('#name').focus();
				return false;
			} else {
				$.post('/admin/content/ajaxglobal.php',{flag:51, sid:sid, name:name, tables:tables}, function(data){
					$('#errSity').html(data);
					window.location.reload(true);
				});
			}
		},
		Отмена: function() { $("#dialog-form").dialog( "close" ); }
	}});
	$("#dialog-form").dialog( "open" );
}

function newRazdelOrPages(id, tables, ForPrepend,level){
	var s = "<p><input type='text' name='namemodul' id='namemodul' placeholder='Название' onchange='translit(\"#namemodul\",\"#nameT\")' /></p>";
	s += "<p class='axtung'>Все поля обязательны для заполнения !</p>";
	s += "<input type='hidden' name='nameT' value='' id='nameT' />";
	$( "#dialog-form" ).html(s);
	$("#dialog-form").dialog({ buttons: { 
		Сохранить: function() {
			$('#namemodul').removeClass("ui-state-error");
			var name = $('#namemodul').val(), nameT = $('#nameT').val();
			if( name.length < 1 ) { 
				$('#namemodul').addClass("ui-state-error");
				$('#namemodul').focus();
			} else {
				var has = window.location.href;
				//console.log( id +' = '+ tables+' = '+ ForPrepend+' = '+level+' = '+has);
				$.post('/admin/content/ajaxglobal.php',{flag:6, parent:id, name:name, nameT:nameT, tables:tables, has:has, level:level}, function(data){
					if( tables == 'bis_categories' ) {
						$(ForPrepend).prepend(data);
						$("#dialog-form").dialog( "close" );
					} else {
						//$('#errSity').html(data);
						window.location.reload(true);
					}
				});
			}
		},
		Отмена: function() { $("#dialog-form").dialog( "close" ); }
	}});
	$("#dialog-form").dialog( "open" );
}

function MessageAct(msg) {
	$("#MessageAct").html('');
	$("#MessageAct").append("<p>"+msg+"</p>").slideDown(150);
	setTimeout(function(){$("#MessageAct").slideUp(150);}, 2000);
}

function editNameAdres(e){
	var tr = e.closest('li'), sid = tr.attr('sid'), tables = "bis_adres";
	$.post('/admin/content/ajaxFeature.php',{flag:5, sid:sid, tables:tables}, function(data){
			
		$( "#dialog-form" ).html(data);
		$("#dialog-form").dialog({ buttons: { 
			Сохранить: function() {
				var name = $('#name').val(), district = $('#district').val();
				if( name.length < 1 ) { 
					$('#name').addClass("ui-state-error");
					$('#name').focus();
					return false;
				} else {
					$.post('/admin/content/ajaxFeature.php',{flag:51, sid:sid, name:name, district:district, tables:tables}, function(data){
						$('#errSity').html(data);
						window.location.reload(true);
					});
				}
			},
			Отмена: function() { $("#dialog-form").dialog( "close" ); }
		}});
		$("#dialog-form").dialog( "open" );
		
	});
}

function closeModal(){
	$("#BoxModalMain").removeClass('current').removeClass('for-price-photo'); 
	$('#Dialog').removeClass('for-price-photo').html('');
	$('body').css('overflow', 'auto');
	return false;
}
function modalSity(msg){
	$("#Dialog").html(msg);
	$('body').css('overflow', 'hide');
	$('#BoxModalMain').addClass('current');
}
