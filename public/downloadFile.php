<?php
$ff = $_GET['filename'];
$name = $_GET['name'];
$filename = $_SERVER['DOCUMENT_ROOT']."/files/doc/".basename($ff);
if (file_exists($filename)) {
    if (ob_get_level()) {
      ob_end_clean();
    }
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header("Content-Disposition: attachment; filename=\"$name-".basename($ff)."\"");
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($filename));
    readfile($filename);
    exit;
}
?>
