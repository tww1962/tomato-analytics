<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{

    public function index(Request $request){
        if (Auth::check()) return view('home');
        else return view('loginform');
    }

    public function loginUser(Request $request){
        $validator = Validator::make($request->all(), ['name' => 'required|email','password' => 'required',]);
        if ($validator->fails()){ return response()->json(['success' => false,'msg' => 'Проверьте логин или пароль']); }
        
        $user = User::where('email', $request->name)
            ->whereAnd('password',$request->password)
            ->whereAnd('hide','show')->first();
        if ($user !== null) {
            Auth::loginUsingId($user->sysid,$remember = true);
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false,'msg' => 'Проверьте логин или пароль']);
    }

	public function logout(Request $request){
		Auth::logout();
		$request->session()->flush();
		$request->session()->regenerate();
		//return redirect('/');
	}

}
