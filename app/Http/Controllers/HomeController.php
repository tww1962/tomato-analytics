<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    static $user;

	function glMenu($user){  //  главное меню
		$ss = "";
		$m = explode('#',$user->type_user);
		$ctg = DB::select("SELECT * FROM bis_cmsmenu WHERE hide='show' && parent=0 && vidMenu='hor' && sysid!=1 ORDER BY pos");
		foreach($ctg as $cat){
			if(!in_array($cat->sysid,$m)) continue;
			$name = self::DecodeTitle($cat->name);
			$tr = "/".$cat->script;
			$ss .= "<a href='$tr' data-title='$name' class='nav_".$cat->sysid." left-menu' >$name</a>";
			if($cat->sysid==5) $ss .= "<div class='left-line'></div>";
		}
		return "<section class='content-left'><a href='/'><img src='/img/logo.png?07_09_2021' class='logo' alt='Томат и Чеддр' /></a>"
		.$ss . self::User($user) ."</section>";
	}

	function User($user){
		$ss = "";
		$bg=self::Background($user->sysid);
		$ss .= "<section class='user-dann flex flex-align-start flex-justify-end'>";
			$ss .= "<div class='user-info'>";
				$ss .= "<h4>".self::DecodeTitle($user->name)
					." ".self::DecodeTitle($user->surname)."</h4>";
				$ss .= "<h5>".self::DecodeTitle($user->nik)."</h5>";
			$ss .= "</div>";
			$ss .= "<div class='user-ico'>";
				$ss .= "<div class='user-logo bg-cover' style='$bg'></div>";
				$ss .= "<div class='user-lk-exit flex flex-justify-between'>"
					."<a href='/personal-area' class='user-lk'><span class='icon-cog'></span></a>"
					."<div class='user-exit'><span class='icon-logout-1'></span></div>"
					."</div>";
			$ss .= "</div>";
		$ss .= "</section>";
		return $ss;
	}

	public function DecodeName($ss){ return htmlspecialchars_decode(str_replace('"',"&quot;",$ss),ENT_QUOTES);}
	public function DecodeTitle($text){
		$text = htmlspecialchars_decode($text,ENT_QUOTES);
		return str_replace('"','»',preg_replace('/((^|\s)"(\w))/um', '\2«\3',$text));
	}

    function Background($sysid,$table='bis_pagesLogoAll'){
        $bg=['',''];
		if($table=='bis_galfoto'){
			$sort = "&& hide='show' ORDER BY pos";
			$pole = "foto";
            $path = 'files/small';
		}else{
			$sort = "";
			$pole = "photo";
            $path = 'files/logo';
		}
		$ctg = DB::select("SELECT $pole FROM $table WHERE parent=$sysid $sort LIMIT 1");
		if( !empty($ctg) ) {
			$cat = (array)$ctg[0];
			if(file_exists(public_path($path) ."/". $cat[$pole]) && !empty($cat[$pole])) {
				$size = getimagesize(public_path($path) ."/". $cat[$pole]);
				$bg[0] = "background-image:url(/$path/". $cat[$pole] .");";
				$bg[1] = $size[0];
			}
		}
		return $bg[0];
    }
	public function Sprintf_Time($time){
		return sprintf('%02d:%02d:%02d',$time/3600,($time%3600)/60,($time%3600)%60);
	}

	public function session_param_put($m){
		session(['param' => json_encode($m)]);
	}

	public function ActiveMenu($id){
		return "<script>$(function(){ $('.nav_$id').addClass('current'); });</script>";
	}
} //END
