<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HomeController as General;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use DateTime;

class RatingController extends Controller
{
    public static $user;
    private static $Data_Report;
    private static $Sum_Time_Dat;
    private static $IdListOrg;
    private static $ArrayListOrg;
    private static $Sum_Time;
    private static $List_Courier;
    private static $Courier_Get;
	private static $One_Courier;
    private static $Delivery_Ok;
    private static $Dat;
    public static $Late_Min=[0,60,-1];
    private static $QUERYSTRING;
    public static $Late_Where;
    public static $Late_Where_Null;
    private static $sek_Kitchen;

    public function index(Request $request){
        if(isset($request->point_multi)){
            $m = (array)$request->all();
            unset($m['_token']);
            self::$QUERYSTRING = http_build_query($m);
            self::$ArrayListOrg = (array)$request->point_multi;
            if(count(self::$ArrayListOrg) == 1) General::session_param_put($m);
        }
        self::$user = Auth::user();
        $cmsmenu=DB::select("SELECT * FROM bis_cmsmenu WHERE script='rejting-kurerov' LIMIT 1");
		$point = DB::select("SELECT * FROM `Organizations` WHERE Conception='ТиЧ' && "
            ."Id in(SELECT id FROM userlist_city WHERE parent=". self::$user->sysid .") ORDER BY OrganizationName");
        return view('rating',['user'=>self::$user,'cmsmenu'=>(array)$cmsmenu[0],'points'=>$point]); 
    }
	/*
    public function new_query(Request $request){
        $m = (array)$request->all();
        unset($m['_token']);
        self::$QUERYSTRING = http_build_query($m);
        self::$ArrayListOrg = (array)$request->point_multi;
        if(count(self::$ArrayListOrg) == 1) General::session_param_put($m);
        return "location.reload(true);";
	}
	*/
	public function Rating_Left_Courier($point,$d1,$d2,$courier){ //Courier_Id
		$d1=implode("-",array_reverse(explode(".",$d1)));
		$d2=implode("-",array_reverse(explode(".",$d2)));
		self::Data_Report($d1,$d2);
		$Any_Courier = empty($courier) ? "" : " && Courier_Id='$courier'";
		$query="SELECT * FROM `OrderTEMP` WHERE "
			."expectedDeliverTime BETWEEN STR_TO_DATE('".self::$Data_Report[0]."','%Y-%m-%d %H:%i:%s') AND "
			."STR_TO_DATE('".self::$Data_Report[1]."','%Y-%m-%d %H:%i:%s') && isCafe=0 && isCanceled=0 && "
			."organizationId='$point' && isClientDelivery=0 $Any_Courier "
			."&& deliveredDateTime IS NOT NULL && deliveredDateTime!='0000-00-00 00:00:00' && "
			."Courier_Name IS NOT NULL ORDER BY expectedDeliverTime";
		$ctg = DB::select($query);
		$ss = $name = "";
		foreach($ctg as $cat1){
			$cat = (array)$cat1;
			$name = General::DecodeTitle($cat['Courier_Name']);
			$d = date('d.m.Y',strtotime($cat['expectedDeliverTime']));
			if(!isset(self::$One_Courier["$d#{$cat['Courier_Id']}"])) 
				self::$One_Courier["$d#{$cat['Courier_Id']}"]=[0,0,""];
			self::$One_Courier["$d#{$cat['Courier_Id']}"][0]+=1;
			self::$One_Courier["$d#{$cat['Courier_Id']}"][1]+=strtotime($cat['deliveredDateTime'])-strtotime($cat['sendTime']);
			self::$One_Courier["$d#{$cat['Courier_Id']}"][2] = $name;
		}
		foreach(self::$One_Courier as $d=>$m){
			$d1=explode('#',$d);
			$ss .= "<div class='rating-tr'>";
				$ss .= "<div class='rating-td'>{$d1[0]}</div>";
				$ss .= "<div class='rating-td'>{$m[2]}</div>";
				$ss .= "<div class='rating-td'>{$m[0]}</div>";
				$m[1]=round($m[1]/$m[0],0);
				$znak=$m[1]<0?"-":"";
				$t=abs($m[1]);
				$ss .= "<div class='rating-td'>$znak". General::Sprintf_Time($t)."</div>";
			$ss .= "</div>";
		}
		return $ss;
	}

	public function Rating_Rezult_Right($checked,$d1,$d2,$courier){
		$ss = "";
		$k=1;
		arsort(self::$List_Courier['count_order']);
		foreach(self::$List_Courier['count_order'] as $key=>$val){
			//if(is_null($val)) continue;
			$sek=self::$List_Courier['count'][$key];
			$znak=$sek<0?"-":"";
			$t=abs($sek);
			$ss .= "<div><span>". self::$List_Courier['name'][$key] ."</span>"
			."<span>$val&nbsp;|&nbsp;"
			."$znak". General::Sprintf_Time($t) ."</span></div>";
			$k++;
			//if($k==11) break;
		}
		return $ss;
	}


    function Rating_Courier_List($point,$d1,$d2,$courier){
        if(empty($point)) return "";
        self::$Courier_Get = $courier;
		$ss = "";
		self::$List_Courier['name']=self::$List_Courier['id']=self::$List_Courier['count']=self::$List_Courier['count_order']=[];
		$d1=implode("-",array_reverse(explode(".",$d1)));
		$d2=implode("-",array_reverse(explode(".",$d2)));
		self::Data_Report($d1,$d2);
		$query="SELECT Courier_Name,Courier_Id,count(Courier_Id) as count,"
		."sum(timestampdiff(SECOND,`sendTime`,`deliveredDateTime`)) as sek FROM `OrderTEMP` WHERE "
		."expectedDeliverTime BETWEEN STR_TO_DATE('".self::$Data_Report[0]."','%Y-%m-%d %H:%i:%s') AND "
		."STR_TO_DATE('".self::$Data_Report[1]."','%Y-%m-%d %H:%i:%s') && isCafe=0 && isCanceled=0 && "
		."organizationId='$point' && isClientDelivery=0 && "
		."deliveredDateTime IS NOT NULL && deliveredDateTime!='0000-00-00 00:00:00' && "
		."Courier_Name IS NOT NULL GROUP BY Courier_Id";
		$ctg = DB::select($query);
		foreach($ctg as $cat){
			$name = General::DecodeTitle($cat->Courier_Name);
			self::$List_Courier['name'][] = $name;
			self::$List_Courier['id'][] = $cat->Courier_Id;
			self::$List_Courier['count'][] = is_null($cat->count) ? "===" : round($cat->sek/$cat->count,0);
			self::$List_Courier['count_order'][] = is_null($cat->count) ? "0" : $cat->count;
		}
		natsort(self::$List_Courier['name']);
		foreach(self::$List_Courier['name'] as $key=>$val){
			$selected = self::$List_Courier['id'][$key]==self::$Courier_Get ? "selected" : "";
			$ss .= "<option value='". self::$List_Courier['id'][$key] ."' $selected >$val</option>";
		}
		if(!empty($ss)) return "<select name='courier' id='rating_courier_list'><option value='0'>Все</option>$ss</select>";
		return "";
	}

	public function Data_Report($d1,$d2){
		self::$Data_Report[0] = date("Y-m-d H:i:s",strtotime("$d1 00:00:00"));
		self::$Data_Report[1] = date("Y-m-d H:i:s",strtotime("$d2 23:59:59"));
	}

} //END
