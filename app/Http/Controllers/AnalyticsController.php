<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HomeController as General;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use DateTime;

class AnalyticsController extends Controller
{
    public static $user;
    private static $Data_Report;
    private static $Sum_Time_Dat;
    private static $IdListOrg;
    private static $ArrayListOrg;
    private static $Sum_Time;
    private static $List_Courier;
    private static $Delivery_Ok;
    private static $Dat;
    private static $Late_Min=[0,60,-1];
    private static $QUERYSTRING;

    public function index(){
        self::$user = Auth::user();
        $cmsmenu=DB::select("SELECT * FROM bis_cmsmenu WHERE script='vsya-analitika' LIMIT 1");
		$point = DB::select("SELECT * FROM `Organizations` WHERE Conception='ТиЧ' && "
            ."Id in(SELECT id FROM userlist_city WHERE parent=". self::$user->sysid .") ORDER BY OrganizationName");
        return view('analitik',['user'=>self::$user,'cmsmenu'=>(array)$cmsmenu[0],'points'=>$point]); 
    }

    public function show(Request $request){
        $m = (array)$request->all();
        unset($m['_token']);
        self::$QUERYSTRING = http_build_query($m);
        self::$ArrayListOrg = (array)$request->point_multi;
        if(count(self::$ArrayListOrg) == 1) General::session_param_put($m);
        self::$IdListOrg = implode("','",self::$ArrayListOrg);
		self::$Dat[0]=$request->date1;
		self::$Dat[1]=$request->date2;
		return self::show_dann(1);
	}
    public function show_reload($param){
        self::$QUERYSTRING = http_build_query($param);
        self::$ArrayListOrg =$param['point_multi'];
		if(is_array(self::$ArrayListOrg)) self::$IdListOrg = implode("','",self::$ArrayListOrg);
		else self::$IdListOrg = self::$ArrayListOrg;
		self::$Dat[0]=$param['date1'];
		self::$Dat[1]=$param['date2'];
		return self::show_dann(0);
	}
	public function show_dann($ajax){
		$d1=implode("-",array_reverse(explode(".",self::$Dat[0])));
		$d2=implode("-",array_reverse(explode(".",self::$Dat[1])));
		self::Data_Report($d1,$d2);
		self::$Sum_Time_Dat[0]=new DateTime(self::$Data_Report[0]);
		self::$Sum_Time_Dat[1]=new DateTime(self::$Data_Report[1]);
		self::$Sum_Time_Dat[2]=new DateTime(self::$Data_Report[0]);
		self::$Sum_Time_Dat[3]=new DateTime(self::$Data_Report[1]);
		$interval = self::$Sum_Time_Dat[3]->diff(self::$Sum_Time_Dat[2]);
		$day = ($interval->d<=7)?7:$interval->d;
		self::$Sum_Time_Dat[2]->modify("-$day day");
		self::$Sum_Time_Dat[3]->modify("-$day day");
        $Result = self::Analitik_Rating_Courier();
        $ss = "";
        if(empty($Result)){
            $ss .= "<div class='analitik-rezult' style='padding:0 10px;margin-bottom:40px'>Нет данных</div>";
        }else{
            //$ss .= "<div class='analitik-rezult flex flex-wrap analitik-rezult-ajax'>"
            $ss .= "<div class='analitik-block'>$Result</div>"
            ."<div class='analitik-block'>".self::Analitik_Delivery()."</div>"
            ."<div class='analitik-block'>".self::Analitik_Kitchen()."</div>"
            ."<div class='analitik-block'>".self::Analitik_Delivery_Order()."</div>"
            ."<div class='analitik-block'>".self::Analitik_Delivery_Ok()."</div>";
            //."</div>";
        }
		if($ajax) return "$('.analitik-rezult-ajax').html(\"$ss\");";
		else return $ss;
    }

	public function accumulatively($points){ // накопительно с 1-го каждого месяца
        $ss = "";
		$day=(int)date('j');
		$date = new DateTime();
		if($day>1){
			$d1=$date->format('Y-m-01');
			$d1_=$date->format('01.m.Y');
			$date->modify('-1 day');
			$d2=$date->format('Y-m-d');
			$d2_=$date->format('d.m.Y');
		}else{
			$date->modify('-1 month');
			$d1=$date->format('Y-m-01');
			$d1_=$date->format('01.m.Y');
			$d2=$date->format('Y-m-t');
			$d2_=$date->format('t.m.Y');
		}
        $org = array_column($points, 'OrganizationName');
        $ss .= "<strong>Данные за период с $d1_ по $d2_<br />".implode(", ",$org)."</strong>";
		self::Data_Report($d1,$d2);
		self::$Sum_Time_Dat[0]=new DateTime(self::$Data_Report[0]);
		self::$Sum_Time_Dat[1]=new DateTime(self::$Data_Report[1]);
		self::$Sum_Time_Dat[2]=new DateTime(self::$Data_Report[0]);
		self::$Sum_Time_Dat[3]=new DateTime(self::$Data_Report[1]);
		self::$Sum_Time_Dat[2]->modify("-1 month");
		self::$Sum_Time_Dat[3]->modify("-1 month");
		self::$IdListOrg = implode("','",array_column($points,'Id'));
		$Result = self::Analitik_Rating_Courier();
		$ss .= "<div class='analitik-rezult flex flex-wrap'>"
			."<div class='analitik-block'>".self::Analitik_Delivery(1)."</div>"
			."<div class='analitik-block'>".self::Analitik_Kitchen()."</div>"
			."<div class='analitik-block'>".self::Analitik_Delivery_Order()."</div>"
			."<div class='analitik-block'>".self::Analitik_Delivery_Ok(1)."</div>"
			."</div>";
        return $ss;
	}

	private function Analitik_Delivery($href=0){
		self::Analitik_Sum_Delivery();
		$delta="";
		if(self::$Sum_Time['count'][1]!=0 && self::$Sum_Time['count'][0]!=0){
			$procent=100 * (self::$Sum_Time['sek'][1]/self::$Sum_Time['count'][1]);
			$procent=round($procent/(self::$Sum_Time['sek'][0]/self::$Sum_Time['count'][0]),0);
			$delta="&nbsp;&nbsp;&nbsp;<span class='procent'>".(100-$procent)."%</span>";
		}
		$ss = "";
		$ss .= "<div class='analitik-block-content'>";
			$ss .= "<h3>Среднее время доставки курьером</h3>";
			$ss .= "<p>С ".self::$Sum_Time_Dat[0]->format('d.m.Y') 
				." по ".self::$Sum_Time_Dat[1]->format('d.m.Y')."$delta</p>";
			$ss .= self::Analitik_Time_Print(0);
			$ss .= "<p>В сравнении с ".self::$Sum_Time_Dat[2]->format('d.m.Y')
				." по ".self::$Sum_Time_Dat[3]->format('d.m.Y')."</p>";
			$ss .= self::Analitik_Time_Print(1);
		$ss .= "</div>";
        
        if(is_array(self::$ArrayListOrg) && count(self::$ArrayListOrg) == 1 && $href==0){
            $result = DB::select("SELECT script FROM bis_cmsmenu WHERE sysid=3 LIMIT 1");
            $ss .= "<a href='/".$result[0]->script."?".self::$QUERYSTRING."' class='analitik-button'>Подробнее</a>";
        }
		return $ss;
	}

	private function Analitik_Sum_Delivery(){
		// expectedDeliverTime на createTime
		$query="SELECT count(Courier_Id) as count,"
		."sum(timestampdiff(SECOND,`sendTime`,`deliveredDateTime`)) as sek,"
		."sum(timestampdiff(SECOND,`createTime`,`deliveredDateTime`)) as sek1,"
		."sum(TIME_TO_SEC(totalKitchenTime)) sek2 "
		."FROM `OrderTEMP` WHERE isClosest=1 && "
		."expectedDeliverTime BETWEEN STR_TO_DATE('".
			self::$Sum_Time_Dat[2]->format('Y-m-d H:i:s')."','%Y-%m-%d %H:%i:%s') AND "
		."STR_TO_DATE('"
			.self::$Sum_Time_Dat[3]->format('Y-m-d H:i:s')."','%Y-%m-%d %H:%i:%s') "
		."&& isCafe=0 && isCanceled=0 && organizationId in('". self::$IdListOrg ."') && isClientDelivery=0 && "
		."deliveredDateTime IS NOT NULL && deliveredDateTime!='0000-00-00 00:00:00' && "
		."sendTime IS NOT NULL && sendTime!='0000-00-00 00:00:00' && "
		."Courier_Name IS NOT NULL && Courier_Id IS NOT NULL GROUP BY Courier_Id";
		$ctg = DB::select($query);
		foreach($ctg as $cat){
			self::$Sum_Time['sek'][1] += $cat->sek; 
			self::$Sum_Time['count'][1] += $cat->count;
			self::$Sum_Time['sek'][3] += abs($cat->sek1); //от оформления до доставки
			self::$Sum_Time['count'][3] += $cat->count;
			self::$Sum_Time['sek'][5] += $cat->sek2; //время кухни
			self::$Sum_Time['count'][5] += $cat->count;
			self::$Sum_Time['sek'][7] += $cat->sek2 + $cat->sek; //время кухни+время курьера
			self::$Sum_Time['count'][7] += $cat->count;
		}
	}

	private function Analitik_Rating_Courier(){
		// expectedDeliverTime на createTime
		self::$Sum_Time['sek']=self::$Sum_Time['count']=[0,0,0,0,0,0,0,0];
		$ss = "";
		self::$List_Courier['name']=self::$List_Courier['id']=self::$List_Courier['count']=self::$List_Courier['count_order']=[];
		$query="SELECT Courier_Name,Courier_Id,count(Courier_Id) as count,"
		."sum(timestampdiff(SECOND,`sendTime`,`deliveredDateTime`)) as sek,"
		."sum(timestampdiff(SECOND,`createTime`,`deliveredDateTime`)) as sek1,"
		."sum(TIME_TO_SEC(totalKitchenTime)) sek2 "
		."FROM `OrderTEMP` WHERE isClosest=1 && "
		."expectedDeliverTime BETWEEN "
		."STR_TO_DATE('".self::$Sum_Time_Dat[0]->format('Y-m-d H:i:s')."','%Y-%m-%d %H:%i:%s') AND "
		."STR_TO_DATE('".self::$Sum_Time_Dat[1]->format('Y-m-d H:i:s')."','%Y-%m-%d %H:%i:%s') "
		."&& isCafe=0 && isCanceled=0 && "
		."organizationId in('". self::$IdListOrg ."') && isClientDelivery=0 && "
		."deliveredDateTime IS NOT NULL && deliveredDateTime!='0000-00-00 00:00:00' && "
		."sendTime IS NOT NULL && sendTime!='0000-00-00 00:00:00' && "
		."Courier_Name IS NOT NULL && Courier_Id IS NOT NULL GROUP BY Courier_Id";
		$ctg = DB::select($query);
		foreach($ctg as $cat1){
            $cat = (array)$cat1;
			$name= General::DecodeTitle($cat['Courier_Name']);
			self::$List_Courier['name'][]=$name;
			self::$List_Courier['id'][]=$cat['Courier_Id'];
			self::$List_Courier['count'][]=is_null($cat['count'])?"===":round($cat['sek']/$cat['count'],0);
			self::$List_Courier['count_order'][]=is_null($cat['count'])?"0":$cat['count'];
			self::$Sum_Time['sek'][0] += $cat['sek'];
			self::$Sum_Time['count'][0] += $cat['count'];
			self::$Sum_Time['sek'][2] += abs($cat['sek1']); //от оформления до доставки
			self::$Sum_Time['count'][2] += $cat['count'];
			self::$Sum_Time['sek'][4] += $cat['sek2']; //время кухни
			self::$Sum_Time['count'][4] += $cat['count'];
			self::$Sum_Time['sek'][6] += $cat['sek2']+$cat['sek']; //время кухни+время курьера
			self::$Sum_Time['count'][6] += $cat['count'];
		}
		$k=1;
		arsort(self::$List_Courier['count_order']);
		foreach(self::$List_Courier['count_order'] as $key=>$val){
			//if(is_null($val)) continue;
			$sek=self::$List_Courier['count'][$key];
			$znak=$sek<0?"-":"";
			$t=abs($sek);
			$ss .= "<div class='space-between'><span>". self::$List_Courier['name'][$key] ."</span><span>$val&nbsp;|&nbsp;"
			    ."$znak".General::Sprintf_Time($t)."</span></div>";
			$k++;
			//if($k==11) break;
		}
		if(empty($ss)) return "";
        if(is_array(self::$ArrayListOrg) && count(self::$ArrayListOrg) == 1){
            $result = DB::select("SELECT script FROM bis_cmsmenu WHERE sysid=4 LIMIT 1");
            $ss .= "<a href='/".$result[0]->script."?".self::$QUERYSTRING."' class='analitik-button'>Подробнее</a>";
        }
        return "<div class='analitik-block-content'><h3>Рейтинг курьеров</h3>$ss</div>";
	}

	private function Analitik_Time_Print($n){
		if(self::$Sum_Time['count'][$n]>0){
			$znak=self::$Sum_Time['sek'][$n]<0?"-":"";
			$t=abs(self::$Sum_Time['sek'][$n])/self::$Sum_Time['count'][$n];
			return "<div><strong>$znak".sprintf('%02d',$t/3600)."</strong>&nbsp;час&nbsp;"
				."<strong>".sprintf('%02d',($t%3600)/60)."</strong>&nbsp;мин&nbsp;"
				."<strong>".sprintf('%02d',($t%3600)%60)."</strong>&nbsp;сек&nbsp;"
				."</div>";
		}else{
			return "<div><strong>==</strong>&nbsp;час&nbsp;"
				."<strong>==</strong>&nbsp;мин&nbsp;"
				."<strong>==</strong>&nbsp;сек&nbsp;"
				."</div>";
		}
	}
	public function Data_Report($d1,$d2){
		self::$Data_Report[0] = date("Y-m-d H:i:s",strtotime("$d1 00:00:00"));
		self::$Data_Report[1] = date("Y-m-d H:i:s",strtotime("$d2 23:59:59"));
	}

	private function Analitik_Kitchen(){
		if(self::$Sum_Time['count'][4]==0) return "Данных нет";
		$delta="";
		if(self::$Sum_Time['count'][5]!=0){
			$procent=100 * (self::$Sum_Time['sek'][5]/self::$Sum_Time['count'][5]);
			$procent=round($procent/(self::$Sum_Time['sek'][4]/self::$Sum_Time['count'][4]),0);
			$delta="&nbsp;&nbsp;&nbsp;<span class='procent'>".(100-$procent)."%</span>";
		}
		$ss = "";
		$ss .= "<h3>Среднее время кухни</h3>";
		$ss .= "<p>С ".self::$Sum_Time_Dat[0]->format('d.m.Y') 
			." по ".self::$Sum_Time_Dat[1]->format('d.m.Y')."$delta</p>";
		$ss .= self::Analitik_Time_Print_Kitchen(4);
		$ss .= "<p>В сравнении с ".self::$Sum_Time_Dat[2]->format('d.m.Y')
			." по ".self::$Sum_Time_Dat[3]->format('d.m.Y')."</p>";
		$ss .= self::Analitik_Time_Print_Kitchen(5);
		return "<div class='analitik-block-content'>$ss</div>";
	}
	private function Analitik_Time_Print_Kitchen($n){
		if(self::$Sum_Time['count'][$n]>0){
			$znak=self::$Sum_Time['sek'][$n]<0?"-":"";
			$t=abs(self::$Sum_Time['sek'][$n])/self::$Sum_Time['count'][$n];
			return "<div><strong>$znak".sprintf('%02d',$t/3600)."</strong>&nbsp;час&nbsp;"
				."<strong>".sprintf('%02d',($t%3600)/60)."</strong>&nbsp;мин&nbsp;"
				."<strong>".sprintf('%02d',($t%3600)%60)."</strong>&nbsp;сек&nbsp;"
				."</div>";
		}else{
			return "<div><strong>==</strong>&nbsp;час&nbsp;"
				."<strong>==</strong>&nbsp;мин&nbsp;"
				."<strong>==</strong>&nbsp;сек&nbsp;"
				."</div>";
		}
	}

	private function Analitik_Delivery_Order(){
		$delta="";
		if(self::$Sum_Time['count'][2]==0) return "Данных нет";
		if(self::$Sum_Time['count'][3]!=0){
			$procent=100 * (self::$Sum_Time['sek'][3]/self::$Sum_Time['count'][3]);
			$procent=round($procent/(self::$Sum_Time['sek'][2]/self::$Sum_Time['count'][2]),0);
			$delta="&nbsp;&nbsp;&nbsp;<span class='procent'>".(100-$procent)."%</span>";
		}
		$ss = "";
		$ss .= "<div class='analitik-block-content'>";
			$ss .= "<h3>Среднее время выполнения заказа</h3>";
			$ss .= "<p>С ".self::$Sum_Time_Dat[0]->format('d.m.Y') 
				." по ".self::$Sum_Time_Dat[1]->format('d.m.Y')."$delta</p>";
			$ss .= self::Analitik_Time_Print(2);
			$ss .= "<p>В сравнении с ".self::$Sum_Time_Dat[2]->format('d.m.Y')
				." по ".self::$Sum_Time_Dat[3]->format('d.m.Y')."</p>";
			$ss .= self::Analitik_Time_Print(3);
		$ss .= "</div>";
		return $ss;
	}

	private function Analitik_Delivery_Ok($href=0){
		self::$Delivery_Ok=[0,0,0,0,0];
		self::Analitik_Delivery_Ok_Sum();
		$ss = "";
		$ss .= "<h3>Хорошие доставки</h3>";
		//$ss .= "<p>С {self::$Dat[0]} по {self::$Dat[1]}</p>";
		$ss .= "<div class='space-between'><span>Всего</span><span>". self::$Delivery_Ok[0] ."</span></div>";
		
		$pr = round(self::$Delivery_Ok[1]*100/self::$Delivery_Ok[0],0);
		$ss .= "<div class='space-between'><span>Без опоздания</span>"
			."<span>". self::$Delivery_Ok[1] ."&nbsp;|&nbsp;$pr%</span></div>";
			
		$pr = round(self::$Delivery_Ok[2]*100/self::$Delivery_Ok[0],0);
		$ss .= "<div class='space-between'><span>Опоздание +".self::$Late_Min[0]."/".self::$Late_Min[1]."</span>"
			."<span>". self::$Delivery_Ok[2] ."&nbsp;|&nbsp;$pr%</span></div>";
			
		$pr = round(self::$Delivery_Ok[3]*100/self::$Delivery_Ok[0],0);
		$ss .= "<div class='space-between'><span>Опоздание +".self::$Late_Min[1]."</span>"
			."<span>". self::$Delivery_Ok[3] ."&nbsp;|&nbsp;$pr%</span></div>";
			
		$pr = round(self::$Delivery_Ok[4]*100/self::$Delivery_Ok[0],0);
		$ss .= "<div class='space-between'><span>Не отжато</span><span>". self::$Delivery_Ok[4] ."&nbsp;|&nbsp;$pr%</span></div>";
		
        if(is_array(self::$ArrayListOrg) && count(self::$ArrayListOrg) == 1){
            $result = DB::select("SELECT script FROM bis_cmsmenu WHERE sysid=3 LIMIT 1");
            $ss .= "<a href='/".$result[0]->script."?".self::$QUERYSTRING."' class='analitik-button'>Подробнее</a>";
        }
		return "<div class='analitik-block-content'>$ss</div>";
	}
	private function Analitik_Delivery_Ok_Sum(){
		$query="SELECT * FROM `OrderTEMP` WHERE "
		."expectedDeliverTime BETWEEN "
		."STR_TO_DATE('".self::$Sum_Time_Dat[0]->format('Y-m-d H:i:s')."','%Y-%m-%d %H:%i:%s') AND "
		."STR_TO_DATE('".self::$Sum_Time_Dat[1]->format('Y-m-d H:i:s')."','%Y-%m-%d %H:%i:%s') "
		."&& isCafe=0 && isCanceled=0 && "
		."organizationId in('". self::$IdListOrg ."') && isClientDelivery=0 && Courier_Name IS NOT NULL "
		."&& isClosest=1";
		$ctg = DB::select($query);
		foreach($ctg as $cat1){
            $cat = (array)$cat1;
			self::$Delivery_Ok[0]++; // всего
			if(!is_null($cat['deliveredDateTime']) && $cat['deliveredDateTime']!='0000-00-00 00:00:00'){
				if((int)$cat['LeadTime'] <= self::$Late_Min[0]){ // отлично
					self::$Delivery_Ok[1]++;
				}elseif((int)$cat['LeadTime']>self::$Late_Min[0] && (int)$cat['LeadTime'] <= self::$Late_Min[1]){
					// опоздание
					self::$Delivery_Ok[2]++;
				}elseif((int)$cat['LeadTime'] > self::$Late_Min[1]){
					self::$Delivery_Ok[3]++; // критическое опоздание
				}
			}else{
				self::$Delivery_Ok[4]++; // не отжат
			}
		}
	}

} //end
