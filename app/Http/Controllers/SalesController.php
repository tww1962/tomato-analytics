<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HomeController as General;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use DateTime;

class SalesController extends Controller
{
    public static $user;
    private static $Data_Report;
    private static $Sum_Time_Dat;
    private static $IdListOrg;
    private static $ArrayListOrg;
    private static $Sum_Time;
    private static $List_Courier;
    private static $Courier_Get;
	private static $One_Courier;
    private static $Delivery_Ok;
    private static $Dat;
    public static $Late_Min=[0,60,-1];
    private static $M_Sales;

    public function index(Request $request){
        if(isset($request->flagMain)){ //модальные
            if((int)$request->flagMain == 1000 ){ //список продуктов
                return self::Sales_Dish($request);
            }elseif((int)$request->flagMain == 1001 ){ // форма
                return self::Sales_Rezult($request);
            }
            return "alert('Неизвестная ошибка, обратитесь к разработчику')";
        }else{
            self::$user = Auth::user();
            $cmsmenu=DB::select("SELECT * FROM bis_cmsmenu WHERE script='sales' LIMIT 1");
            $query="SELECT sysid,`Department.Id`,`Department` FROM `analitic_promo_order` "
                ."WHERE `Department.Id`!='37bcd563-750c-4b5d-b678-471865a7bd51' GROUP BY `Department`";
            $point = DB::select($query);
            return view('sales',['user'=>self::$user,'cmsmenu'=>(array)$cmsmenu[0],'points'=>$point]);
        }
    }

	private function Sales_Rezult($request){
		$d1 = implode("-",array_reverse(explode(".",$request->d1)));
		$d2 = implode("-",array_reverse(explode(".",$request->d2)));
		$point = "'".implode("','",explode("#",$request->point))."'";
		$dish = "'".implode("','",explode("#",$request->dish))."'";
		$query="SELECT `Delivery.CustomerCreatedDateTyped` as user_data,"
			."`UniqOrderId.Id` as order_dish FROM `analitic_promo_order` "
			."WHERE `OpenDate.Typed` BETWEEN "
			."STR_TO_DATE('$d1','%Y-%m-%d') AND STR_TO_DATE('$d2','%Y-%m-%d') "
			."&& `Department.Id` in($point) && `DishId` in($dish) && "
			."`Delivery.CustomerCreatedDateTyped` IS NOT NULL && `Delivery.CustomerCreatedDateTyped`!='0000-00-00' "
			."GROUP BY `UniqOrderId.Id`";
		//return "console.log('$query')";
		$ctg = DB::select($query);
		self::$M_Sales['count_sales'] = count($ctg);
		if(self::$M_Sales['count_sales']<1) return "<h2>Данные не найдены</h2>";
		self::$M_Sales['time_start']=strtotime($d1." 00:00:00");
		self::$M_Sales['time_stop']=strtotime($d2." 23:59:59");
		self::$M_Sales['new_user']=self::$M_Sales['old_user']=self::$M_Sales['sales_sum']=0;
		foreach($ctg as $cat){
			$User_Time=strtotime($cat->user_data ." 01:00:00");
			if($User_Time>=self::$M_Sales['time_start'] && $User_Time<=self::$M_Sales['time_stop']) self::$M_Sales['new_user']++;
			else self::$M_Sales['old_user']++;
			self::Sales_Rezult_Mani($cat->order_dish);
		}
		$ss = "";
		$ss .= "<div>Количество розданных листовок: <input type='number' value='0' id='count_flyer' /></div>";
		$ss .= "<div>Количество заказов с листовок: <strong id='count_order'>". self::$M_Sales['count_sales'] ."</strong></div>";
		$ss .= "<div>Конверсия: <strong id='conversion' style='color:#F52341'></strong></div>";
		$ss .= "<div>Новые клиенты от листовок: <strong>". self::$M_Sales['new_user'] ."</strong></div>";
		$ss .= "<div>Старые клиенты от листовок: <strong>". self::$M_Sales['old_user'] ."</strong></div>";
		$ss .= "<div>Выручка от листовок: <strong>". self::$M_Sales['sales_sum'] ."</strong></div>";
		return $ss;
	}
	private function Sales_Rezult_Mani($order_id){
		$query="SELECT sum(`DishDiscountSumInt`) as order_sum FROM `analitic_promo_order` WHERE `UniqOrderId.Id`='$order_id'";
		$ctg = DB::select($query);
		self::$M_Sales['sales_sum'] += $ctg[0]->order_sum;
	}

    private function Sales_Dish($request){
		$d1=implode("-",array_reverse(explode(".",$request->d1)));
		$d2=implode("-",array_reverse(explode(".",$request->d2)));
		$point="'".implode("','",explode("#",$request->point))."'";
		$query="SELECT sysid,`DishName`,`DishId` FROM `analitic_promo_order` WHERE `OpenDate.Typed` BETWEEN "
		    ."STR_TO_DATE('$d1','%Y-%m-%d') AND STR_TO_DATE('$d2','%Y-%m-%d') && `Department.Id` in($point) "
		    ."GROUP BY `DishId` ORDER BY `DishName`";
		$ss = "";
		$ctg = DB::select($query);
		foreach($ctg as $cat1){
            $cat = (array)$cat1;
			$name = General::DecodeTitle($cat['DishName']);
			$ss .= "<div class='flex flex-align-center'>"
				."<input type='checkbox' name='sales_dish[]' data-sid='{$cat['DishId']}' id='sales_dish_{$cat['sysid']}' />"
				."<label for='sales_dish_{$cat['sysid']}'>$name</label>"
				."</div>";
		}
		return $ss;
	}

    public function Data_Report($d1,$d2){
		self::$Data_Report[0] = date("Y-m-d H:i:s",strtotime("$d1 00:00:00"));
		self::$Data_Report[1] = date("Y-m-d H:i:s",strtotime("$d2 23:59:59"));
	}

} //END
