<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HomeController as General;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use DateTime;


class DeliveryController extends Controller
{
    public static $user;
    private static $Data_Report;
    private static $Sum_Time_Dat;
    private static $IdListOrg;
    private static $ArrayListOrg;
    private static $Sum_Time;
    private static $List_Courier;
    private static $Delivery_Ok;
    private static $Dat;
    public static $Late_Min=[0,60,-1];
    private static $QUERYSTRING;
    public static $Late_Where;
    public static $Late_Where_Null;
    private static $sek_Kitchen;

    public function index(Request $request){
        if(isset($request->point_multi)){
            $m = (array)$request->all();
            unset($m['_token']);
            self::$QUERYSTRING = http_build_query($m);
            self::$ArrayListOrg = (array)$request->point_multi;
            if(count(self::$ArrayListOrg) == 1) General::session_param_put($m);
        }
        self::$user = Auth::user();
        $cmsmenu=DB::select("SELECT * FROM bis_cmsmenu WHERE script='dostavka-i-kurery' LIMIT 1");
		$point = DB::select("SELECT * FROM `Organizations` WHERE Conception='ТиЧ' && "
            ."Id in(SELECT id FROM userlist_city WHERE parent=". self::$user->sysid .") ORDER BY OrganizationName");
        return view('delivery',['user'=>self::$user,'cmsmenu'=>(array)$cmsmenu[0],'points'=>$point]); 
    }
	/*
    public function new_query(Request $request){
        $m = (array)$request->all();
        unset($m['_token']);
        self::$QUERYSTRING = http_build_query($m);
        self::$ArrayListOrg = (array)$request->point_multi;
        if(count(self::$ArrayListOrg) == 1) General::session_param_put($m);
        return "location.reload(true);";
	}
	*/
    public function show_reload($param){
        self::$QUERYSTRING = http_build_query($param);
        self::$IdListOrg = $param['point_multi'];
		self::$Dat[0]=$param['date1'];
		self::$Dat[1]=$param['date2'];
        self::$Late_Where = self::$Late_Where = "";
        if(isset($param['late'])){
            if(isset($param['late'][2])) self::$Late_Where_Null = "LeadTime IS NULL ";
            if(isset($param['late'][0]) && isset($param['late'][1])){
                self::$Late_Where="LeadTime>".self::$Late_Min[0];
            }elseif(!isset($param['late'][0]) && isset($param['late'][1])){
                self::$Late_Where="LeadTime>".self::$Late_Min[1];
            }elseif(isset($param['late'][0]) && !isset($param['late'][1])){
                self::$Late_Where=" (LeadTime>".self::$Late_Min[0] ." && LeadTime<=".self::$Late_Min[1].")" ;
            }
        }
		return self::show_dann(0);
	}

	function show_dann($ajax){
		$d1=implode("-",array_reverse(explode(".",self::$Dat[0])));
		$d2=implode("-",array_reverse(explode(".",self::$Dat[1])));
		self::Data_Report($d1,$d2);
		$query="SELECT * FROM `OrderTEMP` WHERE "
		."expectedDeliverTime BETWEEN STR_TO_DATE('".self::$Data_Report[0]."','%Y-%m-%d %H:%i:%s') AND "
		."STR_TO_DATE('".self::$Data_Report[1]."','%Y-%m-%d %H:%i:%s') && isCafe=0 && isCanceled=0 && "
		."organizationId='". self::$IdListOrg ."' && isClientDelivery=0 && Courier_Name IS NOT NULL ";
		if(!empty(self::$Late_Where) && empty(self::$Late_Where_Null)){
			$query .= "&& deliveryStatus in(3,4,5) && ".self::$Late_Where;
		}elseif(!empty(self::$Late_Where) && !empty(self::$Late_Where_Null)){
			$query .= "&& deliveryStatus in(3,4,5) && (".self::$Late_Where ." || ".self::$Late_Where_Null.")";
		}elseif(empty(self::$Late_Where) && !empty(self::$Late_Where_Null)){
			$query .= "&& deliveryStatus in(3,4,5) && ".self::$Late_Where_Null;
		}else{
			$query .= "&& deliveryStatus in(3,4,5)";
		}
        $result = DB::select($query." ORDER BY expectedDeliverTime DESC");
        $ss = "";
        foreach($result as $cat1){
            $cat = (array)$cat1;
            $td_number = "<div class='delivery-td'><span data-sid='{$cat['id']}' class='modal' title='Данные заказа'>{$cat['number']}</span></div>";
            $td = "";
            /*
			if((int)$cat['deliveryStatus']==0){ // не подтверждён
				$td .= $this->Delivery_Unconfirmed($cat);
				$td .= $this->Delivery_Empty(8);
			}else
            */
            if((int)$cat['deliveryStatus']==1){// подтверждён
				$td .= self::Delivery_New($cat);
				$td .= self::Delivery_Empty(5);
			}elseif((int)$cat['deliveryStatus']==2){// на кухне
				$td .= self::Delivery_Waiting($cat);
				$td .= self::Delivery_Empty(4);
			}elseif((int)$cat['deliveryStatus']==3 && !is_null($cat['Courier_Name'])){// в пути ?? назначен
				$td .= self::Delivery_OnWay($cat);
				$td .= "<div class='delivery-td'>В пути</div>";
				$td .= self::Delivery_Empty(2);
				$td .= self::Delivery_Courier($cat);
			}elseif(!is_null($cat['Courier_Name'])){//доставлен || закрыт in_array((int)$cat['deliveryStatus'],[4,5])
				$td .= self::Delivery_Delivered($cat);
				$td .= self::Delivery_Courier($cat);
			}
			//$td .= $this->Delivery_Courier($cat);
			if(!empty($td)) $ss .= "<div class='delivery-tr'>$td_number$td</div>";
        }
		return $ss;
	}

	function Delivery_Delivered($cat){
		$ss = self::Delivery_OnWay($cat);
		if(!self::Is_Null_($cat['deliveredDateTime']) && $cat['deliveredDateTime']!='0000-00-00 00:00:00'){
			$ss .= "<div class='delivery-td'><span>";
				if(self::Is_Null_($cat['sendTime']) || $cat['sendTime']=='0000-00-00 00:00:00'){
					$sendTime=0;
					$ss .= "О:00:00:00<br />";
				}else{
					$O=$sendTime=strtotime($cat['sendTime']);
					$ss .= "О:".date('H:i:s',$O)."<br />";
				}
				$ss .= "Д:".date('H:i:s',strtotime($cat['deliveredDateTime']))."</span>";
			$ss .= "</div>";
			$time = strtotime($cat['deliveredDateTime'])-$sendTime;
			$ss .= "<div class='delivery-td'>"
				.General::Sprintf_Time($time)
				."</div>";
			$cl="";
			if(!self::Is_Null_($cat['LeadTime'])){
				if((int)$cat['LeadTime'] <= self::$Late_Min[0]){
					$cl='green';
				}elseif((int)$cat['LeadTime']>self::$Late_Min[0] 
					&& (int)$cat['LeadTime'] <= self::$Late_Min[1])
				{
					$cl = "yelow";
				}elseif((int)$cat['LeadTime'] > self::$Late_Min[1]){
					$cl = "red";
				}
			}
			//$time_itog = self::$sek_Kitchen+$time;
			$time_itog = strtotime($cat['deliveredDateTime'])-strtotime($cat['expectedDeliverTime']);
			$znak=$time_itog<0?"-":"";
			$time_itog=abs($time_itog);
			$deliveredDateTime_createTime=strtotime($cat['deliveredDateTime'])-strtotime($cat['createTime']);
			$isClosest=$cat['isClosest']==1?"":"before_time";
			$ss .= "<div class='delivery-td delivery-td_center delivery-td_color $cl'>"
				."<span class='$isClosest'>"
				.General::Sprintf_Time($deliveredDateTime_createTime)
				."<br />$znak".General::Sprintf_Time($time_itog)
				."</span>"
				."</div>";
		}else{
			$ss .= "<div class='delivery-td delivery-td_center'><span>Не отжат<br />курьером</span></div>";
			$ss .= "<div class='delivery-td delivery-td_center'><span>&nbsp;</span></div>";
			$ss .= "<div class='delivery-td delivery-td_center'><span>&nbsp;</span></div>";
		}
		return $ss;
	}

	function Delivery_Courier($cat){
		$s="&nbsp;";
		if(!is_null($cat['Courier_Name'])) $s=$cat['Courier_Name'];
		return "<div class='delivery-td'><span class='delivery-person'>$s</span></div>";
	}

	function Delivery_OnWay($cat){
		$ss = self::Delivery_Waiting($cat); //ExpectedKitchenDateTime
		if(!self::Is_Null_($cat['startKitchenDateTime'])){
			$time = strtotime($cat['startKitchenDateTime']) + self::$sek_Kitchen - strtotime($cat['sendTime']);
			$ss .= "<div class='delivery-td'>".General::Sprintf_Time($time)."</div>";
		}else $ss .= "<div class='delivery-td'>Назначен</div>";
		return $ss;
	}

	function Delivery_Waiting($cat){
		$ss = self::Delivery_New($cat);
		if(!self::Is_Null_($cat['totalKitchenTime'])){
			$subs = explode(':',$cat['totalKitchenTime']);
			self::$sek_Kitchen = $subs[0]*3600+$subs[1]*60+$subs[2];
			$ss .= "<div class='delivery-td'><span>"
				.General::Sprintf_Time(self::$sek_Kitchen)
				."</span></div>";
		}else{
			$ss .= "<div class='delivery-td delivery-td_center'><span>&nbsp;</span></div>";
			self::$sek_Kitchen=0;
		}
		return $ss;
	}

	function Delivery_New($cat){
		$ss = "";
		$ss .= "<div class='delivery-td'><span>" // содан
			.date('d.m.y',strtotime($cat['createTime']))."<br />"
			.date('H:i:s',strtotime($cat['createTime']))."</span></div>";
		$ss .= "<div class='delivery-td'><span>" //доставить к
			.date('d.m.y',strtotime($cat['expectedDeliverTime']))."<br />"
			.date('H:i:s',strtotime($cat['expectedDeliverTime']))."</span></div>";
		if(!self::Is_Null_($cat['deliveryDuration'])){ //Ожидание
			$s = $cat['deliveryDuration']=="03:15:00"?"ближ.":$cat['deliveryDuration'];
			$ss .= "<div class='delivery-td'><span>$s</span></div>";
		}else $ss .= "<div class='delivery-td delivery-td_center'><span>&nbsp;</span></div>";
		return $ss;
	}

	function Is_Null_($s){
		if(is_null($s)) return true;
		return false;
	}
	function Delivery_Empty($kol){
		$ss = "";
		for($k=0;$k<$kol;$k++){$ss .= "<div class='delivery-td delivery-td_center'><span>&nbsp;</span></div>";}
		return $ss;
	}

	public function Data_Report($d1,$d2){
		self::$Data_Report[0] = date("Y-m-d H:i:s",strtotime("$d1 00:00:00"));
		self::$Data_Report[1] = date("Y-m-d H:i:s",strtotime("$d2 23:59:59"));
	}

    public function modal(Request $request){
		$ctg=DB::select("SELECT * FROM `OrderTEMP` WHERE id='". $request->sid ."' LIMIT 1");
		$cat = (array)$ctg[0];
		$ss = "";
		$ss .= "<div>Заказ № {$cat['number']} от ".date('d.m.Y H:i:s',strtotime($cat['createTime']))."</div>";
		if(!empty($cat['client']) && !is_null($cat['client'])){
			$m=json_decode($cat['client'],true);
			if(isset($m['Name'])) $ss .= "<div>".General::DecodeTitle($m['Name']) ." "
				.General::DecodeTitle($m['Surname']) ."</div>";
		}
		$ss .= "<div>Телефон: {$cat['phone']}</div>";
		if(!empty($cat['address']) && !is_null($cat['address'])){
			$m=json_decode($cat['address'],true);
			$ss .= "<div>"
				."{$m['Street']['City']['Name']}, {$m['Street']['Name']}, {$m['House']}, {$m['Flat']}"
				."</div>";
		}
		return "modalSity('<h3>Данные заказа</h3>$ss');";
    }

} //END
