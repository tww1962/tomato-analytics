<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Imagick;
use App\Http\Controllers\Admin\AdminGlobalController as General;

class AdminUploadController extends Controller
{
    private $Modul;
    private $Parent;
    private $Table;
	private $arrayParent;

    public function index(Request $request,$modul){
        $this->Modul=$modul;
        $this->Parent=(int)$request->parent;
        switch($modul){
            case 'photogallery': //загрузка фото
                return $this->Photogallery($request);
                break;
			case 'photogallery-name': //название фото
				return $this->PhotogalleryName($request);
				break;
			case 'photogallery-bulk-select': //массовые изменения
				return $this->PhotogalleryBulkSelect($request);
				break;
			case 'photogallery-turn': //поворот
				return $this->PhotogalleryTurn($request);
				break;
			case 'slider-photo': // загрузка фото слайдер
				return $this->PhotoSlider($request);
				break;
			case 'slider-info': //данные фото слайдера
				return $this->PhotoSliderInfo($request);
				break;
			case 'doc-files': //новый документ
				return $this->DocFilesUpload($request);
				break;
			case 'doc-name': //корректировка названия документа
				return $this->DocFilesName($request);
				break;
			default: return "";
		}
    }

	private function DocFilesName($request){
		$name = General::DecodeTitle($request->name);
		$sid = (int)$request->sid;
		DB::update("UPDATE bis_files SET name='$name' WHERE sysid=$sid LIMIT 1");
        return response()->json(['success' => true,'msg' => $name]);

	}
	private function DocFilesUpload($request){
		$msgRezult = "<p>&nbsp;</p>";
        if($request->hasfile('foto')){
			$name = General::DecodeTitle($request->name);
            $mime=config('photoupload.allow_format_doc');
			$file=$request->file('foto');
			$fileName = $file->getClientOriginalName();
			if(empty($fileName) || !$file->isValid()){
				$msgRezult .= "<p>$fileName - не загружен, нет имени</p>";
			}elseif(!in_array($file->extension(),$mime)){
				$msgRezult .= "<p>$fileName - не загружен, проверьте тип файла</p>";
			}else{
				$namePhoto="doc". str_replace('.','',(string)microtime(true)) .".". $file->extension();
				$file->move(public_path('files/doc'), $namePhoto);
				DB::insert("INSERT INTO bis_files(parent,name,files) VALUES({$this->Parent},'$name','$namePhoto')");
				$msgRezult .= "<p>$fileName - загружен</p>";
			}
        }else $msgRezult .= "<p>Файлы не найдены</p>";
        $msgRezult .= "<p>&nbsp;<p><button type='button' onclick='window.location.reload(true);'>Мною прочитано</button></p>";
        return $msgRezult;

	}
	function DocFilesDann($parent){
		$ss = "";
		$ctg = DB::select("SELECT * FROM bis_files WHERE parent=$parent ORDER BY pos");
		if(!empty($ctg)){
			$r = mt_rand(1000,5000);
			$ss .= "<ul id='sortable' class='sortableRazdel general-ul all-list' data-tables='bis_files'>";
			foreach($ctg as $cat){
				$sid = $cat->sysid; $nik = General::DecodeTitle($cat->name);
				if($cat->hide == 'show') { $glaz = "<span class='main-icon'><i class='fa fa-eye' aria-hidden='true' title='Скрыть'></i></span>";} 
				else { $glaz = "<span class='main-icon'><i class='fa fa-eye-slash' aria-hidden='true' title='Отобразить'></i></span>";}
				$ss .= "<li id='li$sid' sid='$sid'>";
					$ss .= "<div class='all-list-item'>";
						$ss .= "<div><span class='main-icon sort'><i class='fa fa-arrows-v' aria-hidden='true' title='Сортировка'></i></span></div>";
						$ss .= "<div id='div$sid'>$nik</div>";
						$ss .= "<div data-sid='$sid' name='$nik' data-parent='#div$sid' class='filesupload'>";
							$ss .= "<span class='main-icon'><i class='fa fa-pencil' aria-hidden='true' title='Редактировать имя файла'></i></span>";
							$ss .= $glaz;
							$ss .= "<span class='main-icon'><i class='fa fa-trash-o' aria-hidden='true' title='Удалить'></i></span>";
						$ss .="</div>";
					$ss .="</div>";
				$ss .= "</li>";
			}
			$ss .= "</ul>";
		}
		return $ss;
	}

    private function PhotoSlider($request){
		$msgRezult = "<p>&nbsp;</p>";
		$kol = count($request->file('foto'));
		if($kol > config('photoupload.KOLMAXFILE_SLIDER')) $kol = config('photoupload.KOLMAXFILE_SLIDER');
        if($request->hasfile('foto')){
            $i=1;
            $mime=config('photoupload.allow_format_slider');
            $SIZE_MAX=config('photoupload.SIZE_MAX_SLIDER');
            foreach($request->file('foto') as $key => $file){
                if($i>$kol) break;
                $i++;
                $fileName = $file->getClientOriginalName();
                if(empty($fileName) || !$file->isValid()){
                    $msgRezult .= "<p>$fileName - не загружен, нет имени</p>";
                    continue;
                }
                if(!in_array($file->extension(),$mime)){
                    $msgRezult .= "<p>$fileName - не загружен, проверьте тип файла</p>";
                    continue;
                }
                if ($file->getSize() > $SIZE_MAX){
                    $msgRezult .= "<p>$fileName - не загружен, проверьте размер файла</p>";
                    continue;
                }
                $namePhoto="img". str_replace('.','',(string)microtime(true)) .".". $file->extension();
               	$msgRezult .= $this->PhotoSlideryReSize($file->path(),$namePhoto,(int)$request->width,(int)$request->height);
            }
        }else $msgRezult .= "<p>Файлы не найдены</p>";
        $msgRezult .= "<p>&nbsp;<p><button type='button' onclick='window.location.reload(true);'>Мною прочитано</button></p>";
        return $msgRezult;
    }
	private function PhotoSlideryReSize($image, $fotoInBase,$width,$height){
		$FOTO = public_path('files/foto') . "/$fotoInBase";
		$size_img = getimagesize($image);
		$im = new Imagick($image);
		if( $size_img[0] > $width) $im->adaptiveResizeImage($width,0);
        $im->writeImage($FOTO);
		$im->clear(); 
		$im->destroy();
        DB::insert("INSERT INTO bis_foto(parent,foto) VALUES({$this->Parent}, '$fotoInBase')");
        unlink($image);
		return "<p>$fotoInBase - загружен</p>";
	}
	function SliderDann($FotoWidth,$parent){
		$ss = "";
		$ss .= "<p>Для сортировки фотографий, нажмите и удерживая фото переместите его на место.</p>";

		$ctg = DB::select("SELECT * FROM bis_foto WHERE parent=$parent ORDER BY pos");
		if(!empty($ctg)){
			$ss .= "<ul id='sortable' class='sortableSlider general-ul slider-list' data-tables='bis_foto' data-w='{$FotoWidth[0]}' data-h='{$FotoWidth[1]}'>";
			$r = mt_rand(1000,5000);
			$style="overflow:hidden;width:{$FotoWidth[0]}px;max-height:{$FotoWidth[1]}px;background:#FFF;";
			foreach($ctg as $cat1){
				$cat=(array)$cat1;
				$sid = $cat['sysid'];
				if($cat['hide'] == 'show') { $glaz = "<span class='main-icon'><i class='fa fa-eye' aria-hidden='true' title='Скрыть'></i></span>";} 
				else { $glaz = "<span class='main-icon'><i class='fa fa-eye-slash' aria-hidden='true' title='Отобразить'></i></span>";}
				$ss .= "<li id='li$sid' sid='$sid'>";
				$ss .= "<div style='$style' id='divfoto_$sid' class='sort'>";
					$ss .= "<img src='/files/foto/{$cat['foto']}?$r' width='{$FotoWidth[0]}' id='foto_$sid'/>";
				$ss .= "</div>";
				$ss .= "<div class='photo-slider-menu' data-sid='$sid' data-parent='#li$sid'>";
					$ss .= "<span class='main-icon'><i class='fa fa-trash-o' aria-hidden='true' title='Удалить'></i></span>";
					$ss .= $glaz;
				$ss .= "</div>";
				$ss .= "<form><table>";
				$ss .= "<tr><td>Название</td><td>Описание</td></tr>";
				$ss .= "<tr><td><textarea class='input' name='name' style='height:50px'>".htmlspecialchars_decode($cat['name'])."</textarea></td>";
				$ss .= "<td><textarea class='input' type='text' name='info' style='height:50px'>".htmlspecialchars_decode($cat['info'])."</textarea></td></tr>";
				$ss.="<tr><td colspan='2'><label>Ссылка <input class='input' type='text' name='href' value='{$cat['ahref']}'/></label></td></tr>";
				$ss.="<tr><td colspan='2'><input type='submit' class='button' value='Сохранить' /><span id='sp$sid' style='color: #fe8d00'></span></td></tr>";
				$ss .= "</table><input type='hidden' name='sysid' value='$sid' /></form></li>";
			}
			$ss .= "</ul>";
		}
		return $ss;
	}
	private function PhotoSliderInfo($request){
		$sysid = (int)$request->sysid;
		$name = htmlspecialchars($request->name, ENT_QUOTES);
		$info = htmlspecialchars($request->info, ENT_QUOTES);
		$href = htmlspecialchars($request->href, ENT_QUOTES);
		DB::update("update bis_foto set name='$name', info='$info', ahref='$href' where sysid=$sysid LIMIT 1");
		return response()->json(['success' => true,'id' => $sysid]);
	}

	private function PhotogalleryTurn($request){
		$sid = (int)$request->sysid;
		$photo = $request->photo;
		$preview=config('photoupload.preview_photo');
		$pathImg = [$_SERVER['DOCUMENT_ROOT'] .'/files/small/',$_SERVER['DOCUMENT_ROOT'] .'/files/average/',$_SERVER['DOCUMENT_ROOT'] .'/files/big/'];
		foreach($pathImg as $path){
			$im = new Imagick($path.$photo);
			$im->rotateimage('white',90);
			$im->writeImage($path.$photo);
			$im->clear(); 
			$im->destroy();
		}
		return response()->json(['success' => true,'msg' => "/files/$preview/$photo?".mt_rand(1000,5000)]);
	}

	private function PhotogalleryBulkSelect($request){
		$listId = $request->m;
		$vid = (int)$request->vid;
		switch( $vid ){
			case 1:
				DB::update("UPDATE bis_galfoto SET hide='show' WHERE sysid in ( $listId )");
			break;
			case 2:
				DB::update("UPDATE bis_galfoto SET hide='hide' WHERE sysid in ( $listId )");
			break;
			case 3:
				DB::delete("DELETE FROM bis_galfoto WHERE sysid in ( $listId )");
			break;
		}
		return response()->json(['success' => true]);
	}
	private function PhotogalleryName($request){
		$name = General::DecodeTitle($request->name);
		$sid = (int)$request->sysid;
		DB::update("UPDATE bis_galfoto SET name='$name' WHERE sysid=$sid LIMIT 1");
        return response()->json(['success' => true,'id' => $sid]);
	}

    private function Photogallery($request){
		$msgRezult = "<p>&nbsp;</p>";
		$kol = count($request->file('foto'));
		if($kol > config('photoupload.KOLMAXFILE')) $kol = config('photoupload.KOLMAXFILE');
        if($request->hasfile('foto')){
            $i=1;
            $mime=config('photoupload.allow_format_photo');
            $SIZE_MAX=config('photoupload.SIZE_MAX');
            foreach($request->file('foto') as $key => $file){
                if($i>$kol) break;
                $i++;
                $fileName = $file->getClientOriginalName();
                if(empty($fileName) || !$file->isValid()){
                    $msgRezult .= "<p>$fileName - не загружен, нет имени</p>";
                    continue;
                }
                if(!in_array($file->extension(),$mime)){
                    $msgRezult .= "<p>$fileName - не загружен, проверьте тип файла</p>";
                    continue;
                }
                if ($file->getSize() > $SIZE_MAX){
                    $msgRezult .= "<p>$fileName - не загружен, проверьте размер файла</p>";
                    continue;
                }
                $namePhoto="img". str_replace('.','',(string)microtime(true)) .".". $file->extension();
                $msgRezult .= $this->PhotogalleryReSize($file->path(),$namePhoto);
            }
        }else $msgRezult .= "<p>Файлы не найдены</p>";
        $msgRezult .= "<p>&nbsp;<p><button type='button' onclick='window.location.reload(true);'>Мною прочитано</button></p>";
        return $msgRezult;
    }

    private function PhotogalleryReSize($image, $fotoInBase){
		$FOTO = public_path('files/big') . "/$fotoInBase";
		$FOTOA = public_path('files/average') . "/$fotoInBase";
		$FOTOS = public_path('files/small') . "/$fotoInBase";
        $size_big=config('photoupload.size_big');
        $size_average=config('photoupload.size_average');
        $size_small=config('photoupload.size_small');
		$size_img = getimagesize($image);
		$im = new Imagick($image);
		if( $size_img[0] > $size_big[0] || $size_img[1] > $size_big[1] ) {
			if( $size_img[0] > $size_img[1] ) $im->adaptiveResizeImage($size_big[0],0);
			else $im->adaptiveResizeImage(0,$size_big[1]);
		}
		$widthFOTO = $im->getImageWidth();
        $heightFOTO = $im->getImageHeight();
        $im->writeImage($FOTO);
		if( $widthFOTO > $size_average[0] || $heightFOTO > $size_average[1] ) {
			if( $widthFOTO > $heightFOTO ) $im->adaptiveResizeImage($size_average[0],0);
			else $im->adaptiveResizeImage(0,$size_average[1]);
		}
		$widthFOTO = $im->getImageWidth();
        $heightFOTO = $im->getImageHeight();
        $im->writeImage($FOTOA);
		if( $widthFOTO > $size_small[0] || $heightFOTO > $size_small[1] ) {
			if( $widthFOTO > $heightFOTO ) $im->adaptiveResizeImage($size_small[0],0);
			else $im->adaptiveResizeImage(0,$size_small[1]);
		}
        $im->writeImage($FOTOS);
		$im->clear(); 
		$im->destroy();
        DB::insert("INSERT INTO bis_galfoto(parent,foto) VALUES({$this->Parent}, '$fotoInBase')");
        unlink($image);
		return "<p>$fotoInBase - загружен</p>";
    }
    
	function PhotogalDann($parent){
		$ss = "";
		$preview=config('photoupload.preview_photo');
		$ctg = DB::select("SELECT * FROM bis_galfoto WHERE parent=$parent ORDER BY pos");
		if(!empty($ctg)){
			$ss .= "<p>Для сортировки фотографий, нажмите и удерживая фото переместите его на место.</p>";
			$ss .= self::PhotogalChoose();
			$r = mt_rand(1000,5000);
			$ss .= "<ul id='sortable' class='sortablePhoto general-ul photo-list' data-tables='bis_galfoto'>";
			foreach($ctg as $cat1){
                $cat=(array)$cat1;
				$sid = $cat['sysid'];
				if($cat['hide'] == 'show') { $glaz = "<span class='main-icon'><i class='fa fa-eye' aria-hidden='true' title='Скрыть'></i></span>";} 
				else { $glaz = "<span class='main-icon'><i class='fa fa-eye-slash' aria-hidden='true' title='Отобразить'></i></span>";}
				$ss .= "<li id='li$sid' sid='$sid'><div>";
					$ss .= "<div class='sort photoPreview' id='photo$sid' style='background-image: url(/files/$preview/{$cat['foto']}?$r)' ></div>";
					$ss .= "<div class='photo-slider-menu' data-sid='$sid' data-parent='#li$sid'>";
						$ss .= "<a class='main-icon' href='/files/big/{$cat['foto']}' data-lightbox='tww' title='Просмотр'><i class='fa fa-file-image-o' aria-hidden='true'></i></a>";
						$ss .= "<span class='main-icon turn' title='Поворот' data-sid='$sid' data-photo='{$cat['foto']}'><i class='fa fa-repeat' aria-hidden='true'></i></span>";
						$ss .= $glaz;
						$ss .= "<span class='main-icon' title='Удалить'><i class='fa fa-trash-o' aria-hidden='true'></i></span>";
						$ss .= "<span class='main-icon for-choose' title='Отметить'><input type='checkbox' data-sid='$sid' class='choose'/></span>";
					$ss .= "</div>";
					$ss .= "<form>";
					$ss .= "<p><input type='text' class='input' name='name' value='{$cat['name']}' required placeholder='Название' /></p>";
					$ss .= "<p><input type='submit' class='button-gray' value='Сохранить' /><span id='sp$sid' style='color: #fe8d00'></span></p>";
					$ss .= "<input type='hidden' name='sysid' value='$sid' /></form>";
				$ss .= "</div></li>";
			}
			$ss .= "</ul>";
			$ss .= self::PhotogalChoose();
		}
		return $ss;
	}
	
	function PhotogalChoose(){
		$ss = "";
		$ss .= "<div class='actions-choose'>";
			$ss .= "<div><input type='checkbox' id='choose'/><label for='choose'>Выбрать все</label></div>";
			$ss .= "<div><select name='actions' id='actions-choose'>".
				"<option value='0'>Действие с выбранными</option>".
				"<option value='1'>Отобразить</option>".
				"<option value='2'>Скрыть</option>".
				"<option value='3'>Удалить</option>".
				"</select></div>";
		$ss .= "</div>";
		return $ss;
	}
    

}
?>