<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cookie;

class AdminGlobalController extends Controller{
	public $idPapa;
	public $idSysid;
	public $arrayTranslit;
	public $dbBlog;
	public $kolRazdel;
	static $PathURL;
	private $Visible;
	private $Translit;
	private $Photo;
	private $LevelRazdel;
	private $LevelRecord;
	private $ChildrenMenu;
	public static $FeatureGlobalMenu;
	public static $UserAdmin;
	static $en;

	public function __construct(){
		$this->arrayTranslit = array();
		$this->idSysid = 0;
	}
	
	function SortPagesForRazdel(){ //  смена родителя
		$ss = "<h3 style='width: 475px;'>". NAMERAZDEL ."</h3>";
		if( isset( $_GET['find']) ) {
			$query = $this->ArticlesListFind();
			$ctg = $this->dbBlog->f_select($query);
		} elseif( IDRAZDEL == $this->SidRazdel ) {
			$ctg = $this->dbBlog->f_select("SELECT * FROM bis_pages WHERE level=". IDRAZDEL ." order by ". SORT);
		} else {
			$ctg = $this->dbBlog->f_select("SELECT * FROM bis_pages WHERE parent=". IDRAZDEL ." order by ". SORT);
		}
		if( $ctg->num_rows > 0 ) {
			$ss .= "<ul id='change-parent' class='change-parent'>";
			while( $cat = $ctg->fetch_assoc() ){
				$ss .= "<li data-sid='{$cat['sysid']}' data-parent='{$cat['parent']}' data-tables='bis_pages' id='change{$cat['sysid']}' title='Перетащи меня'>{$cat['name']}</li>";
			}
			$ss .= "</ul>";
			$ss .= $this->NewParentPages($this->SidRazdel);
		} else { $ss .= "<h2>Нет данных в разделе</h2>"; }
		$ss .= "<a class='parent-new-footer' class='user-select' href='{$this->PagGlobal}'>Выключить режим изменения раздела</a>";
		return $ss;
	}
	
	function NewParentPages($parent){
		$ss = "";
		$ctg = $this->dbBlog->f_select("select * from bis_categories where parent=$parent order by ". SORT);
		$ss .= "<div class='parent-new' id='parent-new'>";
			$ss .= "<a href='{$this->PagGlobal}' class='close user-select'>Выключить режим</a>";
			$this->ChildrenMenu = "";
			$this->NewParentPagesRazdel($parent);
			$ss .= $this->ChildrenMenu;
		$ss .= "</div>";
		$ss .= "<div class='parent-new-footer' id='parentNewClose' class='user-select'>Сменить раздел</div>";
		return $ss;
	}
	function NewParentPagesRazdel($parent){
		$ctg = $this->dbBlog->f_select("select * from bis_categories where parent=$parent order by ". SORT);
		if($ctg->num_rows > 0) {
			$this->ChildrenMenu .= "<ul>";
			while($cat = $ctg->fetch_assoc()){
				$hide = ( $cat['hide'] == 'hide' ) ? "<i class='fa fa-eye-slash' aria-hidden='true' style='color: red' title='скрыт'></i>" : "<i class='fa fa-eye' aria-hidden='true'  title='активен' style='color: greenyellow'></i>";
				$this->ChildrenMenu .= "<li><div data-sid='{$cat['sysid']}' id='dpn{$cat['sysid']}' >{$cat['name']}&nbsp;$hide</div>";
				$this->NewParentPagesRazdel($cat['sysid']);
				$this->ChildrenMenu .= "</li>";
			}
			$this->ChildrenMenu .= "</ul>";
		}
	}
	
	function reversPARENT($sid){  //  ищу sysid родителя 
		$this->idPapa = $sid;
		$ctg = $this->dbBlog->f_select("select sysid,parent from bis_pages where sysid=$sid");
		if($ctg->num_rows > 0){ 
			$cat = $ctg->fetch_assoc();
			$this->idSysid = $cat['sysid'];
			$this->reversPARENT($cat['parent']);
		} return true;
	}
	
	function reversTRANSLIT($sid){  //  ищу все ссылки
		$ctg = $this->dbBlog->f_select("select parent, translit, sysid from bis_pages where sysid=$sid");
		if($ctg->num_rows > 0){
			$cat = $ctg->fetch_assoc();
			$this->idSysid = $cat['sysid'];
			$this->arrayTranslit[] = $cat['translit'];
			$this->reversTRANSLIT($cat['parent']);
		} else {
			$ctg = $this->dbBlog->f_select("select parent, translit, sysid from bis_cmsmenu where sysid=$sid");
			if($ctg->num_rows > 0){
				$cat = $ctg->fetch_assoc();
				$this->arrayTranslit[] = $cat['translit'];
				$this->reversTRANSLIT($cat['parent']);
			}
		}
		return true;
	}
	
	function NavLeftContent(){
		$dostup = explode("#",self::$UserAdmin['dostup']);
		$ss = "<div class='left-global'>";
		$ss .= "<ul class='global-nav'>";
			$ss .= "<li><span style='color:#FFF;font-weight:bold;font-size:24px'>ТиЧ Аналитика</span></li>";
			$result = DB::select("SELECT * FROM bis_cmsmenu WHERE hide='show' and parent=0 and vidMenu!='book' ORDER BY pos");
			if(!empty($result)){
				$ss .= "<li>";
					$ss .= "<a class='user-select' href='javascript:vois(0)' id='nav0'>".
						"<i class='fa fa-files-o' aria-hidden='true'></i>Страницы сайта</a>";
				$ss .= "<ul>";
				foreach($result as $cat){
					if(in_array($cat->sysid,$dostup) || self::$UserAdmin['superroot']=='yes'){
						$name = htmlspecialchars_decode($cat->name);
						$ss .= "<li>";
							if( $cat->modul != 'akkord' ) {
								$ss .= "<a href='/admin/content/".$cat->modul
									."/?nav=0&id=". $cat->sysid ."' id='m". $cat->sysid ."' >$name</a>";
							} else {
								$ss .= "<a href='javascript:void(0)' id='m". $cat->sysid ."' >$name</a>";
							}
							$ss .= self::NavLeftChildren($cat->sysid, $dostup,'0');
						$ss .= "</li>";
					}
				}
				$ss .= "</ul>";
				$ss .= "</li>";
			}
			$result = DB::select("SELECT * FROM bis_cmsmenu WHERE hide='show' and vidMenu='book' ORDER BY pos");
			foreach($result as $cat){
				if(in_array($cat->sysid,$dostup) || self::$UserAdmin['superroot']=='yes'){
					$name = htmlspecialchars_decode($cat['name']);
					$ss .= "<li>";
						$ss .= "<a class='user-select' href='javascript:vois(0)' id='nav". $cat->sysid ."'>".
							"<i class='fa fa-book' aria-hidden='true'></i>$name</a>";
					$ss .= self::NavLeftChildren($cat->sysid, $dostup,$cat->sysid);
					$ss .= "</li>";
				}
			}
			if(in_array(850,$dostup) || self::$UserAdmin['superroot']=='yes'){
				$result = DB::select("select * from bis_cmsservice where sysid=850 and hide='show' LIMIT 1");
				if(!empty($result)) {
					$ss .= "<li class='for-nav-hide'>";
						$ss .= "<a class='user-select' href='javascript:void(0)' title='Обратная связь' id='nav850'>".
							"<i class='fa fa-commenting-o' aria-hidden='true'></i>Обратная связь</a>";
						$ss .= self::ServiceMenu(850,$dostup);
					$ss .= "</li>";
				}
			}
			if(in_array(900,$dostup) || self::$UserAdmin['superroot']=='yes'){
				$result = DB::select("select * from bis_cmsservice where sysid=900 and hide='show' LIMIT 1");
				if(!empty($result)) {
					$ss .= "<li class='for-nav-hide'>";
						$ss .= "<a class='user-select' href='javascript:void(0)' title='Настройки сайта' id='nav900'>".
							"<i class='fa fa-cog' aria-hidden='true'></i>Настройки</a>";
						$ss .= self::ServiceMenu(900,$dostup);
					$ss .= "</li>";
				}
			}
			if(in_array(950,$dostup) || self::$UserAdmin['superroot']=='yes'){
				$result = DB::select("select * from bis_cmsservice where sysid=950 and hide='show' LIMIT 1");
				if(!empty($result)) {
					$ss .= "<li class='for-nav-hide'>";
						$ss .= "<a class='user-select' href='javascript:void(0)' title='SEO' id='nav950'>".
							"<i class='fa fa-line-chart' aria-hidden='true'></i>SEO</a>";
						$ss .= self::ServiceMenu(950,$dostup);
					$ss .= "</li>";
				}
			}
		$ss .= "</ul>";
		$ss .= "</div>";
		return $ss;
	}
	public static function ServiceMenu($parent,$dostup){
		$ss = "";
		$result = DB::select("select * from bis_cmsservice where parent=$parent and hide='show' ORDER BY pos");
		$ss .= "<ul class='nav-hide'>";
		foreach($result as $cat){
			if(in_array($cat->sysid,$dostup) || self::$UserAdmin['superroot']=='yes'){
				$name = htmlspecialchars_decode($cat->name);
				$ss .= "<li>";
					$ss .= "<a href='/admin/content/". $cat->modul ."/?nav=$parent&"
						."type=". $cat->setting ."&parent=". $cat->parent ."&id=". $cat->sysid ."' ".
						"id='m". $cat->sysid ."' >$name</a>";
				$ss .= "</li>";
			}
		}
		$ss .= "</ul>";
		return $ss;
	}
	public static function NavLeftChildren($parent, $dostup, $nav){
		$ss = "";
		$result = DB::select("SELECT * FROM bis_cmsmenu WHERE parent=$parent and hide='show' ORDER BY pos");
		if(!empty($result)){
			$ss .= "<ul>";
			foreach($result as $cat){
				$name = htmlspecialchars_decode($cat['name']);
				$ss .= "<li>";
					$ss .= "<a href='/admin/content/{$cat->modul}/?nav=$nav&id={$cat->sysid}' "
						."id='m{$cat->sysid}' >$name</a>";
				$ss .= "</li>";
			}
			$ss .= "</ul>";
		}
		return $ss;
	}

	function GlobalSubmit($return){
		$ss = "";
		$ss .= "<div class='div-for-submit'>";
		$ss .= "<div class='table-cell-middle'><p>";
			$ss .= "<input type='checkbox' name='save_pages' id='SaveAndClose' class='radio_vkl' />".
				"<label class='for-checked' for='SaveAndClose'>Закрыть и</label>&nbsp;";
			$ss .= "<input type='submit' class='button' value='Сохранить' />";
		$ss .= "</p></div>";
		$ss .= "<div class='table-cell-middle'><p><a href='$return'>Отмена</a></p></div>";
		$ss .= "</div>";
		return $ss;
	}
	
	function NameGlobalMenu(){
		self::$UserAdmin=json_decode(Cookie::get("user_admin"),true);
		$NameFixed = "";
		if( isset($_GET['id']) ) {
			$table = ( (int)$_GET['id'] < 850 ) ? "bis_cmsmenu" : "bis_cmsservice";
			$result = DB::select("SELECT * FROM $table WHERE sysid={$_GET['id']} LIMIT 1");
			$NameFixed = $result[0]->name;
		}
		$xkTop = "<div class='xk'>";
			$xkTop .= "<div>$NameFixed</div>";
			
			$xkTop .= "<div class='xk-menu'>";
				$xkTop .= "<div>";
					$xkTop .= "<a href='/admin/content' title='На главную'>"
						."<i class='fa fa-home' aria-hidden='true'></i>Главная</a>";
				$xkTop .= "</div>";
				$xkTop .= "<div>";
					$xkTop .= "<a href='/' target='_blank' title='Перейти на сайта'>"
						."<i class='fa fa-desktop' aria-hidden='true'></i>Сайт</a>";
				$xkTop .= "</div>";
				//$xkTop .= $this->Lang();
				$xkTop .= "<div><i class='fa fa-user-o' aria-hidden='true'></i> ". self::$UserAdmin['name'] ."</div>";
				$xkTop .= "<div>";
					$xkTop .= "<a href='/admin/exit' id='exit' title='Закрыть администрирование'>"
						."<i class='fa fa-sign-out' aria-hidden='true'></i>Выход</a>";
				$xkTop .= "</div>";
			$xkTop .= "</div>";
		$xkTop .= "</div>";
		return $xkTop;
	}
	
	function Lang(){
		$ss = "";
		$ctg = $this->dbBlog->f_select("SELECT * FROM lang WHERE hide='show' ORDER BY pos");
		if($ctg->num_rows>0){
			$ss .= "<div><select name='lang' id='lang'>";
			while($cat = $ctg->fetch_assoc()){
				if(!isset($_SESSION['LANG'])) $_SESSION['LANG']=$cat['lang'];
				$sel = (isset($_SESSION['LANG']) && $cat['lang']==$_SESSION['LANG'])?"selected":"";
				$ss .= "<option value='{$cat['lang']}' $sel>{$cat['name']}</option>";
			}
			$ss .= "</select></div>";
		}
		return $ss;
	}
	
	function MenuSheet($MassParam,$Sysyid,$url) {
		$findpages = $ss = "";
		$result = DB::select("SELECT * FROM bis_cmsmenu WHERE sysid={$MassParam['id']} LIMIT 1");
		self::$FeatureGlobalMenu = $result[0];
		if(self::$FeatureGlobalMenu->modul=='articles' || self::$FeatureGlobalMenu->modul=='categories'){
			$ss .= self::MenuArticles($MassParam, $Sysyid, 0, $url,1);
		}
		$result = DB::select("SELECT * FROM module WHERE hide='show' ORDER BY pos");
		$ss .= "<ul class='menusheet'>";
		foreach($result as $cat){
			if(strpos(self::$FeatureGlobalMenu->setting,"#{$cat->modul}#")!==false && $Sysyid>=$cat->only){
				$href = "/admin/content/{$cat->modul}/?sysid=$Sysyid&".http_build_query($MassParam);
				$class = (in_array($cat->modul,$url))?"current user-select":"";
				$ss .= "<li class='$class'><a class='$class' href='$href'>{$cat->name}</a></li>";
			}
		}
		$href = "/admin/content/slider/?sysid=$Sysyid&". http_build_query($MassParam);
		$class = in_array('slider',$url)?"current user-select":"";
		if(self::$FeatureGlobalMenu->slider_pages>0 && $Sysyid < 1000){
			$ss .= "<li class='$class'><a class='$class' href='$href'>Слайдер/Заставка</a></li>";
		} elseif (self::$FeatureGlobalMenu->slider_cat>0 && $Sysyid < 5000){
			$ss .= "<li class='$class'><a class='$class' href='$href'>Слайдер/Заставка</a></li>";
		} elseif (self::$FeatureGlobalMenu->slider_list > 0 && $Sysyid >= 5000){
			$ss .= "<li class='$class'><a class='$class' href='$href'>Слайдер/Заставка</a></li>";
		}
		$href = "/admin/content/seo/?sysid=$Sysyid&". http_build_query($MassParam);
		$class = (in_array('seo',$url))?"current user-select":"";
		$ss .= "<li class='$class'><a class='$class' href='$href'>SEO</a></li>";
		$ss .= "</ul>";
		return $ss;
	}
	
	function MenuArticles($MassParam, $Sysyid, $ParentCat, $url, $InSheet=0) {
		$ss = "";
		if($InSheet==1) unset($MassParam['sysid']);
		$QUERYSTRING=http_build_query($MassParam);
		if(!is_object(self::$FeatureGlobalMenu)){
			$ctg = DB::select("SELECT * FROM bis_cmsmenu WHERE sysid={$MassParam['id']} LIMIT 1");
			self::$FeatureGlobalMenu = $ctg[0];
		}
		$date = (strpos(self::$FeatureGlobalMenu->setting_dop,"#data#") !== false && 
			self::$FeatureGlobalMenu->modul=='articles')?1:0;
		$ss .= "<ul class='menu-articles'>";
			$class = in_array('articles',$url)?"current user-select":"";
			$href = "/admin/content/articles/?$QUERYSTRING";
			$ss .= "<li class='$class menu-articles-nav'><a class='$class' href='$href'>Все страницы</a></li>";
			if(self::$FeatureGlobalMenu->firstKol > 0) {
				$m_=$MassParam;
				unset($m_['parent'],$m_['pages']);
				$class = in_array('categories',$url)?"current user-select":"";
				$href = "/admin/content/categories/?". http_build_query($m_);
				$ss .= "<li class='$class menu-articles-nav'><a class='$class' href='$href'>Категории</a></li>";
			}
			if( $InSheet == 0 ) {
				$class = (self::$FeatureGlobalMenu->modul=='sheet' && $Sysyid == $MassParam['id'])?"current user-select":"";
				$href = "/admin/content/sheet/?sysid=$Sysyid&$QUERYSTRING";
				$ss .= "<li class='$class menu-articles-nav'><a class='$class' href='$href'>Описание</a></li>";
				$ss .= "<li class='menu-articles-add'>".
					"<button class='button newPages' type='button' data-date='$date' data-parent='$ParentCat' data-level='{$MassParam['id']}' ".
					"id='ButtonNewPages'>Добавить</button></li>";
				/*
				if( in_array('articles',$url) ) {
					$ss .= "<li>";
						$ss .= "<button type='button' class='button current filterbutton filterGo'>Поиск</button>";
					$ss .= "</li>";
					$href = URILOCALMODUL . "changeCategory/?". QUERYSTRINGCHANGE;
					if( $this->FeatureGlobalMenu['firstKol'] > 0 ){
						$ss .= "<li>";
							$ss .= "<a class='button current' href='$href'>Сменить категорию</a>";
						$ss .= "</li>";
					}
				}
				*/
			}
		$ss .= "</ul>";
		return $ss;
	}
	
	function NameTranslitDubl($nameT,$sysid,$level=0) {
		if($level==0){
			$ctg = DB::select("SELECT sysid FROM bis_cmsmenu WHERE translit='$nameT' && sysid != $sysid LIMIT 1");
		}else{ 
			$ctg = DB::select("SELECT sysid FROM bis_pages WHERE translit='$nameT' && sysid != $sysid && level=$level LIMIT 1");
			if( empty($ctg) ) { 
				$ctg = DB::select("SELECT sysid FROM bis_categories WHERE translit='$nameT' && sysid != $sysid && level=$level LIMIT 1");
			}
		}
		if(!empty($ctg)) return $nameT .'-'. $sysid; 
		return $nameT;
	}
	
	public function InsertUpdateInfo($sysid,$anons,$info) {
		$ctg = DB::select("SELECT sysid from bis_info WHERE parent = $sysid LIMIT 1");
		if(!empty($ctg)) {
			DB::update("update bis_info set info='$info', anons='$anons' WHERE parent=$sysid LIMIT 1");
		} else {
			DB::insert("insert into bis_info(parent,info,anons) values($sysid,'$info','$anons')");
		}
		return;
	}
	public function InsertSeo($sysid,$nameh1){
		$ctg = DB::select("SELECT sysid FROM bis_pagesSeo WHERE parent = $sysid LIMIT 1");
		if(empty($ctg)) {
			DB::insert("INSERT INTO bis_pagesSeo(parent,seo_title,seo_desc,seo_key) values($sysid,'$nameh1','$nameh1','$nameh1')");
		}
		return;
	}

	public function Mass_Translit(){
		$ctg=DB::select("SELECT * FROM translit");
		foreach($ctg as $cat){
			self::$en[$cat->rus]=$cat->en;
		}
	}
	public function Translit_Global($value){
		self::Mass_Translit();
		$str = mb_strtolower($value,'UTF-8');
		$str = trim(strtr($str,self::$en),'-');
		$str = preg_replace("~(\\\|\*|\?|\[|\?|\]|\(|\\\$|\))~", "",$str);
		$str = str_replace('#039','',$str);
		$str = str_replace('&nbsp;',' ',$str);
		$str = mb_ereg_replace('[^-0-9a-z]','-',$str);
		$str = mb_ereg_replace('[-]+','-',$str);
		return  str_replace(chr(194).chr(160),'',$str);
	}
	public function PreviewLogoArticlesCategories( $parent ) {
		$ss = '';
		$ctg = DB::select("SELECT photo FROM bis_pagesLogoAll WHERE parent=$parent LIMIT 1");
		if(!empty($ctg)){
			$ss = "<img src='/files/logo/".$ctg[0]->photo."' alt='Логотип' />";
		}
		return "<div class='for-logo'>$ss</div>";
	}
	public function DecodeName($ss){ return htmlspecialchars_decode(str_replace('"',"&quot;",$ss),ENT_QUOTES);}
	public function DecodeTitle($text){
		$text = htmlspecialchars_decode($text,ENT_QUOTES);
		return str_replace('"','»',preg_replace('/((^|\s)"(\w))/um', '\2«\3',$text));
	}

} //END CLASS
?>