<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Validator;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$cookie = Cookie::get("user_admin");
        if(!empty($cookie) && !is_null($cookie) && $cookie!='null'){
			return redirect()->route('admin.content');
		}else{
			return view('admin/dashboard');
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$validator = Validator::make($request->all(),[
			'pass' => 'required',
			'login' => 'required',
		]);
		//dump($validator);
        if($validator->fails()) return "alert('Проверьте логин или пароль');";
		$result = DB::table('userlist')->where([
			'name'=>$request->login,
			'pass'=>md5($request->pass),
			'hide'=>'show'
		])->first();
		if(!empty($result)){
			$cookie = Cookie::queue("user_admin",
				json_encode($result,JSON_UNESCAPED_UNICODE ^ JSON_UNESCAPED_SLASHES),
				480
			);
			return "location.reload(true);";
		}
		return "alert('Проверьте логин или пароль');";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        Cookie::queue('user_admin','');
		return redirect()->route('admin.start');
    }
}
