<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class AdminIndexController extends Controller
{
    public function index()
    {
		if(self::Is_Auth()) return view('admin.index');
		return redirect()->route('admin.start');
    }
	
	static function Is_Auth(){
		$cookie = Cookie::get("user_admin");
        if(empty($cookie) || is_null($cookie) || $cookie=='null') return false;
		return true;
	}

    public function show(Request $request,$module)
    {
		if(!self::Is_Auth()) return redirect()->route('admin.start');
		$MassParam=$request->all();
		$uri = explode('/',$request->path());
        /*
        $KOLDANN=isset($_SESSION['PANIGACIAKOL'])? $_SESSION['PANIGACIAKOL'] : config("app.LIMITADMIN");
        $NOMPAGES=isset($MassParam['pages']) ? (int)$MassParam['pages'] : 0;
        $LIMIT=$KOLDANN==0 ? "" : " LIMIT ".($NOMPAGES*$KOLDANN).",$KOLDANN";
        */
        return view("admin.$module.index",['MassParam'=>$MassParam,'url'=>$uri]);
    }

}
