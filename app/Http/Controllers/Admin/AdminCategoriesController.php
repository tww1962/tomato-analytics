<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Admin\AdminGlobalController as General;

class AdminCategoriesController extends Controller
{
	private $NamePages;
	static $ParentCat;
	static $Sysid;
	static $MassParam;
	private $MassFind;
	static $ChildrenMenu;
	private $ListBrend;
	static $RecordCmsMenu;

	public static function CategoriesAllModulChildren($parent,$orderby){
		$ctg = DB::select("SELECT * FROM bis_categories WHERE parent=$parent ORDER BY $orderby");
		if(!empty($ctg)) {
			self::$ChildrenMenu .= "<ul>";
			foreach($ctg as $cat1){
				$cat=(array)$cat1;
				$sid = $cat['sysid']; $nik = $cat['name'];
				if($cat['hide'] == 'show') { $glaz = "<span class='main-icon'><i class='fa fa-eye' aria-hidden='true' title='Скрыть'></i></span>";} 
				else { $glaz = "<span class='main-icon'><i class='fa fa-eye-slash' aria-hidden='true' title='Отобразить'></i></span>";}
				self::$ChildrenMenu .= "<li id='li$sid' sid='$sid'>";
					self::$ChildrenMenu .= "<div class='all-list-item'>";
						self::$ChildrenMenu .= "<div><span class='main-icon sort'><i class='fa fa-arrows' aria-hidden='true' title='Сортировка'></i></span></div>";
						self::$ChildrenMenu .= "<div><span class='main-icon disclose'><strong class='ui-icon' title='Развернуть/Свернуть'></strong></span></div>";
						self::$ChildrenMenu .= General::PreviewLogoArticlesCategories($sid);
						self::$ChildrenMenu .= "<div><a href='/admin/content/sheet/?sysid=$sid&"
							.http_build_query(self::$MassParam) ."'>$nik</a></div>";
						self::$ChildrenMenu .= "<div>";
							self::$ChildrenMenu .= "<div data-sid='$sid'>";
								self::$ChildrenMenu .= "<span title='Добавить подраздел' class='main-icon plus newPages' "
									."data-level='".self::$MassParam['id']."' data-parent='$sid' data-id='#li$sid>ul'>"
									."<i class='fa fa-plus' aria-hidden='true'></i></span>";
								if(strpos(self::$RecordCmsMenu['setting_dop'],"#inGlCat#") !== false) {
									$cl = ( $cat['inGl'] == 1 ) ? "current" : "";
									self::$ChildrenMenu .= "<span class='main-icon $cl' title='На главную'><i class='fa fa-bolt'></i></span>";
								}
								if(strpos(self::$RecordCmsMenu['setting_dop'],"#inNewCat#") !== false) {
									$cl = ( $cat['inNew'] == 1 ) ? "current" : "";
									self::$ChildrenMenu .= "<span class='main-icon $cl' title='Новинка'><i class='fa fa-paper-plane'></i></span>";
								}
							self::$ChildrenMenu .="</div>";
							self::$ChildrenMenu .= "<div data-sid='$sid' data-name='$nik' data-parent='#li$sid'>";
								self::$ChildrenMenu .= $glaz;
								self::$ChildrenMenu .= "<span class='main-icon'><i class='fa fa-trash-o' aria-hidden='true' title='Удалить'></i></span>";
							self::$ChildrenMenu .="</div>";
						self::$ChildrenMenu .= "</div>";
					self::$ChildrenMenu .="</div>";
					self::$ChildrenMenu .= self::CategoriesAllModulChildren($sid,$orderby);
				self::$ChildrenMenu .= "</li>";
			}
			self::$ChildrenMenu .= "</ul>";
		}
	}
	function VariablesCategories($FeatureGlobalMenu,$sysid,$MassParam,$ParentCat){
		self::$RecordCmsMenu = (array)$FeatureGlobalMenu;
		self::$Sysid=$sysid;
		self::$MassParam=$MassParam;
		self::$ParentCat=$ParentCat;
	}
	
} // END
