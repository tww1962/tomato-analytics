<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminGlobalController as General;
use Illuminate\Support\Facades\DB;

class AdminSettingController extends Controller
{

    public function save(Request $request,$modul){
        switch($modul){
            case 'stop':
                return $this->SettingStopSave($request);
        }
    }

    function SettingStop($MassParam) {
		$ctg = DB::select("SELECT nav FROM bis_cmsservice WHERE sysid={$MassParam['id']} LIMIT 1");
		$cat = (array)$ctg[0]; 
		$vkl = ( $cat['nav'] == 1 ) ? "checked" : "";;
		$ctg = DB::select("SELECT * FROM bis_info WHERE parent={$MassParam['id']} LIMIT 1");
		if(!empty($ctg)) $cat = (array)$ctg[0];
		$info = isset($cat['info'])?htmlspecialchars_decode($cat['info'], ENT_QUOTES):""; 
		$ss = "<form name='frmDannStop' id='frmDannStop'>";
		$ss .= "<div class='for-vkl-vykl'><input type='checkbox' class='vkl_vykl' id='vkl_vykl' name='vkl' $vkl />"
            ."<label for='vkl_vykl'>Профилактика</label></div>";
		$ss .= "<textarea name='info' id='info' class='texarea-none'>$info</textarea>";
		$ss .= "<input type='hidden' name='sysid' value='{$MassParam['id']}' />";
        $ss .= "<input type='hidden' name='return' value='/admin/content' />";
        $ss .= General::GlobalSubmit('/admin/content');
		$ss .= "<input type='hidden' name='_token' value='". csrf_token() ."' /></form>";
		$ss .= "<script>$(function() { $('#info').ckeditor({ height:500 }); });</script>";
		$ss .= "<p>&nbsp;</p><p>&nbsp;</p>";
		return $ss;
	}
    function SettingStopSave($request){
		$id = (int)$request->sysid;
		$info = htmlspecialchars($request->info, ENT_QUOTES);
		$vkl = isset($request->vkl) ? 1 : 0;
		DB::update("update bis_cmsservice set nav=$vkl where sysid=$id LIMIT 1");
		$ctg = DB::select("select sysid from bis_info where parent=$id LIMIT 1");
		if(!empty($ctg)) {
			DB::update("update bis_info set info='$info' where sysid=$id LIMIT 1");
		} elseif( !empty($info) ) {
			DB::insert("insert into bis_info(parent,info) values($id,'$info')");
		}
        $return = isset($request->save_pages)?$request->return:"";
		return response()->json(['success' => true,'msg' => 'Записано','return'=>$return]);
    }

}
