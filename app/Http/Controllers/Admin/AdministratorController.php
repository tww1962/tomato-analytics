<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminGlobalController as General;
use Illuminate\Support\Facades\DB;

class AdministratorController extends Controller
{

    public function save(Request $request){
		$id_user = (int)$request->id_user;
		$name = General::DecodeTitle($request->name);
		$p1 = trim($request->pass);
		$p2 = trim($request->passOld);
		$msg = ( empty($name) ) ? "<p>Нет имени</p>" : "";
		if( $id_user == 0 ){
            $msg .= ( $p1 != $p2 || empty($p1) || empty($p2) ) ?  "<p>Пароли не совпадают</p>" : "";
		} elseif ( !empty($p1) ) {
			$msg .= ( $p1 != $p2 ) ?  "<p>Пароли не совпадают</p>" : "";
		}
		if( !empty($msg) ) response()->json(['success' => false,'msg' => $msg]);
		
		$root = isset($request->root) ? 'yes' : 'no';
		$edit = isset($request->edit) ? 'yes' : 'no';
		$dostup = [];
		foreach( $request->role as $val ) { $dostup[] = $val; }
		$dd = implode('#',$dostup);
		if($id_user == 0) { 
			$id_user = DB::table('userlist')->insertGetId([
                'name'=>$name,
                'pass'=>md5($p1),
                'root'=>$root,
                'dostup'=>$dd,
                'edit'=>$edit
            ]);
		} else {
			if( !empty($p1) )
				DB::update("update userlist set name='$name', pass='".md5($p1)."', root='$root', dostup='$dd', edit='$edit' where id_user=$id_user");
			else
				DB::update("update userlist set name='$name', root='$root', dostup='$dd', edit='$edit' where id_user=$id_user");
		}
		DB::delete("DELETE FROM userlist_city WHERE parent=$id_user");
		if(isset($request->org)){
			foreach ($request->org as $m){ DB::insert("INSERT INTO userlist_city(parent,id) VALUES($id_user,'$m')"); }
		}
        $return = isset($request->save_pages)?$request->return:"";
        return response()->json(['success' => true,'msg' => 'Записано','return'=>$return,'idUser'=>$id_user]);
    }

    function RoleUserDopKorr($id_user,$MassParam){
		$dostup = []; 
		$chRoot = $chEdit = $name = '';
		if( $id_user != 0 ) {
			$ctg = DB::select("SELECT * FROM userlist WHERE id_user=$id_user");
			$cat = (array)$ctg[0];
			$name = $cat['name'];
			$dostup = explode('#',$cat['dostup']);
			$chRoot = ( $cat['root'] == 'yes' ) ? "checked" : '';
			$chEdit = ( $cat['edit'] == 'yes' ) ? "checked" : '';
		}
		$ss = "<form class='administrator' id='frmUser'>";
		$ss .= "<div class='edit-admin'>";
			$ss .= "<div><input class='input' placeholder='Имя*' required type='text' value='$name' name='name' /></div>";
			$ss .= "<div><input class='input' placeholder='Пароль*' type='text' value='' name='pass' /></div>";
			$ss .= "<div><input class='input' placeholder='Повторите пароль*' type='text' value='' name='passOld' /></label></div>";
		$ss .= "</div>";
		$ss .= self::Organizations($id_user);
		$ss .= "<ul>";
		$ss .= "<li><p><input type='checkbox' name='root' $chRoot class='vkl_vykl' id='c2' /><label for='c2'>Супер администратор (права на добавление/корректировку администраторов)</label></p></li>";
		$ss .= "<li style='display:none'><p><input type='checkbox' name='edit' $chEdit class='vkl_vykl' id='c3' /><label for='c3'>Право редактирования текста в «живую» на сайте</label></p></li>";
		$ss .= "<li><p style='padding: 15px 0;'><input type='radio' onclick=\"$('input[list=\'radioAll\']').prop('checked',true);\" name='radioAll' class='radio_vkl' id='c4' /><label for='c4'>включить все </label>";
		$ss .= "<input type='radio' onclick=\"$('input[list=\'radioAll\']').prop('checked',false);\" name='radioAll' class='radio_vkl' id='c5' /><label for='c5'>отключить все </label></p></li>";
		$k = 5;
		$tablesUser[0] = "bis_cmsmenu";
		$tablesUser[1] = "bis_cmsservice";
		foreach( $tablesUser as $table ){
			$ctg = DB::select("select name, sysid from $table where parent=0 and hide='show' order by pos");
            foreach($ctg as $cat){
				$k++;
                $sid=$cat->sysid;
				$ch = in_array($sid,$dostup )? "checked" : '';
				$ss .= "<li><p><input type='checkbox' name='role[]' value='$sid' $ch "
                    ."id='cr$sid' class='parent-role role vkl_vykl' list='radioAll' />"
					."<label for='cr$sid'>".$cat->name."</label></p>";
				$ss .= self::RoleUserDopKorrPopup($sid,$dostup,$table);
				$ss .= "</li>";
			}
		}
		$ss .= "</ul>";
        $ss .= General::GlobalSubmit("/admin/content/user?" . http_build_query($MassParam));
        $ss .= "<input type='hidden' name='return' value='/admin/content/user?" . http_build_query($MassParam) ."' />";
        $ss .= "<input type='hidden' name='_token' value='". csrf_token() ."' />";
        $ss .= "<input type='hidden' name='id_user' id='idUser' value='$id_user' /></form>";
		return $ss;
    }
    private function RoleUserDopKorrPopup($parent, $dostup,$table){
		$ss = '';
		$ctg = DB::select("select name, sysid from $table where parent=$parent and hide='show' order by pos");
		if( !empty($ctg) ){
			$ss = "<ul>";
			foreach($ctg as $cat1) {
                $cat=(array)$cat1;
				$ch = ( in_array($cat['sysid'], $dostup ) ) ? "checked" : '';
				$ss .= "<li><p><input type='checkbox' name='role[]' value='{$cat['sysid']}' $ch "
                    ."id='ch{$cat['sysid']}' class='children role vkl_vykl' list='radioAll' />"
                    ."<label for='ch{$cat['sysid']}'>{$cat['name']}</label></p></li>";
			}
			$ss .= "</ul>";
		}
		return $ss;

    }

    function RoleUserDann($MassParam){
        $ss = "";
		$ctg = DB::select("SELECT * FROM userlist WHERE superroot='no' order by name");
        if(!empty($ctg)){
            $ss = "<ul class='all-list general-ul' id='list-user' data-tables='userlist'>";
            foreach($ctg as $cat){
                $sid = $cat->id_user; $nik = $cat->name;
                if($cat->hide == 'show') $glaz = "<span class='main-icon'><i class='fa fa-eye' aria-hidden='true' title='Отключить: $nik'></i></span>";
                else $glaz = "<span class='main-icon'><i class='fa fa-eye-slash' aria-hidden='true' title='Активировать: $nik'></i></span>";
                $ss .= "<li id='li$sid'>";
                    $ss .= "<div class='all-list-item'>";
                        $ss .= "<div><a href='/admin/content/user?edit=$sid&". http_build_query($MassParam) ."' title='Редактировать'>$nik</a></div>";
                        $ss .= "<div data-sid='$sid' data-name='$nik' data-parent='#user$sid'>";
                            $ss .= "$glaz<span class='main-icon'><i class='fa fa-trash-o' aria-hidden='true' title='Удалить: $nik'></i></span>";
                        $ss .="</div>";
                    $ss .="</div>";
                $ss .= "</li>";
            }
            $ss .= "</ul>";
        }
		return $ss;
	}

	public function Organizations($id_user){
		$ss = "";
		$ctg = DB::select("SELECT * FROM `Organizations` WHERE Conception='ТиЧ' ORDER BY OrganizationName");
		foreach($ctg as $cat1){
			$cat=(array)$cat1;
			/*
			if($cat['Id']=='00000000-0000-0000-0000-000000000006' ||
				$cat['Id']=='00000000-0000-0000-0000-100000000025') continue;
			*/
			$checked=self::Organizations_Yes($cat['Id'],$id_user);
			$name=General::DecodeTitle($cat['OrganizationName']);
			$ss .= "<div style='margin-top:7px'><input type='checkbox' $checked value='{$cat['Id']}' "
				."name='org[]' id='org{$cat['sysid']}' />"
				."&nbsp;<label for='org{$cat['sysid']}'>$name</label>";
		}
		$name_razdel=$id_user<1000000000?"Города под управлением":"Доступные точки продаж";
		return "<div class='organization' style='padding:10px 0'>"
		."<p><strong>$name_razdel</strong></p>$ss</div>";
	}
	private function Organizations_Yes($Id,$id_user){
		$ctg = DB::select("SELECT sysid FROM userlist_city WHERE "
		."id='$Id' && parent=$id_user LIMIT 1");
		if(!empty($ctg)) return "checked";
		return "";
	}


}
