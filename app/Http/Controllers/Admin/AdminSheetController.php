<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
//use App\Http\Controllers\Admin\AdminGlobalController as General;

class AdminSheetController extends Controller
{
	public static $Sysid;
	public static $MassParam;
	public static $Table;
	public static $RecordCmsMenu;
	public static $Cat;
	static $ChildrenMenu;
	static $nameCatActive="- Выбор категории -";

	function CategoriesSelect(){
		///$for - list - для смены раздела при списке, pages - для смены раздела в корр.страницы
		self::$ChildrenMenu = "";
		self::CategoriesSelectItem(self::$MassParam['id']);
		$ss = "<p>Категория</p>";
		$ss .= "<div class='select-categories' style='padding: 0 0 15px'>";
			$ss .= "<div class='select-categories-item' >";
				$ss .= "<div class='active-categories input'>".self::$nameCatActive."</div>";
				$ss .= "<div class='list-categories'>";
					$ss .= "<div class='from-input-categories'>";
						$ss .= "<input type='text' class='input' autocomplete='off' id='findcat'/>";
					$ss .= "</div>";
					$ss .= self::$ChildrenMenu;
				$ss .= "</div>";
			$ss .= "</div>";
		$ss .= "</div>";
		return $ss;
	}
	function CategoriesSelectItem($parent){
		$ctg = DB::select("SELECT name,sysid FROM bis_categories WHERE parent=$parent ORDER BY pos ");
		if(!empty($ctg)) {
			self::$ChildrenMenu .= "<ul data-return='#sidcategories'>";
			foreach($ctg as $cat){
				if($cat->sysid == self::$Cat['parent']) self::$nameCatActive = $cat->name;
				self::$ChildrenMenu .= "<li><a href='javascript:void(0)' data-sid='".$cat->sysid."'>".$cat->name."</a>";
				self::CategoriesSelectItem($cat->sysid);
				self::$ChildrenMenu .= "</li>";
			}
			self::$ChildrenMenu .= "</ul>";
		}
	}

	function VariablesPages($FeatureGlobalMenu,$sysid,$MassParam){
		self::$RecordCmsMenu = $FeatureGlobalMenu;
		self::$Sysid=$sysid;
		self::$MassParam=$MassParam;
		if(self::$Sysid < 1000){
			self::$Table = "bis_cmsmenu";
			self::$Cat=self::$RecordCmsMenu;
		}elseif(self::$Sysid < 5000){
			self::$Table = "bis_categories";
		}else{
			self::$Table = "bis_pages";
		}
		if(self::$Sysid >= 1000 && self::$Sysid<1000000000){
			$ctg = DB::select("SELECT * FROM ".self::$Table." WHERE sysid=$sysid LIMIT 1");
			self::$Cat=(array)$ctg[0];
		}
	}
	function Role($m){
		$ss = "";
		$ctg = DB::select("SELECT role FROM bis_user_role ORDER BY sysid");
		$k=1;
		if(!empty($ctg)){
			foreach($ctg as $cat){
				$ch = in_array($cat->role,$m)?"checked":"";
				$ss .= "<div class='flex flex-align-center' style='margin:0 20px 20px 0'>"
					."<input $ch type='checkbox' name='role[]' id='role_$k' value='".$cat->role."' />"
					."<label for='role_$k' style='margin-left:5px'>".$cat->role."</label></div>";
				$k++;
			}
			return "<div class='flex flex-wrap flex-align-center'>"
				."<p style='width:100%;padding-top:15px;font-weight:bold'>Какие роли имеею доступ:</p>$ss</div>";
		}
		return "";
	}
	function DannLogo(){
		if(self::$Sysid < 1000){ 
			$logo = [
				'inlogo'=>self::$RecordCmsMenu['logo_pages'],
				'sizelogo'=>self::$RecordCmsMenu['size_logo_pages']
			];
		} elseif(self::$Sysid < 5000){
			$logo = [
				'inlogo'=>self::$RecordCmsMenu['logo_cat'],
				'sizelogo'=>self::$RecordCmsMenu['size_logo_cat']
			];
		} else {
			$logo = [
				'inlogo'=>self::$RecordCmsMenu['logo_list'],
				'sizelogo'=>self::$RecordCmsMenu['size_logo_list']
			];
		}
		if($logo['inlogo'] == 1 && !empty($logo['sizelogo'])) { // логотип страницы
			return self::LogoAllModul(self::$Sysid,$logo['sizelogo']);
		}
	}
	function LogoAllModul($parent, $size) {
		$r = mt_rand(1000,5000);
		$wh = explode('#',$size);
		$ctg = DB::select("SELECT * FROM bis_pagesLogoAll WHERE parent=$parent LIMIT 1");
		if(!empty($ctg)) $cat = (array)$ctg[0];
		$sysid = !isset($cat)?0:(int)$cat['sysid'];
		$photo = !isset($cat)?"":$cat['photo'];
		$ss = "<form name='frmLogo' id='frmLogo' enctype='multipart/form-data'>";
		$ss .= "<input style='display:none' type='file' accept='image/png, image/jpeg' name='photo' id='fotoInput' />";
		$ss .= "<p>Логотип (рекомендуемый размер {$wh[0]} px на {$wh[1]} px)</p>";
		$w_div = (int)$wh[0] + 2;
		$h_div = (int)$wh[1] + 2;
		$ss .= "<div id='fotoImg' class='photo-logo-modul' style='width:".$w_div."px; height:".$h_div."px;'>";
		if(file_exists($_SERVER['DOCUMENT_ROOT'] ."/files/logo/$photo") && !empty($photo) ) {
			$style='block';
			$size_img = getimagesize($_SERVER['DOCUMENT_ROOT'] ."/files/logo/$photo" );
			$ss .= "<img src='/files/logo/$photo?".filemtime("./files/logo/$photo")."' />";
		} else { $style= 'none'; }
		$ss .= "</div>";
		
		$ss .= "<div class='button-logo'>";
			$ss .= "<span class='main-icon' id='frmlogo_download'>"
				."<i class='fa fa-download' aria-hidden='true' title='Загрузить'></i></span>";
			$ss .= "<span class='main-icon' style='display: $style' id='frmlogo_cropp'>".
				"<i class='fa fa-crop' aria-hidden='true' title='Подогнать'></i></span>";
			$ss .= "<span class='main-icon' id='frmlogo_del' style='display: $style'>".
				"<i class='fa fa-trash-o' aria-hidden='true' title='Удалить'></i></span>";
		$ss .= "</div>";
		$ss .= "<input type='hidden' name='width_height_img' id='width_height_img' value='$size' />";
		$ss .= "<input type='hidden' id='frmlogo_flag' name='flag' value='1' />";
		$ss .= "<input type='hidden' name='sysid' id='sysid_photo' value='$sysid' /><input type='hidden' name='parent' value='$parent' />";
		$ss .= "<input type='hidden' name='_token' value='". csrf_token() ."' /></form>";
		return $ss;
	}
	function InfoPagesAllModul() {
		$ctg = DB::select("SELECT * FROM bis_info WHERE parent=".self::$Sysid." LIMIT 1");
		$ss = array("","");
		if(!empty($ctg)){
			$cat = (array)$ctg[0]; 
			$ss[0] = htmlspecialchars_decode($cat['anons'], ENT_QUOTES);
			$ss[1] = htmlspecialchars_decode($cat['info'], ENT_QUOTES);
		}
		return $ss;
	}
	function Sheet_Anons($anons){
		if(strpos(self::$RecordCmsMenu['setting_dop'],"#anonsp#")!==false && self::$Table=="bis_cmsmenu"){
			return self::Sheet_Anons_Textarea($anons);
		}elseif(strpos(self::$RecordCmsMenu['setting_dop'],"#anonsl#")!==false && self::$Table=="bis_pages"){
			return self::Sheet_Anons_Textarea($anons);
		}elseif(strpos(self::$RecordCmsMenu['setting_dop'],"#anonsc#")!==false && self::$Table=="bis_categories"){
			return self::Sheet_Anons_Textarea($anons);
		}
	}
	function Sheet_Anons_Textarea($anons){
		return "<p class='for-edit'>Описание</p>"
		."<textarea name='anons' id='anons' class='main-ckeditor'>$anons</textarea>";
	}
	
} // END
