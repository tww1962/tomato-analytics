<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class AdminSheetLogoController extends Controller
{
    private $basePath;
    public function __construct(){
        $this->basePath = public_path('files/logo');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(isset($request->flag)){
            if((int)$request->flag==1) return $this->create($request);
            else return $this->destroy((int)$request->sysid);
        } elseif(isset($request->imgUrl)){
            return $this->update($request);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($request)
    {
        $allowTypes = 'jpeg,jpg,png,gif,webp';
        $validator = Validator::make($request->all(), [
            'photo.*' => ['required', 'mimes:' . $allowTypes, 'max:' . (5*1024*1024)],
        ], [], ['photo' => 'file']);
        if ($validator->fails()) {
            $msg = $validator->errors()->first();
            return response()->json(['success' => false,'msg' => $msg]);
        }
        if (!$request->hasFile('photo')) return response()->json(['success' => false, 'msg' => 'Файл не выбран']);

        $ctg=DB::select("SELECT * FROM bis_pagesLogoAll WHERE parent=". $request->parent);
        foreach($ctg as $cat){ @unlink($this->basePath ."/". $cat->photo); }
        $ctg=DB::delete("DELETE FROM bis_pagesLogoAll WHERE parent=". $request->parent);
        $photo = $request->file('photo');
        $namePhoto="img". str_replace('.','',(string)microtime(true)) .".". $photo->getClientOriginalExtension();
        $photo->move($this->basePath,$namePhoto);
        $id=DB::table("bis_pagesLogoAll")->insertGetId(['parent'=>$request->parent,'photo'=>$namePhoto]);
        return response()->json([
            'success' => true,
            'msg'     => "/files/logo/". $namePhoto,
            'id'      => $id
        ]);
    }

    public function update($request)
    {
        $imgUrl = $request->imgUrl;
        $imgUrlM = explode("?",$imgUrl);
        $imgUrl = $imgUrlM[0];
        // original sizes
        $imgInitW = $request->imgInitW;
        $imgInitH = $request->imgInitH;
        // resized sizes
        $imgW = $request->imgW;
        $imgH = $request->imgH;
        // offsets
        $imgY1 = $request->imgY1;
        $imgX1 = $request->imgX1;
        // crop box
        $cropW = $request->cropW;
        $cropH = $request->cropH;
        // rotation angle
        $angle = $request->rotation;
        $fotoName = basename($imgUrl);
        $imgUrl=$_SERVER['DOCUMENT_ROOT'] . $imgUrl;
        $what = getimagesize($imgUrl);
        $rr = strtolower($what['mime']);
        switch($rr)
        {
            case 'image/png':
                $source_image = imagecreatefrompng($imgUrl);
                $type = '.png';
                break;
            case 'image/jpeg':
                $source_image = imagecreatefromjpeg($imgUrl);
                $type = '.jpg';
                break;
            case 'image/gif':
                $source_image = imagecreatefromgif($imgUrl);
                $type = '.gif';
                break;
            default: 	
                return response()->json(["status" => 'error', "message" => "Неизвестный тип файла. $imgUrl - ".(print_r($what,true))]);	
                exit;
        }
        $output_filename = $imgUrl; //   это новый файл
        $resizedImage = imagecreatetruecolor($imgW, $imgH);
        $white = imagecolorallocate($resizedImage, 255, 255, 255);
        imagefill($resizedImage, 0, 0, $white);
        imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
        if( $angle != 0 ) {
            $rotated_image = imagerotate($resizedImage, -$angle, $white);
            // новая высота и ширина повернутой картинки
            $rotated_width = imagesx($rotated_image);
            $rotated_height = imagesy($rotated_image);
            // различие между вращающимися и оригинальных размеров
            $dx = $rotated_width - $imgW;
            $dy = $rotated_height - $imgH;
            // поворачиваемое изображение, чтобы вписаться в оригинальный размер
            $cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
            $white = imagecolorallocate($cropped_rotated_image, 255, 255, 255);
            imagefill($cropped_rotated_image, 0, 0, $white);
            ///imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
            imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
        } else { $cropped_rotated_image = $resizedImage; }
        // обрезать изображение в выбранной области
        $final_image = imagecreatetruecolor($cropW, $cropH);
        $white = imagecolorallocate($final_image, 255, 255, 255);
        imagefill($final_image, 0, 0, $white);
        imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
        
        @unlink($imgUrl); //  удаляю старый
        switch($rr)
        {
            case 'image/png':
                imagepng($final_image, $output_filename);
                break;
            case 'image/jpeg':
                imagejpeg($final_image, $output_filename);
                break;
            case 'image/gif':
                imagegif($final_image, $output_filename);
                break;
        }
        return response()->json(["status" => 'success',"url" => "/files/logo/$fotoName?". mt_rand(5000,10000)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ctg=DB::select("SELECT * FROM bis_pagesLogoAll WHERE sysid=$id");
        if(!empty($ctg)) unlink($this->basePath ."/". $ctg[0]->photo);
        DB::delete("DELETE FROM bis_pagesLogoAll WHERE sysid=$id");
        return response()->json(['success' => true]);
    }
}
