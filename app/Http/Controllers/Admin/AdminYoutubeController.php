<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Admin\AdminGlobalController as General;

class AdminYoutubeController extends Controller
{

    public function index(Request $request,$modul){
        $this->Modul=$modul;
        $this->Parent=(int)$request->parent;
        switch($modul){
			case 'youtube-new': //новый
				return $this->YoutubeUpload($request);
				break;
			case 'youtube-name': //корректировка названия
				return $this->YoutubeName($request);
				break;
			default: return "";
		}
    }

	private function YoutubeName($request){
		$name = General::DecodeTitle($request->name);
		$sid = (int)$request->sid;
        if( !empty($name) && !empty($key) ) return response()->json(['success' => false,'msg' => 'Нет обязательных данных']);
		DB::update("UPDATE bis_pagesYoutube SET name='$name' WHERE sysid=$sid LIMIT 1");
        return response()->json(['success' => true,'msg' => $name]);

	}

    private function YoutubeUpload($request){
		$parent = (int)$request->parent;
		$name = General::DecodeTitle($request->name);
		$key = $request->nameY;
		$nameY = "//www.youtube.com/embed/$key?rel=0";
		if( !empty($name) && !empty($key) ) {
			$FOTO = "img" . str_replace('.','',(string)microtime(true)) . ".jpg";
			@copy("http://img.youtube.com/vi/$key/0.jpg", public_path('files/youtube')."/$FOTO");
			$FOTO = file_exists(public_path('files/youtube')."/$FOTO")? $FOTO : "";
			DB::insert("INSERT INTO bis_pagesYoutube(parent,name,data_add,foto,href) values($parent,'$name','". date('Y-m-d H:i:s') ."','$FOTO','$nameY')");
            return response()->json(['success' => true,'msg' =>'']);
		}
        return response()->json(['success' => false,'msg' => 'Нет обязательных данных']);
    }

    function YoutubeList($parent){
        $ss = "";
        $ctg = DB::select("SELECT * FROM bis_pagesYoutube WHERE parent=$parent ORDER BY pos");
        if(!empty($ctg)){
			$ss .= "<ul id='sortable' class='sortableRazdel general-ul all-list' data-tables='bis_pagesYoutube'>";
			foreach($ctg as $cat1){
                $cat=(array)$cat1;
				$sid = $cat['sysid']; $nik = $cat['name'];
				if($cat['hide'] == 'show') { $glaz = "<span class='main-icon'><i class='fa fa-eye' aria-hidden='true' title='Скрыть'></i></span>";} 
				else { $glaz = "<span class='main-icon'><i class='fa fa-eye-slash' aria-hidden='true' title='Отобразить'></i></span>";}
				$ss .= "<li id='li$sid' sid='$sid'>";
					$ss .= "<div class='all-list-item'>";
						$ss .= "<div><span class='main-icon sort'><i class='fa fa-arrows-v' aria-hidden='true' title='Сортировка'></i></span></div>";
						$ss .= self::LogoYoutube( $cat['foto'] );
						$ss .= "<div id='div$sid'>$nik</div>";
						$ss .= "<div data-sid='$sid' name='$nik' data-parent='#div$sid' class='filesupload'>";
							$ss .= "<span class='main-icon'><i class='fa fa-youtube' aria-hidden='true' title='Просмотр' data-href='{$cat['href']}'></i></span>";
							$ss .= "<span class='main-icon'><i class='fa fa-pencil' aria-hidden='true' title='Редактировать имя'></i></span>";
							$ss .= $glaz;
							$ss .= "<span class='main-icon'><i class='fa fa-trash-o' aria-hidden='true' title='Удалить'></i></span>";
						$ss .="</div>";
					$ss .="</div>";
				$ss .= "</li>";
			}
            $ss .= "</ul>";
        }
        return $ss;
    }

    private function LogoYoutube($photo){
		$ss = "<div class='for-logo'>";
		if( file_exists(public_path('files/youtube')."/$photo") ) {
			$ss .= "<img src='/files/youtube/$photo' alt='Превью' />";
		}
		$ss .= "</div>";
		return $ss;
       
    }
}
