<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Admin\AdminGlobalController as General;
use App\Http\Controllers\Admin\AdminSheetController as Sheet;
use App\Http\Controllers\Admin\AdministratorController as UserAdmin;
use Illuminate\Support\Facades\Validator;

class AdminClientController extends Controller
{

    private static $FeatureGlobalMenu;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($query,$MassParam)
    {
        $ss = "";
		$ctg = DB::select($query);
		$ss .= "<ul class='sortableRazdel general-ul all-list' data-tables='bis_user'>";
        foreach($ctg as $cat1){
		    $cat=(array)$cat1;
			$sid = $cat['sysid']; 
			$fio = General::DecodeTitle($cat['name']) ." ". General::DecodeTitle($cat['surname']) .", д/р {$cat['bdate']}"; 
			if($cat['hide'] == 'show') $glaz = "<span class='main-icon'><i class='fa fa-eye' aria-hidden='true' title='Отключить'></i></span>";
			else $glaz = "<span class='main-icon'><i class='fa fa-eye-slash' aria-hidden='true' title='Активировать'></i></span>";
			
			$ss .= "<li id='li$sid' sid='$sid'>";
				$ss .= "<div class='all-list-item'>";
					$ss .= General::PreviewLogoArticlesCategories($sid);
					$ss .= "<div class='all-list-item-edit'>"
						."<a href='/admin/content/client/?". http_build_query($MassParam) ."&sysid=$sid'>$fio</a>"
						."</div>";
					$ss .= "<div>";
						$ss .= "<div data-sid='$sid' data-name='$fio' data-parent='#li$sid'>";
							$ss .= $glaz;
							$ss .= "<span class='main-icon'><i class='fa fa-trash-o' aria-hidden='true' title='Удалить'></i></span>";
						$ss .="</div>";
					$ss .= "</div>";
				$ss .="</div>";
			$ss .= "</li>";
		}
		$ss .= "</ul>";
        return $ss;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($MassParam)
    {
    	$result = DB::select("SELECT * FROM bis_cmsmenu WHERE sysid={$MassParam['id']} LIMIT 1");
	    self::$FeatureGlobalMenu = (array)$result[0];
        Sheet::VariablesPages(self::$FeatureGlobalMenu,$MassParam['sysid'],$MassParam);
		$ctg = DB::select("SELECT * FROM bis_user WHERE sysid={$MassParam['sysid']} LIMIT 1");
		$cat = (array)$ctg[0];
		$ss = "";
		$ss .= "<p><strong>ID: {$MassParam['sysid']}</strong></p>";
		$ss .= Sheet::DannLogo();
		$ss .= "<form name='frmDann' id='frmClient' style='padding-bottom:100px'>";
		$name = htmlspecialchars_decode($cat['name'], ENT_QUOTES);
		$nameh1 = htmlspecialchars_decode($cat['surname'], ENT_QUOTES);
		$nik = htmlspecialchars_decode($cat['nik'], ENT_QUOTES);
		$ss .= "<div class='flex flex-align-center' style='margin-bottom:10px'>"
			."<div style='width:150px'><span class='zvezda'>*</span>Имя:</div>"
			."<div style='flex:1 0 auto;-webkit-flex:1 0 auto;'>"
			."<input type='text' class='input' name='name' value='$name' required /></div>"
			."</div>";
		$ss .= "<div class='flex flex-align-center' style='margin-bottom:10px'>"
			."<div style='width:150px'><span class='zvezda'>*</span>Фамилия:</div>"
			."<div style='flex:1 0 auto;-webkit-flex:1 0 auto;'>"
			."<input type='text' class='input' name='surname' value='$nameh1' required /></div>"
			."</div>";
		$ss .= "<div class='flex flex-align-center' style='margin-bottom:10px'>"
			."<div style='width:150px'><span class='zvezda'>*</span>Должность:</div>"
			."<div style='flex:1 0 auto;-webkit-flex:1 0 auto;'>"
			."<input type='text' class='input' name='nik' value='$nik' required /></div>"
			."</div>";
		$m=[0=>"",1=>"",2=>""];
		$m[$cat['sex']]="checked";
		$ss .= "<div class='flex flex-align-center' style='margin-bottom:10px'>"
				."<div class='flex flex-align-center' style='margin-right:40px;'>"
					."<span>Телефон:&nbsp;</span>"
					."<span style='width:150px'>"
						."<input class='input' type='tel' name='phone' value='{$cat['phone']}' />"
					."</span>"
				."</div>"
				."<div class='flex flex-align-center' style='margin-right:40px;'>"
					."<span>Дата рождения:&nbsp;</span>"
					."<span style='width:100px'>"
						."<input id='datepicker' class='input' name='bdate' value='{$cat['bdate']}' readonly />"
					."</span>"
				."</div>"
				."<div class='flex flex-align-center'>"
					."<span style='margin-right:10px;'>Пол:&nbsp;</span>"
					."<span style='margin-right:10px;'>"
						."<input name='sex' value='1' id='men' type='radio' {$m[1]} />"
						."<label for='men'>Мужской</label>"
					."</span>"
					."<span style='margin-right:10px;'>"
						."<input name='sex' value='2' id='women' type='radio' {$m[2]} />"
						."<label for='women'>Женский</label>"
					."</span>"
					."<span>"
						."<input name='sex' value='0' id='nosex' type='radio' {$m[0]} />"
						."<label for='nosex'>Неважно</label>"
					."</span>"
				."</div>"
			."</div>";
		$ss .= "<div class='flex flex-align-center' style='margin-bottom:10px'>"
				."<div class='flex flex-align-center' style='margin-right:40px;'>"
					."<span>Логин, он же E-mail:&nbsp;</span>"
					."<span style='width:200px'>"
						."<input class='input' name='email' type='email' value='{$cat['email']}' required />"
					."</span>"
				."</div>"
				."<div class='flex flex-align-center' style='margin-right:40px;'>"
					."<span>Пароль:&nbsp;</span>"
					."<span style='width:200px'>"
						."<input class='input' name='password' value='{$cat['password']}' required />"
					."</span>"
				."</div>"
			."</div>";
        
		$ss .= UserAdmin::Organizations($MassParam['sysid']);
		$ss .= self::Role($cat['type_user']);
		$x=$MassParam;
		unset($x['sysid']);
		$ss .= "<input type='hidden' name='sysid' value='{$MassParam['sysid']}' />"
			."<input type='hidden' name='return' value='/admin/content/client/?". http_build_query($x) ."' />";
        $ss .= "<input type='hidden' name='_token' value='". csrf_token() ."' />";
		$ss .= General::GlobalSubmit("/admin/content/client/?". http_build_query($x));
		$ss .= "</form>";
		return $ss;
    }

    private function Role($type){
		$ss = "";
		$m = explode('#',$type);
		$ctg = DB::select("SELECT * FROM bis_cmsmenu WHERE sysid>1 && sysid<100 && hide='show' ORDER BY pos");
        foreach($ctg as $cat1){
		    $cat=(array)$cat1;
			$name=General::DecodeTitle($cat['name']);
			$checked=in_array($cat['sysid'],$m)?"checked":"";
			$ss .= "<div style='margin-top:7px'>"
			."<input type='checkbox' $checked value='{$cat['sysid']}' name='role[]' id='role{$cat['sysid']}' />"
			."&nbsp;<label for='role{$cat['sysid']}'>$name</label>";
		}
		return "<div class='role' style='padding:10px 0'>"
		."<p><strong>Доступные разделы аналитики</strong></p>$ss</div>";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
		$validator = Validator::make($request->all(),[
            'name' => 'required',
            'surname' => 'required',
            'nik' => 'required',
            'email' => 'required|email',
            'password' => 'required',

        ]);
        if($validator->fails()) return response()->json(['success' => false,'msg' => 'Проверьте заполнение обязательных полей!!!']);
        $email = $request->email;
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) return response()->json(['success' => false,'msg' => 'Проверьте Email']);
        $sysid = (int)$request->sysid;
		$name = htmlspecialchars(General::DecodeTitle($request->name), ENT_QUOTES);
		$surname = htmlspecialchars(General::DecodeTitle($request->surname), ENT_QUOTES);
		$nik = htmlspecialchars(General::DecodeTitle($request->nik), ENT_QUOTES);
		$phone = htmlspecialchars(strip_tags(trim(htmlspecialchars_decode($request->phone))),ENT_QUOTES);
		$bdate = htmlspecialchars(strip_tags(trim(htmlspecialchars_decode($request->bdate))),ENT_QUOTES);
		$sex=(int)$request->sex;
		$password=$request->password;
		if(!isset($request->role)) $m_role=[];
		else $m_role=$request->role;
		array_push($m_role,1);
		$role=implode('#',$m_role);
		DB::update("UPDATE bis_user SET name='$name',surname='$surname',phone='$phone',nik='$nik',"
		    ."bdate='$bdate',sex=$sex,email='$email',password='$password',type_user='$role' WHERE sysid=$sysid LIMIT 1");
        DB::delete("DELETE FROM userlist_city WHERE parent=$sysid");
		if(isset($request->org)){
			foreach($request->org as $m){
				DB::insert("INSERT INTO userlist_city(parent,id) VALUES($sysid,'$m')");
			}
		}
		$return = isset($request->save_pages)?$request->return:"";
		return response()->json(['success' => true,'msg' => 'Записано','return'=>$return]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
