<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Admin\AdminGlobalController as General;
use Illuminate\Support\Facades\Storage;

class AdminGoogleUserController extends Controller
{

    public function index(Request $request){
		$homepage = file_get_contents("https://docs.google.com/spreadsheets/d/e/"
            ."2PACX-1vTiQ5QEPohMGm0f9Ev6yQ7hFAflSwTa4rpl2lYxTcMlDzi0yU-QbeO85lZST0Bmkje1__n1LQfd7uEl/"
            ."pub?gid=921441470&single=true&output=csv");
		if($homepage){ 
            Storage::put('reestr_role.csv', $homepage);
            $file = Storage::path('reestr_role.csv');
			$handle = @fopen($file,"r");
			if($handle){
                DB::statement("TRUNCATE TABLE bis_user_role");
				$k=1;
				while(($data= fgetcsv($handle,0,",")) !== FALSE){
					if($k>=2 && !empty($data[2])){
						DB::insert("INSERT INTO bis_user_role(role) VALUES('{$data[2]}')");
					}
					$k++;
				}
		    	fclose($handle);
			}else return response()->json(['success' => false,'msg' => 'Файл ролей гугл не получен']);
		} else return response()->json(['success' => false,'msg' => 'Файл ролей гугл не получен']);
        
        $homepage = file_get_contents("https://docs.google.com/spreadsheets/d/e/"
            ."2PACX-1vTiQ5QEPohMGm0f9Ev6yQ7hFAflSwTa4rpl2lYxTcMlDzi0yU-QbeO85lZST0Bmkje1__n1LQfd7uEl/"
            ."pub?gid=193973388&single=true&output=csv");
        if($homepage){
            Storage::put('reestr.csv', $homepage);
            $file = Storage::path('reestr.csv');
            $handle = @fopen($file,"r");
            if($handle){
                DB::unprepared('UPDATE bis_user SET parent=0');
                $k=1;
                while(($data= fgetcsv($handle,0,",")) !== FALSE){
                    if($k>=2 && !empty($data[2])){
                        $ctg=DB::select("SELECT sysid FROM bis_user WHERE name='{$data[2]}' LIMIT 1");
                        if(!empty($ctg)){
                            $cat = $ctg[0];
                            DB::update("UPDATE bis_user SET type_user='{$data[1]}',city='{$data[3]}',"
                            ."adres='{$data[4]}',email='{$data[5]}',phone='{$data[6]}',"
                            ."nik='{$data[7]}',password='{$data[8]}',hide='show',"
                            ."parent=1 WHERE sysid=".$cat->sysid." LIMIT 1");
                        }else{
                            DB::insert("INSERT INTO bis_user(type_user,name,city,adres,email,phone,nik,password,parent,hide) "
                            ."VALUES('{$data[1]}','{$data[2]}','{$data[3]}','{$data[4]}','{$data[5]}',"
                            ."'{$data[6]}','{$data[7]}','{$data[8]}',1,'show')");
                        }
                    
                    }
                    $k++;
                }
                fclose($handle);
                DB::unprepared("UPDATE bis_user SET hide='hide' WHERE parent=0");
            } else return response()->json(['success' => false,'msg' => 'Файл пользователей гугл не получен']);
        } else return response()->json(['success' => false,'msg' => 'Файл пользователей гугл не получен']);
        return response()->json(['success' => true,'msg' => 'OK']);
    }

    function ListGoogleUser(){
        $ss = "";
        $ctg=DB::select("SELECT * FROM bis_user WHERE hide='show' ORDER BY name");
        if(!empty($ctg)){
            $ss .= "<ul class='general-ul all-list' data-tables='bis_user'>";
            foreach($ctg as $cat1) {
                $cat=(array)$cat1;
                $sid = $cat['sysid']; 
                $fio = General::DecodeTitle($cat['name']) ." [{$cat['type_user']}] Роль: {$cat['adres']}"; 
                $ss .= "<li>";
                    $ss .= "<div class='all-list-item'>";
                        $ss .= "<div class='all-list-item-edit'>$fio</div>";
                        $ss .= "<div></div>";
                    $ss .="</div>";
                $ss .= "</li>";
            }
            $ss .= "</ul>";
        }
        return $ss;
    }
}
