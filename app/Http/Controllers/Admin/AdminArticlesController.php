<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
//use App\Http\Controllers\Admin\AdminGlobalController as General;

class AdminArticlesController extends Controller
{
	private $NamePages;
	static $ParentCat;
	static $Sysid;
	static $MassParam;
	private $MassFind;
	static $ChildrenMenu;
	private $ListBrend;
	static $RecordCmsMenu;

	function CategoriesSelect(){
		///$for - list - для смены раздела при списке, pages - для смены раздела в корр.страницы
		$nameCatActive = self::CategoriesSelectActive();
		$ss = "";
		$ss .= "<div class='select-categories'>";
			$ss .= "<div class='select-categories-item' >";
				$ss .= "<div class='active-categories input'>$nameCatActive</div>";
				$ss .= "<div class='list-categories'>";
					$ss .= "<div class='from-input'>";
						$ss .= "<input type='text' class='input' autocomplete='off' id='findcat'/>";
					$ss .= "</div>";
					self::$ChildrenMenu = "";
					self::CategoriesSelectItem(self::$MassParam['id']);
					$ss .= self::$ChildrenMenu;
				$ss .= "</div>";
			$ss .= "</div>";
			$m=self::$MassParam;
			unset($m['parent'], $m['pages']);
			$ss .= "<div class='select-categories-item'><a href='/admin/content/articles/?"
				.http_build_query($m) ."' class='button current'>Очистить</a></div>";
		$ss .= "</div>";
		return $ss;
	}
	function CategoriesSelectItem($parent){
		$QUERYSTRING=http_build_query(self::$MassParam);
		$ctg = DB::select("SELECT name,sysid FROM bis_categories WHERE parent=$parent ORDER BY name ");
		if(!empty($ctg)) {
			self::$ChildrenMenu .= "<ul>";
			foreach($ctg as $cat1){
				$cat=(array)$cat1;
				self::$ChildrenMenu .= "<li><a href='/admin/content/articles/?$QUERYSTRING&parent={$cat['sysid']}'>{$cat['name']}</a>";
				self::CategoriesSelectItem($cat['sysid']);
				self::$ChildrenMenu .= "</li>";
			}
			self::$ChildrenMenu .= "</ul>";
		}
	}
	function CategoriesSelectActive(){
		if( self::$MassParam['id'] == self::$ParentCat ) return "- Выбор категории -";
		else {
			$ctg = DB::select("SELECT name FROM bis_categories WHERE sysid=". self::$ParentCat ." LIMIT 1");
			if(!empty($ctg)) return $ctg[0]->name;
			return "- Выбор категории -";
		}
	}

	function VariablesArticles($FeatureGlobalMenu,$sysid,$MassParam,$ParentCat){
		self::$RecordCmsMenu = (array)$FeatureGlobalMenu;
		self::$Sysid=$sysid;
		self::$MassParam=$MassParam;
		self::$ParentCat=$ParentCat;
	}
	
} // END
