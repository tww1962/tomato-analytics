<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Admin\AdminGlobalController as General;

class AdminAjaxController extends Controller
{
    private $Modul;
    private $Sysid;
    private $Table;
	private $arrayParent;

    public function index(Request $request,$modul){
        $this->Modul=$modul;
        switch($modul){
            case 'sheet':
                return $this->Sheet($request);
                break;
			case 'seo':
				return $this->Seo($request);
				break;
			case 'articles':
				return $this->Articles($request);
				break;
			case 'categories':
				return $this->Categories($request);
				break;
			case 'delhideshow':
				$this->DelHideShow($request); //для всех страниц
				break;
			case 'sorttables': //сортировка
				$mass = explode('#',$request->m);
				foreach($mass as $val){
					$m = explode(':',$val);
					DB::update("UPDATE ". $request->tables ." SET pos={$m[1]} WHERE sysid={$m[0]} LIMIT 1");
				}
				break;
			case 'newparent': //смена родителя
				DB::update("UPDATE ". $request->tables ." SET parent=". $request->parent ." WHERE sysid=". $request->sysid ." LIMIT 1");
				break;
			case 'client':
				return $this->Client($request);
				break;
		}
    }

	private function Categories($request){
		$parent = (int)$request->parent;
		$level = (int)$request->level;
		$FeatureGlobalMenu=$request->setting;
		$name = htmlspecialchars(General::DecodeTitle(strip_tags(trim(htmlspecialchars_decode($request->name)))), ENT_QUOTES);
		$nameT = General::Translit_Global($name);
		if( !empty($name) ) {
			$sysid = DB::table('bis_categories')->insertGetId(['name'=>"$name",'nameh1'=>"$name",'parent'=>$parent,'level'=>$level,'pos'=>-1]);
			$nameT = General::NameTranslitDubl($nameT,$sysid,$level);
			DB::update("UPDATE bis_categories SET translit='$nameT' WHERE sysid=$sysid LIMIT 1");
			General::InsertSeo($sysid,$name);
			$ss = "";
			$ss .= "<li id='li$sysid' sid='$sysid' class='mjs-nestedSortable-leaf'>";
				$ss .= "<div class='all-list-item'>";
					$ss .= "<div class='flex'>";
						$ss .= "<span class='main-icon sort' style='margin-right:3px'><i class='fa fa-arrows' aria-hidden='true' title='Сортировка'></i></span>";
						$ss .= "<span class='main-icon disclose'><strong class='ui-icon' title='Развернуть/Свернуть'></strong></span>";
					$ss .= "</div>";
					$ss .= "<div class='for-logo'></div>"; // for logo
					$ss .= "<div><a href='/admin/content/sheet/?sysid=$sysid&".$request->has."'>$name</a></div>";
					$ss .= "<div>";
						$ss .= "<div data-sid='$sysid'>";
							if(strpos($FeatureGlobalMenu,"#inGlCat#") !== false) $ss .= "<span class='main-icon'><i class='fa fa-bolt'></i></span>";
							if(strpos($FeatureGlobalMenu,"#inNewCat#") !== false) $ss .= "<span class='main-icon'><i class='fa fa-paper-plane'></i></span>";
							$ss .= "<span title='Добавить подраздел' class='main-icon plus newPages' data-level='$level' "
								."data-parent='$sysid' data-id='#li$sysid>ul'><i class='fa fa-plus' aria-hidden='true'></i></span>";
						$ss .="</div>";
						$ss .= "<div data-sid='$sysid' data-name='$name' data-parent='#li$sysid'>";
							$ss .= "<span class='main-icon'><i class='fa fa-eye' aria-hidden='true' title='Скрыть'></i></span>";
							$ss .= "<span class='main-icon'><i class='fa fa-trash-o' aria-hidden='true' title='Удалить'></i></span>";
						$ss .="</div>";
					$ss .= "</div>";
				$ss .="</div>";
			$ss .= "</li>";
			return $ss;
		}
	}

	private function Articles($request){
		$parent = (int)$request->parent;
		$level = (int)$request->level;
		$name = htmlspecialchars(General::DecodeTitle(strip_tags(trim(htmlspecialchars_decode($request->name)))), ENT_QUOTES);
		$nameT = General::Translit_Global($name);
		if( !empty($name) ) {
			$sysid = DB::table('bis_pages')->insertGetId(['name'=>"$name",'nameh1'=>"$name",'parent'=>$parent,'level'=>$level,'pos'=>-1]);
			if( !empty($request->data) ) { //новости
				$dd = implode('-',array_reverse(explode('.',$request->data))); 
				DB::update("UPDATE bis_pages SET data='$dd' WHERE sysid=$sysid LIMIT 1");
			}
			$nameT = General::NameTranslitDubl($nameT,$sysid,$level);
			DB::update("UPDATE bis_pages SET translit='$nameT' WHERE sysid=$sysid LIMIT 1");
			General::InsertSeo($sysid,$name);
		}
	}

	private function Seo($request){
		$parent = (int)$request->parent;
		$t = htmlspecialchars($request->t, ENT_QUOTES);
		$k = htmlspecialchars($request->k, ENT_QUOTES);
		$d = htmlspecialchars($request->d, ENT_QUOTES);
		$ctg = DB::select("SELECT * FROM bis_pagesSeo WHERE parent=$parent LIMIT 1");
		if(!empty($ctg)){
			DB::update("UPDATE bis_pagesSeo SET seo_title='$t', seo_key='$k', seo_desc='$d' WHERE parent=$parent LIMIT 1");
		}else{
			DB::insert("INSERT bis_pagesSeo(seo_title,seo_key,seo_desc,parent) VALUES('$t','$k','$d',$parent)");
		}
		$return = isset($request->save_pages)?$request->return:"";
		return response()->json(['success' => true,'msg' => 'Записано','return'=>$return]);
	}

    private function Client($request){
		$validator = Validator::make($request->all(),['name' => 'required','surname' => 'required',]);
        if($validator->fails()) return response()->json(['success' => false,'msg' => 'Проверьте заполнение обязательных полей!!!']);
		$name = htmlspecialchars(General::DecodeTitle($request->name), ENT_QUOTES);
		$surname = htmlspecialchars(General::DecodeTitle($request->surname), ENT_QUOTES);
		DB::insert("INSERT bis_user(parent,name,surname) VALUES(".$request->parent.",'$name','$surname')");
		return response()->json(['success' => true]);
	}

    private function Sheet($request){
		$validator = Validator::make($request->all(),['name' => 'required','nameh1' => 'required',]);
        if($validator->fails()) return response()->json(['success' => false,'msg' => 'Проверьте заполнение обязательных полей!!!']);
        $this->Sysid=$request->sysid;
        $this->Table=$request->table;
		$name = htmlspecialchars(General::DecodeTitle($request->name), ENT_QUOTES);
		$nameh1 = htmlspecialchars(General::DecodeTitle($request->nameh1), ENT_QUOTES);
		$nameT = General::Translit_Global($name);
		$l=isset($request->level)?(int)$request->level:0;
		$nameT = General::NameTranslitDubl($nameT,$this->Sysid,$l);
		$info = isset($request->info) ? htmlspecialchars($request->info, ENT_QUOTES) : "";
		$anons = isset($request->anons) ? htmlspecialchars($request->anons, ENT_QUOTES) : "";
		$articul = isset($request->articul) ? htmlspecialchars($request->articul,ENT_QUOTES) : "";
		$barcode = isset($request->barcode) ? htmlspecialchars($request->barcode, ENT_QUOTES) : "";
		$price = isset($request->price) ? round(1 * str_replace(',','.',$request->price),config("app.DECI")) : 0;
		$priceOld = isset($request->price_old) ? round(1 * str_replace(',','.',$request->price_old),config("app.DECI")) : 0;
		$inGl = isset($request->inGl) ? 1 : 0;
		$inNew = isset($request->inNew) ? 1 : 0;
		$inArxiv = isset($request->inArxiv) ? 1 : 0;
		$provider = isset($request->provider) ? (int)$request->provider : 0;
		if( isset($request->data) ) {
			$data = implode('-',array_reverse(explode('.',$request->data)));// . date('H:i:s',time());
		} else { $data = 'NULL'; }
		if( isset($request->dataPo) ) {
			$dataPo = implode('-',array_reverse(explode('.',$request->dataPo)));// . date('H:i:s',time());
		} else { $dataPo = 'NULL'; }
		
		$id1c=isset($request->role)?implode('#',$request->role):"";
		$l=isset($request->level)?(int)$request->level:0;
		if($this->Table == "bis_cmsmenu") {
			DB::update("UPDATE bis_cmsmenu SET name='$name',nameh1='$nameh1',translit='$nameT' WHERE sysid={$this->Sysid} LIMIT 1");
		} elseif ($this->Table == "bis_pages" ) {
			DB::delete("DELETE FROM bis_pagesSprMany WHERE sysidPages={$this->Sysid}");
			if( isset($request->sprcheck) ){
				foreach($request->sprcheck as $val) {
					DB::insert("INSERT INTO bis_pagesSprMany(sysidPages,sysidSpr) VALUES($this->Sysid,$val)");
				}
			}
			$b = [];
			$brend = "";
			if( isset($request->brend) ){
				$b = explode('#',$request->brend);
				DB::delete("DELETE FROM bis_brend WHERE sysidPages={$this->Sysid} LIMIT 1");
				DB::insert("INSERT INTO bis_brend(sysidPages,sysidBrend,sysidLine) VALUES({$this->Sysid},{$b[0]},{$b[1]})");
				$brend = ",brend={$b[0]}";
			}
			$parent = isset($request->categories)?",parent=".(int)$request->categories:"";
			
			DB::update("UPDATE bis_pages SET name='$name',nameh1='$nameh1',"
			    ."articul='$articul',translit='$nameT',id1c='$id1c',"
			    ."barcode='$barcode'$parent WHERE sysid={$this->Sysid} LIMIT 1");
		} else {
			DB::update("UPDATE bis_categories SET name='$name',id1c='$id1c',".
				"nameh1='$nameh1', translit='$nameT' WHERE sysid={$this->Sysid} LIMIT 1");
		}
        General::InsertUpdateInfo($this->Sysid,$anons,$info);
        General::InsertSeo($this->Sysid,$nameh1);
		$return = isset($request->save_pages)?$request->return:"";
		return response()->json(['success' => true,'msg' => 'Записано','return'=>$return]);
    }

	private function FullParent($parent,$tables){
		if( $tables == "bis_pagesKit" ) return;
		$ctg=DB::select("SELECT sysid FROM $tables WHERE parent=$parent");
		if(!empty($ctg)){
			foreach($ctg as $cat){
				$this->arrayParent[] = $cat->sysid;
				$this->FullParent($cat->sysid,$tables);
			}
		}
	}
	private function AllHideShow($tables, $hide){
		$parent = implode(',',$this->arrayParent);
		DB::update("UPDATE $tables SET hide='$hide' WHERE sysid in ($parent)");
		if($tables == "bis_categories"){
			DB::select("UPDATE bis_pages SET hide='$hide' WHERE parent in ($parent)");
		}
	}
	private function DeleteRecordsFiles() {
		$parent = implode(',',$this->arrayParent);
		$tables = ['bis_files','bis_foto','bis_galfoto','bis_pagesLogoAll'];
		foreach( $tables as $val ){DB::delete("DELETE FROM $val WHERE parent in ($parent)");}
	}
	private function DeleteAllFeature($id){
		$ctg = DB::select("SELECT sysid FROM bis_feature WHERE sysidPages=$id");
		if(!empty($ctg)) foreach($ctg as $cat){ DB::delete("DELETE FROM bis_featureMany WHERE parentFeature=". $cat->sysid);}
		DB::delete("DELETE FROM bis_feature WHERE sysidPages=$id"); 
	}
	private function FullSysidPages($parent){  //  ищу все sysid при удалении категории
		$ctg = DB::select("SELECT sysid FROM bis_pages WHERE parent in ($parent)");
		if(!empty($ctg)){
			foreach($ctg as $cat){
				$this->arrayParent[] = $cat->sysid;
				$this->DeleteAllFeature($cat->sysid);
			}
		}
	}

	private function DelHideShow($request){
		$vid = (int)$request->vid;
		$id = (int)$request->id;
		$tables = $request->tables;
		$hide_show=[0=>'hide',1=>'show'];
		if($vid<3){
			if( $tables == 'bis_cmsmenu' ) { 
					if($vid==0) DB::update("update $tables set vidMenu='none' WHERE sysid=$id LIMIT 1");
					else{
						$ctg = DB::select("select * from $tables WHERE sysid=$id LIMIT 1");
						$cat = (array)$ctg[0];
						$vm = ( $cat['nav'] == 1 ) ? "ind" : "hor";
						DB::update("update $tables set vidMenu='$vm' WHERE sysid=$id LIMIT 1"); 
	
					}
			} elseif($tables=='userlist') { 
				DB::update("update $tables set hide='{$hide_show[$vid]}' WHERE id_user=$id LIMIT 1"); 
			} elseif($tables == 'bis_feature' || $tables == 'bis_foto' || $tables=='userlist') { 
				DB::update("update $tables set hide='{$hide_show[$vid]}' WHERE sysid=$id LIMIT 1"); 
			} else { 
				DB::update("update $tables set hide='{$hide_show[$vid]}' WHERE sysid=$id LIMIT 1");
				$this->arrayParent = [];
				$this->arrayParent[] = $id;
				$this->FullParent($id,$tables);
				if( count($this->arrayParent) > 1 ){
					$this->AllHideShow($tables,$hide_show[$vid]);
				}
			}
		}else{
			$this->arrayParent = [];
			$this->arrayParent[] = $id;
			if(in_array($tables,["bis_files","bis_related","bis_filesPdf","bis_foto"])){
				DB::delete("DELETE FROM $tables WHERE sysid=$id LIMIT 1");
			} elseif($tables=='userlist') { 
				DB::delete("DELETE FROM $tables WHERE id_user=$id LIMIT 1"); 
			} elseif( $tables == "bis_pages" ) {
				$this->FullParent($id,$tables);
				$this->DeleteRecordsFiles();
				$this->DeleteAllFeature($id);
				$parent = implode(',',$this->arrayParent);
				DB::delete("DELETE FROM bis_datatime WHERE parent=$id"); 
				DB::delete("DELETE FROM bis_adres WHERE parent=$id");
				DB::delete("DELETE FROM bis_pagesKit WHERE parent1=$id || parent2=$id");
				DB::delete("DELETE FROM bis_pages WHERE sysid=$id LIMIT 1");
			} elseif( $tables == "bis_categories" ) {
				$this->FullParent($id,$tables);
				$parent = implode(',',$this->arrayParent);
				$this->DeleteRecordsFiles();
				DB::delete("DELETE FROM bis_categories WHERE sysid in ($parent)");
				$this->arrayParent = [];
				$this->FullSysidPages($parent);
				if( count($this->arrayParent) > 0 ){
					$this->DeleteRecordsFiles();
					DB::delete("DELETE FROM bis_pages WHERE parent in ($parent)");
				}
			} elseif( $tables == "bis_pagesSpr" ) {
				DB::delete("DELETE FROM bis_pagesSpr WHERE sysid=$id || parent=$id"); 
			} elseif( $tables == "bis_feature" ) {
				DB::delete("DELETE FROM bis_featureMany WHERE parentFeature=$id");
				DB::delete("DELETE FROM bis_feature WHERE sysid=$id LIMIT 1");
			} elseif( $tables == "bis_cart" ) {
				DB::delete("DELETE FROM bis_cartTovar WHERE parent={$cat['sysid']}");
				DB::delete("DELETE FROM bis_cart WHERE sysid={$cat['sysid']} LIMIT 1");
			} else {
				DB::delete("DELETE FROM $tables WHERE sysid=$id LIMIT 1"); 
			}

		}
	}
   
} //END
?>