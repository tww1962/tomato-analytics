<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Imagick;

class PersonalController extends Controller
{

    public function index(Request $request){
        if(isset($request->flagMain)){ //модальные
            if((int)$request->flagMain == 851 ){ //данные
                DB::update("UPDATE bis_user SET name='". $request->name ."',surname='". $request->surname ."',nik='". $request->nik ."',"
                    ."sex=". $request->sex .",phone='". $request->phone ."',bdate='". $request->bdate ."' WHERE sysid=". $request->sid ." LIMIT 1");
                return "errorSity('<h3>Отлично</h3><div>Информация сохранена</div>');";
            }elseif((int)$request->flagMain == 852 ){ // фото
                return self::NewPhoto($request);
            }elseif((int)$request->flagMain == 853 ){ // пароль
                return self::NewPassword($request);
            }
            return "alert('Неизвестная ошибка, обратитесь к разработчику')";
        }else{
            $user = Auth::user();
            $cmsmenu=DB::select("SELECT * FROM bis_cmsmenu WHERE script='personal-area' LIMIT 1");
            return view('personal',['user'=>$user,'cmsmenu'=>(array)$cmsmenu[0]]);
        }
    }

	private function NewPhoto($request){ //л/к photo
        if(!$request->hasfile('photo')) return;
        $allowTypes = 'jpeg,jpg,png';
        $validator = Validator::make($request->all(), [
            'photo.*' => ['required', 'mimes:' . $allowTypes, 'max:' . (5*1024*1024)],
        ], [], ['photo' => 'file']);
        if ($validator->fails()) return "errorSity('<h3>Внимание</h3><div>Проверьте размер или формат файла</div>');";
        $photo = $request->file('photo');
        $namePhoto="img". str_replace('.','',(string)microtime(true)) .".". $photo->getClientOriginalExtension();
        DB::delete("DELETE FROM bis_pagesLogoAll WHERE parent=". $request->sid);
        DB::insert("INSERT INTO bis_pagesLogoAll(parent,photo) VALUES(". $request->sid .", '$namePhoto')");
        $photo->move(public_path('files/logo'),$namePhoto);
        $file = public_path('files/logo') ."/". $namePhoto;
        $size_img = getimagesize($file);
        if($size_img[0]>165 || $size_img[1]>165){
            $im = new Imagick($file);
            if( $size_img[0] > $size_img[1] ) {
                $im->adaptiveResizeImage(165,0);
            } else {
                $im->adaptiveResizeImage(0,165);
            }
            $im->writeImage($file);
            $im->clear();
            $im->destroy();
        }
		return "errorSity('<h3>Отлично</h3><div>Фото сохранено</div>');"
		    ."$('#fotoImg').find('img').remove();"
		    ."$('#fotoImg').append('<img src=\"/files/logo/$namePhoto\" />');";
	}

    private function NewPassword($request){
		$ctg=DB::select("SELECT sysid FROM bis_user WHERE sysid=". $request->sid ." && password='". $request->pass ."' LIMIT 1");
		if(empty($ctg)) return "errorSity('<h3>Внимание</h3><div>Проверьте текущий пароль</div>');";
		if($request->pass1 != $request->pass2) return "errorSity('<h3>Внимание</h3><div>Пароли не совпадают</div>');";
		DB::update("UPDATE bis_user SET password='". $request->pass1 ."' WHERE sysid=". $request->sid ." LIMIT 1");
		return "errorSity('<h3>Внимание</h3><div>Пароль изменен</div>');$('#f3').trigger('reset');";
    }

    public function Photo($parent, $wh){
        $result = DB::select("SELECT * FROM bis_pagesLogoAll WHERE parent=$parent LIMIT 1");
        if(!empty($result)){
            if(file_exists(public_path('files/logo') ."/". $result[0]->photo) && !empty($result[0]->photo)){
                return "<img src='/files/logo/". $result[0]->photo ."' />";
            }
        }
        return "";
    }

} //END
