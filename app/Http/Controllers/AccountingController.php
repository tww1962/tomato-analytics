<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HomeController as General;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use DateTime;

class AccountingController extends Controller
{
    public static $user;
    private static $Data_Report;
    private static $Sum_Time_Dat;
    private static $IdListOrg;
    private static $ArrayListOrg;
    private static $Sum_Time;
    private static $List_Courier;
    private static $Courier_Get;
	private static $One_Courier;
    private static $Delivery_Ok;
    private static $Dat;
    public static $Late_Min=[0,60,-1];
    private static $QUERYSTRING;
    public static $Late_Where;
    public static $Late_Where_Null;
    private static $sek_Kitchen;
    private static $Accounting_;
    private static $Accounting_Dann = [0,0,0,0,0,0];

    public function index(Request $request){
        if(isset($request->flagMain)){ //модальные
            if((int)$request->flagMain == 101 ){ // данные курьера
                return self::Accounting_Courier($request);
            }elseif((int)$request->flagMain == 100 ){ //отметка о сдачи отчета
                $x="UPDATE `OrderTEMP` SET refund=". $request->type ." WHERE sysid=". $request->sid ." LIMIT 1";
                DB::update($x);
                return "console.log('Ok')";
            }
            return "alert('Неизвестная ошибка, обратитесь к разработчику')";
        }else{
            if(isset($request->point_multi)){
                $m = (array)$request->all();
                unset($m['_token']);
                self::$QUERYSTRING = http_build_query($m);
                self::$ArrayListOrg = (array)$request->point_multi;
                if(count(self::$ArrayListOrg) == 1) General::session_param_put($m);
            }
            self::$user = Auth::user();
            $cmsmenu=DB::select("SELECT * FROM bis_cmsmenu WHERE script='uchyot-i-vedenie' LIMIT 1");
            $point = DB::select("SELECT * FROM `Organizations` WHERE Conception='ТиЧ' && "
                ."Id in(SELECT id FROM userlist_city WHERE parent=". self::$user->sysid .") ORDER BY OrganizationName");
            return view('accounting',['user'=>self::$user,'cmsmenu'=>(array)$cmsmenu[0],'points'=>$point]);
        }
    }

	public function Accounting_Rezult($param,$checked){
		if(empty($checked)) return "";
		$courier = !empty($param['courier'])?" && Courier_Id='{$param['courier']}'":"";
        $refund = (isset($param['refund']) && (int)$param['refund']>0) ? " && refund=". ((int)$param['refund']-1) : "";
		$d1=implode("-",array_reverse(explode(".",$param['date1'])));
		$d2=implode("-",array_reverse(explode(".",$param['date2'])));
		self::Data_Report($d1,$d2);
		$query="SELECT * FROM `OrderTEMP` WHERE "
		    ."expectedDeliverTime BETWEEN STR_TO_DATE('". self::$Data_Report[0] ."','%Y-%m-%d %H:%i:%s') AND "
		    ."STR_TO_DATE('". self::$Data_Report[1] ."','%Y-%m-%d %H:%i:%s') && isCafe=0 && isCanceled=0 && "
		    ."organizationId='$checked' && isClientDelivery=0 && "
		    ."Courier_Name IS NOT NULL $courier $refund";
		$ctg = DB::select("$query ORDER BY expectedDeliverTime DESC");
		$ss = "";
		foreach($ctg as $cat1) {
            $cat = (array)$cat1;
			$ss .= "<div class='accounting-tr'>";
				$ss .= "<div class='accounting-td'>{$cat['number']}</div>";
				$ss .= "<div class='delivery-td'><span>" //доставить к
					.date('d.m.y',strtotime($cat['expectedDeliverTime']))."<br />"
					.date('H:i:s',strtotime($cat['expectedDeliverTime']))."</span></div>";
				$ss .= self::Accounting_Rezult_Address($cat['address']);
				$ss .= "<div class='accounting-td'>".number_format($cat['resultSum'],2,'.',' ')."</div>";
				$ss .= self::Accounting_Rezult_Summ($cat);
				$ss .= "<div class='accounting-td'>"
					."<span class='delivery-person' data-sid='{$cat['sysid']}' title='подробнее'>"
					.General::DecodeTitle($cat['Courier_Name']) ."</span></div>";
				$cl=$cat['refund']==1?"active":"";
				$type=$cat['refund']==1?"checked":"";
				$ss .= "<div class='accounting-td refund flex flex-align-center flex-justify-center $cl' "
					."data-sid='{$cat['sysid']}'><label></label></div>";
			$ss .= "</div>";
		}
		return $ss;
	}
	private function Accounting_Rezult_Summ($cat){
		$payments=preg_replace('/[\x00-\x1F\x7F\xA0]/u','',$cat['payments']);
		$s=json_decode($cat['payments'],true);
		if(!is_array($s)) return "<div class='accounting-td'></div>";
		$ss = "";
		$mani=[];
		$itog=0;
		foreach($s as $key=>$m){
			$cl = "";
			if($m['Type']['Name']=="Наличные"){
				$cl = "cash";
			}elseif($m['Type']['Name']=="Visa"){
				$cl = "visa";
			}
			$itog += $m['Sum'];
			$mani[]="<span class='$cl'>{$m['Sum']}</span>";
		}
		if($cat['resultSum']<$itog) 
			$mani[]="<span><i>сдача</i> ".number_format($itog-$cat['resultSum'],0,'.',' ')." р.</span>";
		$ss .= implode("<br />",$mani);
		return "<div class='accounting-td'><div>$ss</div></div>";
	}
	private function Accounting_Rezult_Address($address){
		$address = preg_replace('/[\x00-\x1F\x7F\xA0]/u','',$address);
		$find=',"AdditionalInfo"';
		$pos=mb_strpos($address,$find);
		$address = mb_substr($address,0,$pos) . "}";
		$s=json_decode($address,true);
		if(!is_array($s)) return "<div class='accounting-td'></div>";
		$street = General::DecodeTitle($s['Street']['Name']);
		$house = General::DecodeTitle($s['House'])." - ". General::DecodeTitle($s['Flat']);
		return "<div class='accounting-td'>$street, $house</div>";
	}


    function Rating_Courier_List($point,$d1,$d2,$courier){
        if(empty($point)) return "";
        self::$Courier_Get = $courier;
		$ss = "";
		self::$List_Courier['name']=self::$List_Courier['id']=self::$List_Courier['count']=self::$List_Courier['count_order']=[];
		$d1=implode("-",array_reverse(explode(".",$d1)));
		$d2=implode("-",array_reverse(explode(".",$d2)));
		self::Data_Report($d1,$d2);
		$query="SELECT Courier_Name,Courier_Id,count(Courier_Id) as count,"
		."sum(timestampdiff(SECOND,`sendTime`,`deliveredDateTime`)) as sek FROM `OrderTEMP` WHERE "
		."expectedDeliverTime BETWEEN STR_TO_DATE('".self::$Data_Report[0]."','%Y-%m-%d %H:%i:%s') AND "
		."STR_TO_DATE('".self::$Data_Report[1]."','%Y-%m-%d %H:%i:%s') && isCafe=0 && isCanceled=0 && "
		."organizationId='$point' && isClientDelivery=0 && "
		."deliveredDateTime IS NOT NULL && deliveredDateTime!='0000-00-00 00:00:00' && "
		."Courier_Name IS NOT NULL GROUP BY Courier_Id";
		$ctg = DB::select($query);
		foreach($ctg as $cat){
			$name = General::DecodeTitle($cat->Courier_Name);
			self::$List_Courier['name'][] = $name;
			self::$List_Courier['id'][] = $cat->Courier_Id;
			self::$List_Courier['count'][] = is_null($cat->count) ? "===" : round($cat->sek/$cat->count,0);
			self::$List_Courier['count_order'][] = is_null($cat->count) ? "0" : $cat->count;
		}
		natsort(self::$List_Courier['name']);
		foreach(self::$List_Courier['name'] as $key=>$val){
			$selected = self::$List_Courier['id'][$key]==self::$Courier_Get ? "selected" : "";
			$ss .= "<option value='". self::$List_Courier['id'][$key] ."' $selected >$val</option>";
		}
		if(!empty($ss)) return "<select name='courier' id='rating_courier_list'><option value='0'>Все</option>$ss</select>";
		return "";
	}

	public function Data_Report($d1,$d2){
		self::$Data_Report[0] = date("Y-m-d H:i:s",strtotime("$d1 00:00:00"));
		self::$Data_Report[1] = date("Y-m-d H:i:s",strtotime("$d2 23:59:59"));
	}

	private function Accounting_Courier($request){
        $ss = "";
		$ctg=DB::select("SELECT * FROM `OrderTEMP` WHERE sysid=". $request->sid ." LIMIT 1");
		$cat = (array)$ctg[0];
		self::$Accounting_['name'] = General::DecodeTitle($cat['Courier_Name']);
		self::$Accounting_['name_id'] = $cat['Courier_Id'];
		self::$Accounting_['point'] = self::Accounting_Point($request->point);
		self::$Accounting_['data']="С ". $request->d1 ." по ".$request->d2;
		self::Data_Report($request->d1,$request->d2);
		$ss .= "<h3>". self::$Accounting_['name'] ."</h3>";
		$ss .= "<div class='m-accounting for-center'>".self::$Accounting_['point']."</div>";
		$ss .= "<div class='m-accounting for-center'>".self::$Accounting_['data']."</div>";
		$ss .= self::Accounting_Courier_($request);
		return "modalSity(\"$ss\");";
	}
	private function Accounting_Point($point){
		$ctg = DB::select("SELECT * FROM `Organizations` WHERE id='$point' LIMIT 1");
		$cat = $ctg[0];
		return General::DecodeTitle($cat->OrganizationName);
	}
	private function Accounting_Courier_($request){
		$Query_="SELECT * FROM `OrderTEMP` WHERE expectedDeliverTime BETWEEN "
		."STR_TO_DATE('".self::$Data_Report[0]."','%Y-%m-%d %H:%i:%s') AND "
		."STR_TO_DATE('".self::$Data_Report[1]."','%Y-%m-%d %H:%i:%s') "
		."&& isCafe=0 && isCanceled=0 && organizationId='". $request->point ."' && isClientDelivery=0 && "
		."Courier_Id='". self::$Accounting_['name_id'] ."'";
		$ctg = DB::select($Query_);
		foreach($ctg as $cat){
			self::$Accounting_Dann[0]++;
			if(!is_null($cat->deliveredDateTime) && $cat->deliveredDateTime!='0000-00-00 00:00:00') self::$Accounting_Dann[1]++;
			self::Accounting_Sum((array)$cat);
		}
		$cl="flex flex-align-center flex-justify-between";
		return "<div class='m-accounting flex-bag $cl'><span>Заказов</span>"
				."<span>". self::$Accounting_Dann[0] ."</span></div>"
			."<div class='m-accounting flex-bag1 $cl'><span>Подтверждённых</span>"
				."<span>". self::$Accounting_Dann[1] ."</span></div>"
			."<div class='m-accounting flex-mani $cl'><span>Наличных</span>"
				."<span>". self::$Accounting_Dann[2] ."</span></div>"
			."<div class='m-accounting flex-map $cl'><span>Безналичных</span>"
				."<span>". self::$Accounting_Dann[3] ."</span></div>"
			."<div class='m-accounting flex-online $cl'><span>Онлайн оплата</span>"
				."<span>". self::$Accounting_Dann[4] ."</span></div>"
			."<div class='m-accounting flex-red $cl'><span>Задолжность</span>"
				."<span>". self::$Accounting_Dann[5] ."</span></div>";
	}
	private function Accounting_Sum($cat){
		$payments=preg_replace('/[\x00-\x1F\x7F\xA0]/u','',$cat['payments']);
		$s=json_decode($cat['payments'],true);
		if(!is_array($s)) return "";
		$mani=[];
		$itog=0;
		foreach($s as $key=>$m){
			$cl = "";
			if($m['Type']['Name']=="Наличные"){
				self::$Accounting_Dann[2] += $m['Sum'];
				if(!$cat['refund']) self::$Accounting_Dann[5] += $m['Sum'];
			}elseif($m['Type']['Name']=="Visa"){
				self::$Accounting_Dann[3] += $m['Sum'];
			}else self::$Accounting_Dann[4] += $m['Sum'];
			$itog += $m['Sum'];
		}
		if($cat['resultSum']<$itog && !$cat['refund']) 
			self::$Accounting_Dann[5] += $itog-$cat['resultSum'];
	}

} //END
