<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HomeController as General;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use DateTime;

class VyruchkaController extends Controller
{
    public static $user;
    private static $Data_Report;
    private static $Sum_Time_Dat;
    private static $IdListOrg;
    private static $ArrayListOrg;
    private static $Sum_Time;
    private static $List_Courier;
    private static $Courier_Get;
	private static $One_Courier;
    private static $Delivery_Ok;
    private static $Dat;
    public static $Late_Min=[0,60,-1];
    private static $QUERYSTRING;
    public static $Late_Where;
    public static $Late_Where_Null;
    private static $sek_Kitchen;

    public function index(Request $request){
		$user_points = [];
        if(isset($request->point_multi)){
            $m = (array)$request->all();
            unset($m['_token']);
            self::$QUERYSTRING = http_build_query($m);
            $user_points = (array)$request->point_multi;
            General::session_param_put($m);
        }
        self::$user = Auth::user();
        $cmsmenu=DB::select("SELECT * FROM bis_cmsmenu WHERE script='vyruchka' LIMIT 1");
		$point = DB::select("SELECT * FROM `Organizations` WHERE Conception='ТиЧ' && "
            ."Id in(SELECT id FROM userlist_city WHERE parent=". self::$user->sysid .") ORDER BY OrganizationName");
        return view('vyruchka',['user'=>self::$user,'cmsmenu'=>(array)$cmsmenu[0],'points'=>$point,'user_points'=>$user_points]); 
    }

	public function show($param,$points){
		$ss = "";
		self::$Sum_Time[0]=["С {$param['date1']} по {$param['date2']}",""];
		//return "SELECT * FROM `Organizations` WHERE Id in('". implode("','",$points) ."') ORDER BY OrganizationName";
		$ctg = DB::select("SELECT * FROM `Organizations` WHERE Id in('". implode("','",$points) ."') ORDER BY OrganizationName");
		foreach($ctg as $cat){ 
			$ss .= "<div class='proceeds-tr'>";
				$ss .= "<h3>". General::DecodeTitle($cat->OrganizationName)."</h3>";
				$ss .= self::Proceeds_Point("organizationId='". $cat->Id ."' && ",$param);
			$ss .= "</div>";
		}
		return $ss;
	}

	private function Proceeds_Point($usl,$param){
		self::$Sum_Time[1]=[0,0,""]; //Выручка
		self::$Sum_Time[2]=[0,0,""]; //Доставка
		self::$Sum_Time[3]=[0,0,""]; //Самовывоз
		self::$Sum_Time[4]=[0,0,""]; //Ресторан
		
		$d1=implode("-",array_reverse(explode(".",$param['date1'])));
		$d2=implode("-",array_reverse(explode(".",$param['date2'])));
		self::Data_Report($d1,$d2);
		self::$Sum_Time_Dat[0]=self::$Data_Report[0];
		self::$Sum_Time_Dat[1]=self::$Data_Report[1];
		
		$query="SELECT * FROM `OrderTEMP` WHERE "
		."expectedDeliverTime BETWEEN STR_TO_DATE('". self::$Sum_Time_Dat[0] ." 00:00:00','%Y-%m-%d %H:%i:%s') AND "
		."STR_TO_DATE('". self::$Sum_Time_Dat[1] ." 23:59:59','%Y-%m-%d %H:%i:%s') && $usl isCanceled=0";
		$ctg = DB::select($query);
		self::Proceeds_Point_Sum($ctg,0);
		
		self::$Sum_Time_Dat[2] = new DateTime(self::$Sum_Time_Dat[0]);
		self::$Sum_Time_Dat[3] = new DateTime(self::$Sum_Time_Dat[1]);
		$interval = self::$Sum_Time_Dat[3]->diff(self::$Sum_Time_Dat[2]);
		$day = ($interval->d<=7)?8:$interval->d;
		self::$Sum_Time_Dat[2]->modify("-$day day");
		self::$Sum_Time_Dat[3]->modify("-$day day");
		
		self::$Sum_Time[0][1]="С ".self::$Sum_Time_Dat[2]->format('d.m.Y')
			." по ".self::$Sum_Time_Dat[3]->format('d.m.Y');
		
		$query="SELECT * FROM `OrderTEMP` WHERE "
		."expectedDeliverTime BETWEEN STR_TO_DATE('". self::$Sum_Time_Dat[2]->format('Y-m-d H:i:s') 
		."','%Y-%m-%d %H:%i:%s') AND "
		."STR_TO_DATE('". self::$Sum_Time_Dat[3]->format('Y-m-d H:i:s')
		."','%Y-%m-%d %H:%i:%s') && $usl isCanceled=0";
		$ctg = DB::select($query);
		self::Proceeds_Point_Sum($ctg,1);
		if(self::$Sum_Time[1][1]>0){
			self::$Sum_Time[1][2]=round((self::$Sum_Time[1][0]-self::$Sum_Time[1][1])/self::$Sum_Time[1][1]*100,0);
			self::$Sum_Time[1][2]=self::$Sum_Time[1][2]<0
				?"&nbsp;&nbsp;<em style='color:#DC243E'>". self::$Sum_Time[1][2] ."%</em>"
				:"&nbsp;&nbsp;<em style='color:#44C568'>+". self::$Sum_Time[1][2] ."%</em>";
		}
		if(self::$Sum_Time[2][1]>0){
			self::$Sum_Time[2][2]=round((self::$Sum_Time[2][0]-self::$Sum_Time[2][1])/self::$Sum_Time[2][1]*100,0);
			self::$Sum_Time[2][2]=self::$Sum_Time[2][2]<0
				?"&nbsp;&nbsp;<em style='color:#DC243E'>". self::$Sum_Time[2][2] ."%</em>"
				:"&nbsp;&nbsp;<em style='color:#44C568'>+". self::$Sum_Time[2][2] ."%</em>";
		}
		if(self::$Sum_Time[3][1]>0){
			self::$Sum_Time[3][2]=round((self::$Sum_Time[3][0]-self::$Sum_Time[3][1])/self::$Sum_Time[3][1]*100,0);
			self::$Sum_Time[3][2]=self::$Sum_Time[3][2]<0
				?"&nbsp;&nbsp;<em style='color:#DC243E'>". self::$Sum_Time[3][2] ."%</em>"
				:"&nbsp;&nbsp;<em style='color:#44C568'>+". self::$Sum_Time[3][2] ."%</em>";
		}
		if(self::$Sum_Time[4][1]>0){
			self::$Sum_Time[4][2]=round((self::$Sum_Time[4][0]-self::$Sum_Time[4][1])/self::$Sum_Time[4][1]*100,0);
			self::$Sum_Time[4][2]=self::$Sum_Time[4][2]<0
				?"&nbsp;&nbsp;<em style='color:#DC243E'>". self::$Sum_Time[4][2] ."%</em>"
				:"&nbsp;&nbsp;<em style='color:#44C568'>+". self::$Sum_Time[4][2] ."%</em>";
		}
		return "<div class='proceeds-td'>". self::$Sum_Time[0][0] ."</div>"
		."<div class='proceeds-td'>".number_format(self::$Sum_Time[1][0],0,'',' ').self::$Sum_Time[1][2] ."</div>"
		."<div class='proceeds-td'>".number_format(self::$Sum_Time[2][0],0,'',' ').self::$Sum_Time[2][2] ."</div>"
		."<div class='proceeds-td'>".number_format(self::$Sum_Time[3][0],0,'',' ').self::$Sum_Time[3][2] ."</div>"
		."<div class='proceeds-td'>".number_format(self::$Sum_Time[4][0],0,'',' ').self::$Sum_Time[4][2] ."</div>"
		."<div class='proceeds-td'>". self::$Sum_Time[0][1] ."</div>"
		."<div class='proceeds-td'>".number_format(self::$Sum_Time[1][1],0,'',' ')."</div>"
		."<div class='proceeds-td'>".number_format(self::$Sum_Time[2][1],0,'',' ')."</div>"
		."<div class='proceeds-td'>".number_format(self::$Sum_Time[3][1],0,'',' ')."</div>"
		."<div class='proceeds-td'>".number_format(self::$Sum_Time[4][1],0,'',' ')."</div>";
	}
	private function Proceeds_Point_Sum($ctg,$k){
		foreach($ctg as $cat){
			self::$Sum_Time[1][$k] += (int)$cat->resultSum; //Выручка
			if($cat->isClientDelivery==0 && $cat->isCafe==0)
				self::$Sum_Time[2][$k] += (int)$cat->resultSum; //Доставка
			if($cat->isClientDelivery==1 && $cat->isCafe==0)
				self::$Sum_Time[3][$k] += (int)$cat->resultSum; //Самовывоз
			if($cat->isCafe==1)self::$Sum_Time[4][$k] += (int)$cat->resultSum; //Ресторан
		}
	}

	public function Data_Report($d1,$d2){
		self::$Data_Report[0] = date("Y-m-d H:i:s",strtotime("$d1 00:00:00"));
		self::$Data_Report[1] = date("Y-m-d H:i:s",strtotime("$d2 23:59:59"));
	}

} //END
