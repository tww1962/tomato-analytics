<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LoginController@index');
Route::get('/vsya-analitika','AnalyticsController@index');
Route::post('/vsya-analitika','AnalyticsController@show');
Route::match([
	'get',
	'post'
], '/dostavka-i-kurery', 'DeliveryController@index');
Route::post('/dostavka-i-kurery-modal', 'DeliveryController@modal');
Route::match([
	'get',
	'post'
], '/rejting-kurerov', 'RatingController@index');
Route::match([
	'get',
	'post'
], '/vyruchka', 'VyruchkaController@index');
Route::match([
	'get',
	'post'
], '/uchyot-i-vedenie', 'AccountingController@index');
Route::match([
	'get',
	'post'
], '/sales', 'SalesController@index');
Route::match([
	'get',
	'post'
], '/personal-area', 'PersonalController@index');

Route::post('/login', 'LoginController@loginUser');
Route::post('/logout', 'LoginController@logout');
Route::group([
	'prefix'     => 'admin',
	'namespace'  => 'Admin'
], function () {
	Route::get('/', 'DashboardController@index')->name('admin.start');
	Route::post('/authorization', 'DashboardController@store')->name('admin.authorization');
	Route::get('/exit', 'DashboardController@destroy')->name('admin.exit');
	Route::get('/content', 'AdminIndexController@index')->name('admin.content');
	Route::get('/content/{module}', 'AdminIndexController@show');
	Route::post('/croppimages', 'AdminSheetLogoController@index')->name('admin.croppimages');
	Route::post('/ajaxfull/{module}', 'AdminAjaxController@index');
	Route::post('/ajaxuploadfile/{module}', 'AdminUploadController@index');
	Route::post('/ajaxyoutube/{module}', 'AdminYoutubeController@index');
	Route::post('/ajaxgoogleuser', 'AdminGoogleUserController@index');
	Route::post('/ajaxadministrator', 'AdministratorController@save');
	Route::post('/ajaxsetting/{module}', 'AdminSettingController@save');
	Route::post('/ajaxclient','AdminClientController@update');
});

