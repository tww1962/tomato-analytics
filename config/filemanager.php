<?php
return [
    'base_route'      => 'admin/filemanager',
    'middleware'      => ['web'], //, 'auth'
    'allow_format'    => 'jpeg,jpg,png,gif,webp,ico,pdf,txt,doc,docx,xls,xlsx,mp3,rtf',
    'max_size'        => 10*1024,
    'max_image_width' => 1920,
    'image_quality'   => 100,
];