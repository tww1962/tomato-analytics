<?php
return [
    //фотогалерея
    'SIZE_MAX' => 5*1024*1024, //макс размер фото в фотогалереи при загрузки
    'KOLMAXFILE' => 10, //макс кол-во фото в фотогалереи при загрузки
    'size_big' => [900,900],
    'size_average' => [650,650],
    'size_small' => [450,450],
    'allow_format_photo' => ['jpeg','jpg','png','gif','webp','ico','bmp'],
    'preview_photo' => 'small',
    // слайдер/банер
    'SIZE_MAX_SLIDER' => 5*1024*1024, //макс размер фото в слайдер
    'KOLMAXFILE_SLIDER' => 10, //макс кол-во фото в сладер при загрузки
    'allow_format_slider' => ['jpeg','jpg','png','gif','webp','ico','bmp'],
    //документы
    'allow_format_doc' => ['jpeg','jpg','png','gif','webp','ico','pdf','txt','doc','docx','xls','xlsx','mp3','rtf','bmp'],
]
?>