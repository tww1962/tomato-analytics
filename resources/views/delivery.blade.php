<?php
use App\Http\Controllers\HomeController as General;
use App\Http\Controllers\DeliveryController as Main;
$checked = "";
$d1 = $d2 = date('d.m.Y');
$check_filtr = ["","",""];
if(!is_null(session('param'))){
    $param = json_decode(session('param'),true);
    if(is_array($param['point_multi'])) $checked=$param['point_multi'][0];
    else $checked=$param['point_multi'];
    $param['point_multi'] = $checked;
    $d1 = $param['date1'];
    $d2 = $param['date2'];
    $check_filtr=['','',''];
    if(isset($param['late'])){
        $check_filtr[0] = isset($param['late'][0]) ? "checked" : '';
        $check_filtr[1] = isset($param['late'][1]) ? "checked" : '';
        $check_filtr[2] = isset($param['late'][2]) ? "checked" : '';
    }
}
?>
@extends('index')
@section('content')
<header class='flex delivery'>
    <section class='header-left'>
        <h1 class='namePages'>{{ General::DecodeTitle($cmsmenu['nameh1']) }}</h1>
        <form id='analinik_form' method='POST'>
            <div class='delivery-point flex flex-wrap flex-align-center'>
                <span><strong>Точки продаж:</strong></span>
                <select name='point_multi' id='list_full_point'>
                    <option value=''>Выбрать</option>
                    @foreach ($points as $point)
                    <?php $check = $point->Id==$checked ? 'selected' : ''; ?>
                    <option value='{{ $point->Id }}' {{ $check }} >{{ General::DecodeTitle($point->OrganizationName) }}</option>
                    @endforeach
                </select>
            </div>
            <section class='period-filter-submit  flex flex-align-center'>
                <div class='delivery-period flex flex-align-center'>
                    <label>Период:</label>
                    <span>с</span><div class='delivery-date1'>
                        <input type='text' readonly name='date1' id='delivery_date1' value="{{ $d1 }}" />
                    </div>
                    <span>по</span><div class='delivery-date2'>
                        <input type='text' readonly name='date2' id='delivery_date2' value="{{ $d2 }}" />
                    </div>
                </div>
                <div class='delivery-filter flex flex-align-center'>
                    <span class='icon-sliders'></span><span>Фильр</span>
                    <div class='popup-filter'>
                        <div><input type='checkbox' value='0' name='late[0]' id='late0' {{ $check_filtr[0] }} />
				            <label for='late0'>Опоздание {{ Main::$Late_Min[0]."+/".Main::$Late_Min[1] }}</label>
                        </div>
                        <div><input type='checkbox' value='1' name='late[1]' id='late1' {{ $check_filtr[1] }} />
				            <label for='late1'>Опоздание {{ Main::$Late_Min[1] }}+</label>
                        </div>
                        <div><input type='checkbox' value='2' name='late[2]' id='late2' {{ $check_filtr[2] }} />
				            <label for='late2'>Данные отсутствуют</label>
                        </div>
                    </div>
                </div>
                <div class='delivery-reload'>Получить данные</div>
            </section>
            @csrf
        </form>
        <div class='delivery-help flex flex-align-center'>
            <div>О - Отправлено</div>
            <div>Д - Доставлено</div>
            <div class='delivery-no-delays'>- Без опозданий</div>
            <div class='delivery-late'>- Опоздание 0+ / 60</div>
            <div class='delivery-minus-to-salary'>- Опоздание 60+</div>
        </div>
    </section>
    <section class='header-right'>{!! General::User($user) !!}</section>
</header>
<div class='delivery-tr delivery-tr_head'>
	<div class='delivery-td'>Номер</div>
	<div class='delivery-td'>Создан</div>
	<div class='delivery-td'>Доставить к</div>
	<div class='delivery-td'>Зона</div>
	<div class='delivery-td'>Кухня</div>
	<div class='delivery-td'>Назначен</div>
	<div class='delivery-td'>Доставлено</div>
	<div class='delivery-td'>В пути</div>
	<div class='delivery-td'>Всего</div>
	<div class='delivery-td'>Курьер</div>
</div>
<?php
if(isset($param)) echo Main::show_reload($param);
?>
@endsection