<?php
use App\Http\Controllers\HomeController as General;
use App\Http\Controllers\SalesController as Main;
$d1 = $d2 = date('d.m.Y');
?>
@extends('index')
@section('content')
<header class='flex sales-promo' style='margin-bottom:30px'>
    <section class='header-left'>
        <h1 class='namePages'>{{ General::DecodeTitle($cmsmenu['nameh1']) }}</h1>
        <form id='analinik_form' method='POST'>
            <section class='period-filter-submit  flex flex-align-center'>
                <div class='delivery-period flex flex-align-center'>
                    <label>Период:</label>
                    <span>с</span><div class='delivery-date1'>
                        <input type='text' readonly name='date1' id='delivery_date1' value="{{ $d1 }}" />
                    </div>
                    <span>по</span><div class='delivery-date2'>
                        <input type='text' readonly name='date2' id='delivery_date2' value="{{ $d2 }}" />
                    </div>
                </div>
                <div class='analitik-reload'>Получить данные</div>
            </section>
            <div class='delivery-point flex flex-wrap flex-align-center'>
                <p><strong>Точки продаж:</strong></p>
                <div class='sales-point flex flex-wrap flex-align-center'>
                @foreach ($points as $point1)
                <?php $point = (array)$point1; ?>
                <div class='flex flex-align-center'>
                    <input type='checkbox' checked name='sales_point[]' data-sid="{{ $point['Department.Id'] }}" id="sp{{ $point['sysid'] }}" />
                    <label for="sp{{ $point['sysid'] }}">{{ General::DecodeTitle($point['Department']) }}</label>
                </div>
                @endforeach
                </div>
                <div class='dish-selected flex flex-wrap' style='width:100%'></div>
                <button type='button' class='modal-active'>Выбрать блюдо(а)</button>
            </div>
            @csrf
        </form>
    </section>
    <section class='header-right'>{!! General::User($user) !!}</section>
</header>
<div class='sales'></div>
@endsection