<?php
use App\Http\Controllers\HomeController as General;
$body_class = Auth::check()?"":"body-home";
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=3.0">
	<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
	<meta content="telephone=no" name="format-detection">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="shortcut icon" href="/favicon.ico?<?= filemtime("./favicon.ico");?>" type="image/x-icon" />
	<title>Аналитика ТиЧ</title>
	<link rel="stylesheet" type="text/css" href="/css/jquery-ui.min.css"/>
	<link rel='stylesheet' type='text/css' href='/css/style.css?<?= filemtime("./css/style.css");?>' />
</head>
<body class='<?= $body_class;?>'>
@if(Auth::check())
<div class='fixed-menu-button'>
	<!--<div class='fixed-filter'>Фильтр</div>-->
	<div class='for-burger flex flex-align-center'>
		<div class='burger'><span></span><span></span><span></span></div>
		<div>Меню</div>
	</div>
</div>
<section class='content'><div class='window-load'><img src='/img/loading.gif' alt='Ожидайте' /></div>
{!! General::glMenu(Auth::user()) !!}
<section class='content-right'><div class='main'>
@endif
@hasSection('content')
	@yield('content')
@endif
@if(Auth::check())
</div></section>
</section>
@endif
<script src="/js/jquery-3.6.0.min.js?1"></script>
<script src="/js/jquery-ui.min.js"></script>
<span class='strTop no-mobil back-top'></span>
<div id='BoxModalMain'><div id='Dialog'></div></div>
<div id='Modal_Sales'><div id='Modal_Sales_'>
	<div class='modal-sales_header'>
		<input type='text' name='find_dish' id='find_dish' placeholder='Поиск' />
	</div>
	<div class='modal-sales_content'></div>
	<div class='modal-sales_button flex flex-align-center flex-justify-between'>
		<button type='button' class='goto-sales'>Готово</button>
		<button type='button' class='close-modal-sales'>Отмена</button>
	</div>
</div></div>
<script src="/js/main.js?<?= filemtime("./js/main.js");?>"></script>
<iframe name='formAjax' id='formAjax' style='display:none !important'></iframe>
<?php if(isset($cmsmenu)) echo General::ActiveMenu($cmsmenu['sysid']); ?>
</body>
</html>
