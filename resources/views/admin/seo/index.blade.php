<?php
use App\Http\Controllers\Admin\AdminGlobalController as General;
use App\Http\Controllers\Admin\AdminSheetController as Sheet;
?>
@extends('admin.index')
@section('content')
	<?php 
	$sysid = isset($MassParam['sysid'])?(int)$MassParam['sysid']:(int)$MassParam['id']; 
	echo General::MenuSheet($MassParam,$sysid,$url);
	Sheet::VariablesPages((array)General::$FeatureGlobalMenu,$sysid,$MassParam);
	$return = "/admin/content/". Sheet::$RecordCmsMenu['modul'] ."/?". http_build_query($MassParam);
	$t = $k = $d = "";
	$result=DB::select("SELECT * FROM bis_pagesSeo WHERE parent=$sysid LIMIT 1");
	if(!empty($result)){
		$t = $result[0]->seo_title;
		$k = $result[0]->seo_key;
		$d = $result[0]->seo_desc;
	}
	$it = [800 - mb_strlen($k,'UTF-8') , 250 - mb_strlen($d,'UTF-8')];
	?>
	<form name='frmSeo' id='frmSeo'>
		<p><strong>SEO ( данные для поисковых роботов)</strong></p>
		<p>Название:</p><p><input type='text' class='input' style='width:600px;' name='t' id='t' value="{{ $t }}" /></p>
		<p>Поисковые слова через запятую (до 800 знаков). Осталось : <span id='kolk'>{{ $it[0] }}</span></p>
		<p><textarea name='k' class='input' style='height: 100px;' id='k' onkeyup='kolznak1(this)'>{{ $k }}</textarea></p>
		<p>Описание (до 250 знаков). Осталось : <span id='kold'>{{ $it[1] }}</span></p>
		<p><textarea name='d' class='input' style='height: 100px;' id='d' onkeyup='kolznak1(this)'>{{ $d }}</textarea></p>
		<input type='hidden' name='parent' value='{{ $sysid }}' /><input type='hidden' name='return' value='{{ $return }}' />
		@csrf
		<?= General::GlobalSubmit($return);?>
	</form>
	<script>
	$( function() {
		$('#frmSeo').submit( function(){
			let o=$(this),o1=o.serialize();
			$.post('/admin/ajaxfull/seo',o1,function(data){
				if(!data['success']) alert(data['msg']);
				else {
					if(data['return'].length<1) MessageAct(data['msg']);
					else location.href=data['return'];
				}
			});
			return false;
		});

	});
	</script>
@endsection
