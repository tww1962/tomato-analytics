<?php
use App\Http\Controllers\Admin\AdminGlobalController as General;
use App\Http\Controllers\Admin\AdminClientController as Client;
?>
@extends('admin.index')
@section('content')
	<?php 
	$sysid = isset($MassParam['sysid'])?(int)$MassParam['sysid']:(int)$MassParam['id'];
	$m=json_decode(request()->cookie("user_admin"),true);
	if(!is_array($m))  return redirect()->route('admin.start');
	$ParentUser = $m['id_user'];
	$ss = "";
	$ss .= "<div style='margin-bottom:15px'><button class='button new-client' data-parent='{$m['id_user']}' type='button'>Добавить</button></div>";
	if(isset($MassParam['sysid'])){
		$ss .= Client::edit($MassParam);
	}else{
		if($m['superroot']=='yes') $query = "SELECT * FROM bis_user WHERE parent>0 ORDER BY name";
		else $query = "SELECT * FROM bis_user WHERE parent={$m['id_user']} ORDER BY name";
		$ss .= Client::show($query,$MassParam);
	}
	echo $ss;
?>

<script>
var csrf_token = "{{ csrf_token() }}";
$( function() {
	$('#frmClient').submit(function(){
		let o=$(this),o1=o.serialize();
		$.post('/admin/ajaxclient',o1,function(data){
			if(!data['success']) alert(data['msg']);
			else {
				if(data['return'].length<1) MessageAct(data['msg']);
				else location.href=data['return'];
			}
		});
		return false;
	});

	$('.new-client').click(function(){
		var $this = $(this),parent = $this.data('parent'),
		s = "<p style='margin-bottom:15px'>"
			+"<input type='text' class='input' name='name' id='name' placeholder='Имя' /></p>";
		s += "<p><input type='text' class='input' name='surname' id='surname' placeholder='Фамилия' /></p>";
		s += "<p class='axtung'>Все поля обязательны для заполнения !</p>";
		$("#dialog-form").html(s);
		$("#dialog-form").dialog({ buttons: { 
			Сохранить: function() {
				$('#name').removeClass("ui-state-error");
				$('#surname').removeClass("ui-state-error");
				var name= $('#name').val(),surname= $('#surname').val();
				if(name.length<1) $('#name').addClass("ui-state-error");
				else if(surname.length<1) $('#surname').addClass("ui-state-error");
				else {
					$.post('/admin/ajaxfull/client',{'parent':parent,'name':name,'surname':surname,'_token':csrf_token},
					function(data){
						if(!data['success']) return alert(data['msg']);
						location.reload(true);
					});
				}
			},
			Отмена: function() { $("#dialog-form").dialog( "close" ); }
		}});
		$("#dialog-form").dialog( "open" );
	});
	$('#frmlogo_cropp').click( function () { 
		$('#frmlogo_flag').val('2');
		cropp_function();
	});
	function cropp_function(){
		let size=$('#width_height_img').val().split('#'),photo_src=$('img','#fotoImg').attr('src').split('?');
		$('#Cropp2').css({width: size[0]+'px',height: size[1]+'px',marginLeft:'-'+(size[0]/2)+'px',marginTop:'-'+(size[1]/2)+'px'});
		$('#CroppFoto').css({width: size[0]+'px',height: size[1]+'px'});
		var croppicContainerPreloadOptions = {
			cropUrl:'/admin/croppimages',
			loadPicture:photo_src[0],
			enableMousescroll:true,
			loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
			onAfterImgCrop:function(){ 
				let s=Math.ceil(Date.now() / 1000);
				$("#fotoImg").html("<img src='"+photo_src[0]+"?"+s+"' width='100%; height: auto;' />");
				CroppFoto.destroy(); 
				$('#Cropp1').slideUp(150, function() {$("#CroppFoto").html('');} ); 
			},
			onReset:function(){ console.log('onReset') },
			onError:function(errormessage){ alert(errormessage); }
		}
		CroppFoto = new Croppic('CroppFoto', croppicContainerPreloadOptions);
		$('#CroppFoto').append("<input type='hidden' id='CroppFoto_token' name='_token' value='{{ csrf_token() }}' />");
		$('#Cropp1').slideDown(300);
	};

	$('#frmlogo_download').click(function(){
		$('#frmlogo_flag').val('1');
		$('#fotoInput').trigger('click');
	});
	$('#fotoInput').change(function(){
		$('#dialog').html('<p>Идет загрузка файла...</p>');
		$('#dialog').dialog('open');
		var o=$('#frmLogo'), formData = new FormData();
		formData.append('photo', document.getElementById('fotoInput').files[0]);
		formData.append('flag', 1);
		formData.append('_token', $('input[name=_token]',o).val());
		formData.append('parent', $('input[name=parent]',o).val());
		
		$.ajax({
			url: '/admin/croppimages',
			async: false,
			type: 'POST',
			contentType: false,
			processData: false,
			data: formData,
			success: function(data){
				if(!data['success']) alert(data['msg']);
				else{
					$('#fotoImg').html("<img src='"+data['msg']+"' />");
					$('#sysid_photo').val(data['id']);
					$('#frmlogo_cropp').css('display','block');
					$('#frmlogo_del').css('display','block');
					$('#dialog').dialog('close');
				}
			},
			error: function(o,err) { alert(err);}
		});
	});

	$('#frmlogo_del').click(function(e){ 
		e.stopPropagation();
		$('#frmlogo_flag').val('3');
		$( "#dialog-confirm" ).html('<p>Удалить <strong>Логотип</strong><p>');
		$( "#dialog-confirm" ).dialog({ buttons: { 
			Нет: function() { $(this).dialog('close'); },
			Да: function() { 
				let o=$('#frmLogo'), o1=o.serialize();
				$.post('/admin/croppimages',o1,function(data){
					if(!data['success']) alert(data['msg']);
					else{
						$('#fotoImg').html("");
						$('#sysid_photo').val('0');
						$('#frmlogo_cropp').css('display','none');
						$('#frmlogo_del').css('display','none');
						$('#dialog-confirm').dialog('close');
					}
				});
			}
		}});
		$('#dialog-confirm').dialog('open');
	});

});
</script>
@endsection
