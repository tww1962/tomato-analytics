<?php
use App\Http\Controllers\Admin\AdminGlobalController as General;
$r = mt_rand(1,10000);
$ABSOLUTMAIN=$_SERVER['DOCUMENT_ROOT'];
if (!file_exists( $ABSOLUTMAIN ."/files/")) { mkdir( $ABSOLUTMAIN ."/files/");}
if (!file_exists( $ABSOLUTMAIN ."/files/users/")) { mkdir( $ABSOLUTMAIN ."/files/users/"); }
if (!file_exists( $ABSOLUTMAIN ."/files/users/upload/")) { mkdir( $ABSOLUTMAIN ."/files/users/upload/"); }
if (!file_exists( $ABSOLUTMAIN ."/files/logo/")) mkdir( $ABSOLUTMAIN ."/files/logo/");
if (!file_exists( $ABSOLUTMAIN ."/files/doc/")) mkdir( $ABSOLUTMAIN ."/files/doc/");
if (!file_exists( $ABSOLUTMAIN ."/files/foto/")) mkdir( $ABSOLUTMAIN ."/files/foto/");
if (!file_exists( $ABSOLUTMAIN ."/files/big/")) mkdir( $ABSOLUTMAIN ."/files/big/");
if (!file_exists( $ABSOLUTMAIN ."/files/average/")) mkdir( $ABSOLUTMAIN ."/files/average/");
if (!file_exists( $ABSOLUTMAIN ."/files/small/")) mkdir( $ABSOLUTMAIN ."/files/small/");
if (!file_exists( $ABSOLUTMAIN ."/files/pdftoimg/")) mkdir( $ABSOLUTMAIN ."/files/pdftoimg/");
if (!file_exists( $ABSOLUTMAIN ."/files/youtube/")) mkdir( $ABSOLUTMAIN ."/files/youtube/");
if (!file_exists( $ABSOLUTMAIN ."/files/featurefiles/")) mkdir( $ABSOLUTMAIN ."/files/featurefiles/");
?>
<!DOCTYPE html>
<html  lang="ru">
<head>
<title>Система управления сайтом</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="Pragma" content="no-cache" />
<link rel="StyleSheet" type="text/css" href="/script_admin/all_script/jquery-ui.css?1"/>
<link href="/script_admin/all_script/croppic.css" rel="stylesheet"/>
<link href="/script_admin/all_script/lightbox.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="/script_admin/font-awesome.css"/>
<style media='print' type='text/css'/>
	* {background: #ffffff !important; color:#000000 !important}
	div.header, div.left, div.main>p  {display:none;}
	div.right ul#zakazUser li, div.right h2, div.right button {display:none;}
	div.right {float:left !important; margin:0 !important;}
	ul#zakazUser li.current {display:block !important;}
	ul#zakazUser li.current>span#dannZakaz {display:block !important;}
	ul#zakazUser li.current>div {display:block !important;}
	ul#zakazUser li.current>div table {display:block !important;}
	span.ui-icon.ui-icon-print {display: none !important}
	p.noPrint {display: none !important}
</style>
<link rel="stylesheet" type="text/css" href="/script_admin/admin.css?<?= $r;?>"/>
<script src="/script_admin/all_script/jquery-1.11.3.min.js"></script>
@FilemanagerScript
</head>
<body>
<?php echo General::NameGlobalMenu(); ?>
<div class='main'>
	<?php echo General::NavLeftContent(); ?>
	@hasSection('content')
		<div class='content'>@yield('content')</div>
	@endif
</div>
<div id='MessageAct' class='MessageAct'></div>
<div id='BoxModalMain'>
	<div id='BoxModalMain1'>
		<img class='close' src='/script_admin/x-lm.png' onclick='closeModal(); return false;'/>
		<div id='Dialog' class='clearfix'></div>
	</div>
</div>
<div id="dialog" title="Минутку"></div>
<div id="dialog-message" title="Внимание"></div>
<div id="dialog-confirm" title="Подтвердите свои действия"></div>
<div id="dialog-form" title="Добавление элемента">
	<form name='createRazdel' id='createRazdel' target='allForm'>
	</form>
</div>
<div id='mask'>
	<h2 style='text-align:center;' class='slovo'>Идет загрузка файлов...</h2>
	<div id='NameFile'></div>
	<div id='rez'><div class="progress"><div class="bar"></div><div class="percent">0%</div></div><div id="status"></div></div>
</div>
<div id='Cropp1'>
	<div id='Cropp2'>
		<input type='button' onclick="CroppFoto.destroy(); $('#Cropp1').slideUp(150, function() {$('#CroppFoto').html('');} );" value='ОТМЕНА' />
		<div id='CroppFoto'></div>
	</div>
	<div id='Cropp3'></div>
</div>
<div style='display:none' id='errSity'></div>
<script src="/script_admin/all_script/jquery-ui.min.js"></script>
<script src="/script_admin/all_script/jquery.ui.datepicker-ru.js"></script>
<script src="/script_admin/ckeditor/ckeditor.js?<?php echo filemtime("./script_admin/ckeditor/ckeditor.js")?>"></script>
<script src="/script_admin/ckeditor/adapters/jquery.js"></script>
<script src="/script_admin/all_script/jquery.mjs.nestedSortable.js?<?php echo filemtime("./script_admin/all_script/jquery.mjs.nestedSortable.js")?>"></script>
<script src="/script_admin/all_script/jquery.mousewheel.min.js"></script>
<script src="/script_admin/all_script/croppic.js?1"></script>
<script src="/script_admin/all_script/lightbox.min.js"></script>
<script src="/script_admin/all_script/jquery.form.js"></script>
<script src="/script_admin/global.js?<?php echo filemtime("./script_admin/global.js")?>"></script>
<?php
$CurrentLeft = "";
if(isset($_GET['id']))
	$CurrentLeft = "$('#nav{$_GET['nav']}').addClass('current'); $('#m{$_GET['id']}').addClass('current'); ";
?>
<script>
	function errAjax(msg){
		$("#dialog-message").attr({title:'Внимание'});
		$("#dialog-message").html(msg);
		$("#dialog-message").dialog("open"); 
	}
	$( function() { <?= $CurrentLeft; ?> });
</script>
</body>
</html>
