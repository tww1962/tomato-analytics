<?
$r = mt_rand(1,10000);
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, max-age=0, must-revalidate"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Expires" content="Fri, 01 Jan 1990 00:00:00 GMT"/>	
	<title>Система Управления Сайтом</title>
	<meta name="rating" content="General" />
	<style>
		html, body{ margin:0; padding:0; width:100%;  font: normal 16px Arial,Verdana; overflow:hidden;}
		body {background-color: #fff; color:#000;}
		* { -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; outline: none !important; }
		a, img {border: none; text-decoration:none;}
		:focus::-webkit-input-placeholder { color: transparent }
		:focus::-moz-placeholder { color: transparent }
		:focus:-moz-placeholder { color: transparent }
		:focus:-ms-input-placeholder { color: transparent }
		.input_text {
			background-color: #eeeeee !important;
			border: 1px dotted #555;
			border-radius: 3px;
			width: 100%;
			height: 45px;
			color: #bbb;
			font-size:16px;
			outline: none;
			box-shadow: inset 0 0 0 50px #eee; 
			-webkit-box-shadow: inset 0 0 0 50px #eee; 
			padding: 0 10px;
			color:#000;
		}
		.LoginDiv {
			width:410px; margin: 50px auto; border:1px solid #555;
			border-radius: 3px; -moz-border-radius: 5px; -webkit-border-radius: 5px;
			padding: 15px 20px;
		}
		.formLogin { min-height:100%; position:relative;}
		.inputDiv { 
			display: -webkit-flex; display: flex;
			-webkit-align-items: center; align-items: center;
			justify-content: center; -webkit-justify-content: center;
			margin-bottom: 12px; 
		}
		.inputDiv.name-form { background-color:#d18a2c; color:#fff; text-align: center; padding: 15px; margin-bottom: 15px !important; }
		.inputDiv:last-of-type { margin-bottom: 0; }
		button {cursor:pointer; border:4px solid #ff594b;  background-color:#fff; width:100%; height:45px; display: block;}
		button:hover {background-color: #ff594b; color:#fff;}
		.logo-adm {position:relative; margin: 0px auto; margin-top:70px; display:block}
		#imgcapt { width: 140px; height: auto; display: block;}
		.clearfix:before, .clearfix:after { content:" "; display:table; width:100%}
	</style>
</head>
<body>
	<img src='/img/logoAdm.png?<?php echo filemtime("./img/logoAdm.png")?>' class='logo-adm' />
	<div class='LoginDiv'>
		<div class='formLogin' id='formLogin'>
			<div class='inputDiv name-form'>АНАЛИТИКА ТиЧ (УПРАВЛЕНИЕ)</div>
			<form>
				<div class='inputDiv'>
					<input type='text' required class='input_text' name='login' id='login' value="" placeholder='Логин' />
				</div>
				<div class='inputDiv'>
					<input type='password' required class='input_text' name='pass' id='pass' value="" placeholder='Пароль' />
				</div>
				<?php /*
				<div class='inputDiv'>
					<img src='capt.php?<?= $r;?>' alt='' id='imgcapt'/>
					<div style='padding-left: 15px'>
						<input type='text' required name='capt' id='capt' placeholder='Цифры слева' /><br />
						<input type='button' value='Не вижу цифры' onclick="document.getElementById('imgcapt').src = 'capt.php?<?= $r;?>'"/>
					</div>
				</div>
				<div class='inputDiv'><label><input type='checkbox' checked name='save' id='save' /> Запомнить меня</label></div>
				*/ ?>
				<div class='inputDiv'><button type='button' class='btn-submit'>
					<span style='font-size:21px; vertical-align:middle;'>Войти</span>
				</button></div>
				@csrf
			</form>
		</div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<script>
	$(function(){
		$('form').submit(function(){ return false; });
		$('form').keydown(function(event){if(event.keyCode == 13){event.preventDefault(); return false;}});
		$('.btn-submit').click(function(e){
			e.preventDefault();
			if($('#pass').val().length<1 || $('#login').val().length<1) return false;
			let o=$(this).closest('form'), dataObject = o.serialize();
			$.post("{{ route('admin.authorization') }}",dataObject,function(){},'script');
		});
	});
	</script>
</body>
</html>
