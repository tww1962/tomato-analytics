<?php
use App\Http\Controllers\Admin\AdminGlobalController as General;
use App\Http\Controllers\Admin\AdminArticlesController as Articles;
use Illuminate\Support\Facades\DB;

?>
@extends('admin.index')
@section('content')
	<?php
	$ss = "";
	$sysid = isset($MassParam['sysid'])?(int)$MassParam['sysid']:(int)$MassParam['id'];
	$ParentCat = !isset($MassParam['parent'])?(int)$MassParam['id']:(int)$MassParam['parent'];
	Articles::VariablesArticles(General::$FeatureGlobalMenu,$sysid,$MassParam,$ParentCat);
	echo General::MenuArticles($MassParam, $sysid, $ParentCat, $url);

	if( General::$FeatureGlobalMenu->firstKol > 0 && !isset($_GET['find']) ) $ss .= Articles::CategoriesSelect();

	$result = DB::select("SELECT * FROM bis_pages WHERE parent=$ParentCat ORDER BY "
		.General::$FeatureGlobalMenu->sortRazdel);
	$ss .= "<ul id='sortable' class='sortableRazdel general-ul all-list' data-tables='bis_pages'>";
	foreach($result as $cat1){
		$cat=(array)$cat1;
		$sid = $cat['sysid']; 
		$nik = htmlspecialchars_decode($cat['name']); 
		$parent = $cat['parent'];

		$priceVisible = strpos(General::$FeatureGlobalMenu->setting_dop,"#price#")!== false
			?"&nbsp;<strong>[". round($cat['price'],config("app.DECI")) ."]</strong>"
			:"";
		
		$dataNews="";
		if(strpos(General::$FeatureGlobalMenu->setting_dop,"#data#") !== false) {
			if( !is_null($cat['data']) && !empty($cat['data']))
				$dataNews="<strong>[". date('d.m.Y',strtotime($cat['data'])) ."]</strong>&nbsp;";
			else $dataNews="<strong>[NONE]</strong>&nbsp;";
		}

		if($cat['hide'] == 'show') { $glaz = "<span class='main-icon'><i class='fa fa-eye' aria-hidden='true' title='Скрыть'></i></span>";} 
		else { $glaz = "<span class='main-icon'><i class='fa fa-eye-slash' aria-hidden='true' title='Отобразить'></i></span>";}
		$ss .= "<li id='li$sid' sid='$sid'>";
			$ss .= "<div class='all-list-item'>";
				$ss .= "<div>";
					$ss .= "<span class='main-icon sort'><i class='fa fa-arrows-v' aria-hidden='true' title='Сортировка'></i></span>";
				$ss .= "</div>";
				$ss .= General::PreviewLogoArticlesCategories($sid);
				$ss .= "<div class='all-list-item-edit'>".
					"<a href='/admin/content/sheet/?sysid=$sid&". http_build_query($MassParam) ."'>$dataNews$nik$priceVisible</a>".
					"</div>";
				$ss .= "<div>";
					$ss .= "<div data-sid='$sid'>";
						if(strpos(General::$FeatureGlobalMenu->setting_dop,"#inGl#") !== false) {
							$cl = ( $cat['inGl'] == 1 ) ? "current" : "";
							$ss .= "<span class='main-icon $cl xit' data-pole='inGl'><i class='fa fa-bolt' title='На главную'></i></span>";
						}
						if(strpos(General::$FeatureGlobalMenu->setting_dop,"#inNew#") !== false) {
							$cl = ( $cat['inNew'] == 1 ) ? "current" : "";
							$ss .= "<span class='main-icon $cl xit' title='Новость' data-pole='inNew'><i class='fa fa-paper-plane'></i></span>";
						}
						if(strpos(General::$FeatureGlobalMenu->setting_dop,"#inArxiv#") !== false) {
							$cl = ( $cat['inArxiv'] == 1 ) ? "current" : "";
							$ss .= "<span class='main-icon $cl xit' title='Акция' data-pole='inArxiv'><i class='fa fa-thumbs-o-up'></i></span>";
						}
					$ss .="</div>";
					$ss .= "<div data-sid='$sid' data-name='$nik' data-parent='#li$sid'>";
						$ss .= $glaz;
						$ss .= "<span class='main-icon'><i class='fa fa-trash-o' aria-hidden='true' title='Удалить'></i></span>";
					$ss .="</div>";
				$ss .= "</div>";
			$ss .="</div>";
		$ss .= "</li>";
	}
	$ss .= "</ul>";
	echo $ss;
	?>
<script>
var csrf_token = "{{ csrf_token() }}";
$(function() {
	$( '#datepickerFind1').datepicker({ firstDay:1, dateFormat:'dd.mm.yy', changeMonth: true, changeYear: true });
	$( '#datepickerFind2').datepicker({ firstDay:1, dateFormat:'dd.mm.yy', changeMonth: true, changeYear: true });
	$( '#datepickerM').datepicker({ firstDay:1, dateFormat:'dd.mm.yy', changeMonth: true, changeYear: true });
	
	$(document).on('click','.filterGo', function() {
		$('#filter').toggleClass('current');
		return false;
	});
	$('#frmFind').submit( function(){
		var frm = $(this), flag = 0;
		$("input[type='text']",frm).each( function() { if( $(this).val().length > 0 ) { flag=1; return false; } });
		$("input[type='checkbox']:checked", frm).each( function() { flag=1; return false; });
		$("input[type='radio']:checked", frm).each( function() { flag=1; return false; });
		if( flag == 1 ) {
			var s = frm.serialize(), ss = window.location.href;
			ss = delPrm(ss,'find');
			window.location.href = ss + '&find=' + encodeURIComponent(s);
			return false; 
		} else { errAjax("<h3>Нет условия для поиска!</h3>"); return false; }
	});

	$('.newPages').click(function(){
		var $this = $(this), level = $this.data('level'), parent = $this.data('parent'), news = 1 * $this.data('date'),
		s = "<p><input type='text' class='input' name='namemodul' id='namemodul' placeholder='Название' /></p>";
		s += ( news ) ?"<p style='padding: 15px 0 0'><input type='text' class='input' name='datamodul' id='datepickerM' readonly style='width:90px;' placeholder='Дата' /></p>":"";
		s += "<p class='axtung'>Все поля обязательны для заполнения !</p>";
		//s += "<input type='hidden' name='nameT' value='' id='nameT' />";
		$( "#dialog-form" ).html(s);
		$("#dialog-form").dialog({ buttons: { 
			Сохранить: function() {
				$('#namemodul').removeClass("ui-state-error");
				var name = $('#namemodul').val(), data = ( news ) ? $('#datepickerM').val() : "";
				if( name.length < 1 ) { 
					$('#namemodul').addClass("ui-state-error");
					$('#namemodul').focus();
				} else if( news && data.length < 1 ){
					$('#datepickerM').addClass("ui-state-error");
					$('#datepickerM').focus();
				} else {
					var has = window.location.href;
					//console.log( id +' = '+ tables+' = '+ ForPrepend+' = '+level+' = '+has);
					$.post('/admin/ajaxfull/articles',
					{parent:parent,name:name,level:level,data:data,'_token':csrf_token},
					function(data){
						window.location.reload(true);
					});
				}
			},
			Отмена: function() { $("#dialog-form").dialog( "close" ); }
		}});
		$("#dialog-form").dialog( "open" );
		$( '#datepickerM').datepicker({ firstDay:1, dateFormat:'dd.mm.yy', changeMonth: true, changeYear: true });
	});
	$('.active-categories').click( function(){
		$('.list-categories').toggleClass('active');
		$('.from-input>input').val('').focus();
		$('.list-categories>ul').find('a').css('display','block');
	});
	$('#findcat').on('keyup',function(){
		var str = $(this).val().toLowerCase(), str1 ;
		$('li>a','.list-categories').each( function(){
			str1 = $(this).text().toLowerCase(); 
			nom = str1.indexOf(str);
			if( nom == -1 ) {
				$(this).css('display','none');
			} else {
				$(this).css('display','block');
			}
		});
	});
	$(document).click( function(e) { 
		if( $(e.target).closest(".select-categories").length ) return;
		$(".list-categories").removeClass('active');
		e.stopPropagation();
	});
	
	$('#CategoriesSelectForPages').on('click', '.enterRazdel', function(e) {
		e.stopPropagation();
		e.preventDefault();
		var el = $(this), s = el.data('parent'), t = el.text();
		$('#CategoriesSelectForPages').slideUp(150);
		$('#CategoriesSelectForPages').find('a.current').removeClass('current');
		$('#CategoriesSelect').val(t);
		el.addClass('current');
		$('#parentFrmDannPages').val(s);
	});
	
	$('.active-brend').click( function(){
		$('.list-brend').toggleClass('active');
		$('.from-input-brend>input').val('').focus();
		$('.list-brend>ul').find('a').css('display','block');
	});
	$('#findbrend').on('keyup',function(){
		var str = $(this).val().toLowerCase(), str1 ;
		$('li>a','.list-brend').each( function(){
			str1 = $(this).text().toLowerCase(); 
			nom = str1.indexOf(str);
			if( nom == -1 ) {
				$(this).css('display','none');
			} else {
				$(this).css('display','block');
			}
		});
	});
	$('li','.list-brend').on('click','a',function(){
		var $this = $(this), sid = $this.data('sid'), s = $this.text(), 
			ul = $this.closest('ul'), idrezult = ul.data('return');
		$(idrezult).val(sid);
		$('.active-brend').text(s);
		$(".list-brend").removeClass('active');
		$('.from-input-brend>input').val('');
	});


});

function delPrm(Url,Prm) {
	var a=Url.split('?');
	var re = new RegExp('(\\?|&)'+Prm+'=[^&]+','g');
	Url=('?'+a[1]).replace(re,'');
	Url=Url.replace(/^&|\?/,'');
	var dlm=(Url=='')? '': '?';
	return a[0]+dlm+Url;
};
</script>
@endsection
