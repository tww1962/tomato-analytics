<?php
use App\Http\Controllers\Admin\AdminGlobalController as General;
use App\Http\Controllers\Admin\AdminSheetController as Sheet;
?>
@extends('admin.index')
@section('content')
	<?php 
	$sysid = isset($MassParam['sysid'])?(int)$MassParam['sysid']:(int)$MassParam['id']; 
	echo General::MenuSheet($MassParam,$sysid,$url);
	Sheet::VariablesPages((array)General::$FeatureGlobalMenu,$sysid,$MassParam);
	if(General::$FeatureGlobalMenu->modul != "sheet"){
		unset($MassParam['pages']);
		$return = "/admin/content/". Sheet::$RecordCmsMenu['modul'] ."/?". http_build_query($MassParam);
	} else {
		$return = "/admin/content/";
	}
	?>
	<p><strong>ID: {{ $sysid }}</strong></p>
	<?php
	echo Sheet::DannLogo();
	$ss = "";
	$ss .= "<form name='frmDann' id='frmDannPages'>";
	if(Sheet::$Table == "bis_pages" && Sheet::$RecordCmsMenu['firstKol'] > 0) {  // выбор категории
		$ss .= Sheet::CategoriesSelect();
		$ss .= "<input type='hidden' name='categories' id='sidcategories' value='". Sheet::$Cat['parent'] ."' />";
	}
	$name = htmlspecialchars_decode(Sheet::$Cat['name'], ENT_QUOTES);
	$nameh1 = htmlspecialchars_decode(Sheet::$Cat['nameh1'], ENT_QUOTES);
	$ss .= "<p><span class='zvezda'>*</span>Название</p>";
	$ss .= "<p><input type='text' class='input' name='name' value='$name' id='name1' required /></p>";
	$ss .= "<p class='for-edit'><span class='zvezda'>*</span>Название h1</p>";
	$ss .= "<p><input type='text' class='input' name='nameh1' value='$nameh1' id='name2' required /></p>";
	if(Sheet::$Table=="bis_pages" && strpos(Sheet::$RecordCmsMenu['setting_dop'],"#barcode#")!==false) { // выбор бренда
		$ss .= "<p class='for-edit'>Ссылка на документ:<br /><input class='input' type='text' name='barcode' value='".Sheet::$Cat['barcode']."' /></p>";
	}
	$info = Sheet::InfoPagesAllModul();
	if(isset(Sheet::$Cat['id1c'])) $ss .= Sheet::Role(explode('#',Sheet::$Cat['id1c']));
	$ss .= Sheet::Sheet_Anons($info[0]);
	$ss .= "<p class='for-edit'>Описание</p>";
	$ss .= "<textarea name='info' id='info' class='main-ckeditor'>{$info[1]}</textarea>";
	$ss .= "<input type='hidden' name='sysid' value='".Sheet::$Sysid."' />"
		."<input type='hidden' name='flag' value='4' />"
		."<input type='hidden' name='return' value='$return' />"
		."<input type='hidden' name='table' value='".Sheet::$Table."' />";
	if(isset(Sheet::$Cat['level'])) $ss .= "<input type='hidden' name='level' value='".Sheet::$Cat['level']."' />";
	
	$ss .= General::GlobalSubmit($return);
	$ss .= "<input type='hidden' name='_token' value='". csrf_token() ."' /></form>";
	echo $ss;
	?>
<script>
var CroppFoto;
$( function() {
	$('#frmDannPages').submit( function(){
		let o=$(this),name = $('#name1').val(), nameh1 = $('#name2').val(), flag = 0,o1=o.serialize();
		if( name.length < 1 ) { $('#name1').addClass("ui-state-error"); flag = 1; }
		if( nameh1.length < 1 ) { $('#name2').addClass("ui-state-error"); flag = 1; }
		if( flag == 1 ) { errAjax("<p>Проверьте заполнение обязательных полей!</p>"); return false; }
		$.post('/admin/ajaxfull/sheet',o1,function(data){
			if(!data['success']) alert(data['msg']);
			else {
				if(data['return'].length<1) MessageAct(data['msg']);
				else location.href=data['return'];
			}
		});
		return false;
	});
	
	$('#frmlogo_cropp').click( function () { 
		$('#frmlogo_flag').val('2');
		cropp_function();
	});
	function cropp_function(){
		let size=$('#width_height_img').val().split('#'),photo_src=$('img','#fotoImg').attr('src').split('?');
		$('#Cropp2').css({width: size[0]+'px',height: size[1]+'px',marginLeft:'-'+(size[0]/2)+'px',marginTop:'-'+(size[1]/2)+'px'});
		$('#CroppFoto').css({width: size[0]+'px',height: size[1]+'px'});
		var croppicContainerPreloadOptions = {
			cropUrl:'/admin/croppimages',
			loadPicture:photo_src[0],
			enableMousescroll:true,
			loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
			onAfterImgCrop:function(){ 
				let s=Math.ceil(Date.now() / 1000);
				$("#fotoImg").html("<img src='"+photo_src[0]+"?"+s+"' width='100%; height: auto;' />");
				CroppFoto.destroy(); 
				$('#Cropp1').slideUp(150, function() {$("#CroppFoto").html('');} ); 
			},
			onReset:function(){ console.log('onReset') },
			onError:function(errormessage){ alert(errormessage); }
		}
		CroppFoto = new Croppic('CroppFoto', croppicContainerPreloadOptions);
		$('#CroppFoto').append("<input type='hidden' id='CroppFoto_token' name='_token' value='{{ csrf_token() }}' />");
		$('#Cropp1').slideDown(300);
	};

	$('#frmlogo_download').click(function(){
		$('#frmlogo_flag').val('1');
		$('#fotoInput').trigger('click');
	});
	$('#fotoInput').change(function(){
		$('#dialog').html('<p>Идет загрузка файла...</p>');
		$('#dialog').dialog('open');
		var o=$('#frmLogo'), formData = new FormData();
		formData.append('photo', document.getElementById('fotoInput').files[0]);
		formData.append('flag', 1);
		formData.append('_token', $('input[name=_token]',o).val());
		formData.append('parent', $('input[name=parent]',o).val());
		
		$.ajax({
			url: '/admin/croppimages',
			async: false,
			type: 'POST',
			contentType: false,
			processData: false,
			data: formData,
			success: function(data){
				if(!data['success']) alert(data['msg']);
				else{
					$('#fotoImg').html("<img src='"+data['msg']+"' />");
					$('#sysid_photo').val(data['id']);
					$('#frmlogo_cropp').css('display','block');
					$('#frmlogo_del').css('display','block');
					$('#dialog').dialog('close');
				}
			},
			error: function(o,err) { alert(err);}
		});
	});

	$('#frmlogo_del').click(function(e){ 
		e.stopPropagation();
		$('#frmlogo_flag').val('3');
		$( "#dialog-confirm" ).html('<p>Удалить <strong>Логотип</strong><p>');
		$( "#dialog-confirm" ).dialog({ buttons: { 
			Нет: function() { $(this).dialog('close'); },
			Да: function() { 
				let o=$('#frmLogo'), o1=o.serialize();
				$.post('/admin/croppimages',o1,function(data){
					if(!data['success']) alert(data['msg']);
					else{
						$('#fotoImg').html("");
						$('#sysid_photo').val('0');
						$('#frmlogo_cropp').css('display','none');
						$('#frmlogo_del').css('display','none');
						$('#dialog-confirm').dialog('close');
					}
				});
			}
		}});
		$('#dialog-confirm').dialog('open');
	});
	
	$('.active-categories').click( function(){
		$('.list-categories').toggleClass('active');
		$('.from-input-categories>input').val('').focus();
		$('.list-categories>ul').find('a').css('display','block');
	});
	$('#findcat').on('keyup',function(){
		var str = $(this).val().toLowerCase(), str1 ;
		$('li>a','.list-categories').each( function(){
			str1 = $(this).text().toLowerCase(); 
			nom = str1.indexOf(str);
			if( nom == -1 ) {
				$(this).css('display','none');
			} else {
				$(this).css('display','block');
			}
		});
	});
	$(document).click( function(e) { 
		if( $(e.target).closest(".select-categories").length ) {
			$(".list-brend").removeClass('active');
			$('.from-input-brend>input').val('');
			return;
		} else if ( $(e.target).closest(".select-brend").length ){
			$(".list-categories").removeClass('active');
			$('.from-input-categories>input').val('');
			return;
		} else {
			e.stopPropagation();
			$(".list-brend").removeClass('active');
			$('.from-input-brend>input').val('');
			$(".list-categories").removeClass('active');
			$('.from-input-categories>input').val('');
		}
	});
	$('li','.list-categories').on('click','a',function(){
		var $this = $(this), sid = $this.data('sid'), s = $this.text(), 
			ul = $this.closest('ul'), idrezult = ul.data('return');
		$(idrezult).val(sid);
		$('.active-categories').text(s);
		$(".list-categories").removeClass('active');
		$('.from-input-categories>input').val('');
	});
	
	$('.active-brend').click( function(){
		$('.list-brend').toggleClass('active');
		$('.from-input-brend>input').val('').focus();
		$('.list-brend>ul').find('a').css('display','block');
	});
	$('#findbrend').on('keyup',function(){
		var str = $(this).val().toLowerCase(), str1 ;
		$('li>a','.list-brend').each( function(){
			str1 = $(this).text().toLowerCase(); 
			nom = str1.indexOf(str);
			if( nom == -1 ) {
				$(this).css('display','none');
			} else {
				$(this).css('display','block');
			}
		});
	});
	$('li','.list-brend').on('click','a',function(){
		var $this = $(this),sid0=$this.data('parent'),sid1=$this.data('sid'),s = $this.text(), 
			ul=$this.closest('ul'),nameBrend = ul.prev('a'),idrezult = ul.data('return');
		if( sid0 !== undefined ){
			s = nameBrend.text() + ": " +s;
			$(idrezult).val(sid0+'#'+sid1);
		} else { $(idrezult).val(sid1+'#0'); }
		$('.active-brend').text(s);
		$(".list-brend").removeClass('active');
		$('.from-input-brend>input').val('');
	});

});
</script>
@endsection
