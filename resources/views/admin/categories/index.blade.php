<?php
use App\Http\Controllers\Admin\AdminGlobalController as General;
use App\Http\Controllers\Admin\AdminCategoriesController as Categories;
use Illuminate\Support\Facades\DB;

?>
@extends('admin.index')
@section('content')
	<?php
	$ss = "";
	$sysid = isset($MassParam['sysid'])?(int)$MassParam['sysid']:(int)$MassParam['id'];
	$ParentCat = !isset($MassParam['parent'])?(int)$MassParam['id']:(int)$MassParam['parent'];
	//Articles::VariablesArticles(General::$FeatureGlobalMenu,$sysid,$MassParam,$ParentCat);
	echo General::MenuArticles($MassParam, $sysid, $ParentCat, $url);
	Categories::VariablesCategories(General::$FeatureGlobalMenu,$sysid,$MassParam,$ParentCat);
	$kol = General::$FeatureGlobalMenu->firstKol;
	$result = DB::select("SELECT * FROM bis_categories WHERE parent=$ParentCat ORDER BY "
		.General::$FeatureGlobalMenu->sortRazdel);
	$ss .= "<ul id='sortableRazdel' class='sortableRazdel general-ul all-list' data-tables='bis_categories' data-sid='$sysid' data-first='$kol'>";
	foreach($result as $cat1){
		$cat=(array)$cat1;
		$sid = $cat['sysid']; 
		$nik = htmlspecialchars_decode($cat['name']); 
		$parent = $cat['parent'];

		if($cat['hide'] == 'show') { $glaz = "<span class='main-icon'><i class='fa fa-eye' aria-hidden='true' title='Скрыть'></i></span>";} 
		else { $glaz = "<span class='main-icon'><i class='fa fa-eye-slash' aria-hidden='true' title='Отобразить'></i></span>";}
		$ss .= "<li id='li$sid' sid='$sid'>";
			$ss .= "<div class='all-list-item'>";
				$ss .= "<div class='flex'>";
					$ss .= "<span class='main-icon sort' style='margin-right:3px'><i class='fa fa-arrows' aria-hidden='true' title='Сортировка'></i></span>";
					$ss .= "<span class='main-icon disclose'><strong class='ui-icon' title='Развернуть/Свернуть'></strong></span>";
				$ss .= "</div>";
				$ss .= General::PreviewLogoArticlesCategories($sid);
				$ss .= "<div class='all-list-item-edit'>".
					"<a href='/admin/content/sheet/?sysid=$sid&". http_build_query($MassParam) ."'>$nik</a>".
					"</div>";
				$ss .= "<div>";
					$ss .= "<div data-sid='$sid'>";
					$ss .= "<span title='Добавить подраздел' class='main-icon plus newPages' data-level='{$MassParam['id']}' "
						."data-parent='$sid' data-id='#li$sid>ul'><i class='fa fa-plus' aria-hidden='true'></i></span>";
						if(strpos(General::$FeatureGlobalMenu->setting_dop,"#inGlCat#") !== false) {
							$cl = ( $cat['inGl'] == 1 ) ? "current" : "";
							$ss .= "<span class='main-icon $cl xit' data-pole='inGl'><i class='fa fa-bolt' title='На главную'></i></span>";
						}
						if(strpos(General::$FeatureGlobalMenu->setting_dop,"#inNewCat#") !== false) {
							$cl = ( $cat['inNew'] == 1 ) ? "current" : "";
							$ss .= "<span class='main-icon $cl xit' title='Новинка' data-pole='inNew'><i class='fa fa-paper-plane'></i></span>";
						}
					$ss .="</div>";
					$ss .= "<div data-sid='$sid' data-name='$nik' data-parent='#li$sid'>";
						$ss .= $glaz;
						$ss .= "<span class='main-icon'><i class='fa fa-trash-o' aria-hidden='true' title='Удалить'></i></span>";
					$ss .="</div>";
				$ss .= "</div>";
			$ss .="</div>";
			Categories::$ChildrenMenu="";
			Categories::CategoriesAllModulChildren($sid,General::$FeatureGlobalMenu->sortRazdel);
			$ss .= Categories::$ChildrenMenu;
		$ss .= "</li>";
	}
	$ss .= "</ul>";
	echo $ss;
	?>
<script>
var csrf_token = "{{ csrf_token() }}",
	has='<?= "nav={$MassParam['nav']}&id={$MassParam['id']}";?>',
	GlobalMenu='<?= General::$FeatureGlobalMenu->setting_dop;?>';
$(function() {
	$(document).on('click','.newPages',function(){
		let $this = $(this), level = $this.data('level'), parent = $this.data('parent'), papaSid = $this.data('id');
		if(papaSid === undefined) { papaSid = '#sortableRazdel'; }
		let s = "<p><input type='text' class='input' name='namemodul' id='namemodul' placeholder='Название' /></p>";
		s += "<p class='axtung'>Обязательно для заполнения!</p>";
		$( "#dialog-form" ).html(s);
		$("#dialog-form").dialog({ buttons: { 
			Сохранить: function() {
				$('#namemodul').removeClass("ui-state-error");
				var name = $('#namemodul').val(), nameT = $('#nameT').val();
				if( name.length < 1 ) { 
					$('#namemodul').addClass("ui-state-error");
					$('#namemodul').focus();
				} else {
					$.post('/admin/ajaxfull/categories',
					{flag:2,has:has,parent:parent,name:name,level:level,setting:GlobalMenu,'_token':csrf_token},
					function(data){
						$(papaSid).prepend(data);
						$("#dialog-form").dialog( "close" );
					});
				}
			},
			Отмена: function() { $("#dialog-form").dialog( "close" ); }
		}});
		$("#dialog-form").dialog( "open" );
		$( '#datepickerM').datepicker({ firstDay:1, dateFormat:'dd.mm.yy', changeMonth: true, changeYear: true });
	});
});
</script>
@endsection
