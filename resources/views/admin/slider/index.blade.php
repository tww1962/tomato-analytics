<?php
use App\Http\Controllers\Admin\AdminGlobalController as General;
use App\Http\Controllers\Admin\AdminUploadController as Photo;
?>
@extends('admin.index')
@section('content')
	<?php 
	$sysid = isset($MassParam['sysid'])?(int)$MassParam['sysid']:(int)$MassParam['id']; 
	echo General::MenuSheet($MassParam,$sysid,$url);
	if( $sysid < 1000 ){ 
		$FotoWidth = explode("#",General::$FeatureGlobalMenu->size_slider_pages);
	} elseif( $sysid < 5000 ){
		$FotoWidth = explode("#",General::$FeatureGlobalMenu->size_slider_cat);
	} else {
		$FotoWidth = explode("#",General::$FeatureGlobalMenu->size_slider_list);
	}
	$size_kol=[config('photoupload.SIZE_MAX_SLIDER')/1024/1024,config('photoupload.KOLMAXFILE_SLIDER')];
	?>
	<p class='message'><span>Максимальный размер фотографии {{ $size_kol[0] }}М. Разовая загрузка не более {{ $size_kol[1] }} файлов!</span><br />
		<span>Рекомендуемы размер фото: ширина {{ $FotoWidth[0] }}рх, высота {{ $FotoWidth[1] }}px, разрешение 100dpi.</span><br />
		<span>Если ширина больше {{ $FotoWidth[0] }}рх, ширина автоматически будет уменьшина до {{ $FotoWidth[0] }}рх, высота пропорционально ширине</span>
	</p>
		
	<form name='newfoto' id='newfoto' enctype='multipart/form-data' method='POST' action='/admin/ajaxuploadfile/slider-photo'>
		<p style='text-align:center'>
			<input type='file' name='foto[]' id='kolFile' multiple accept='image/png,image/jpeg,image/webp' onchange='newFoto("{{ $size_kol[1] }}")' />
		</p>
		<input type='hidden' name='parent' value='{{ $sysid }}'/>
		<input type='hidden' name='width' value='{{ $FotoWidth[0] }}'/>
		<input type='hidden' name='height' value='{{ $FotoWidth[1] }}'/>
		@csrf
	</form>
	<?= Photo::SliderDann($FotoWidth,$sysid); ?>
<script>
var csrf_token = "{{ csrf_token() }}";
$( function() {
	$('form','.slider-list').submit(function(){
		let o=$(this),o1=o.serialize() + "&_token="+csrf_token;
		$.post('/admin/ajaxuploadfile/slider-info',o1,function(data){
			if(!data['success']) alert(data['msg']);
			else $('#sp'+data['id']).text('Записано');
		});
		return false;
	});
});
</script>
@endsection
