<?php
use App\Http\Controllers\Admin\AdminGlobalController as General;
use App\Http\Controllers\Admin\AdminYoutubeController as Youtube;
?>
@extends('admin.index')
@section('content')
	<?php 
	$sysid = isset($MassParam['sysid'])?(int)$MassParam['sysid']:(int)$MassParam['id']; 
	echo General::MenuSheet($MassParam,$sysid,$url);
	?>
	<p style='padding-bottom: 15px;'><button class='button' onclick='newYoutube({{ $sysid }})'>Добавить видео</button></p>
	<?= Youtube::YoutubeList($sysid);?>
<script>
var csrf_token = "{{ csrf_token() }}";
$( function() {
	$('.filesupload').on('click','.fa-pencil',function(){
		var $this = $(this), papa = $this.closest('div'), sid = papa.data('sid'), name = papa.attr('name'), div = papa.data('parent');
		var s = "<p><input type='text' class='input' name='name' id='name' data-sid='"+sid+"' value='" + name + "' /></p>";
		$( "#dialog-form" ).html(s);
		$("#dialog-form").dialog({ buttons: { 
			Сохранить: function() {
				name = $('#name').val();
				if( name.length < 1 ) { 
					$('#name').addClass("ui-state-error");
					$('#name').focus();
					return false;
				} else {
					$.post('/admin/ajaxyoutube/youtube-name',{sid:sid, name:name,'_token':csrf_token}, function(data){
						if(!data['success']) { alert(data['msg']); return false;}
						$(div).text(name);
						papa.attr('name',name);
						$("#dialog-form").dialog( "close" );
					});
				}
			},
			Отмена: function() { $("#dialog-form").dialog( "close" ); }
		}});
		$("#dialog-form").dialog( "open" );
	});
	
	$('.filesupload').on('click','.fa-youtube',function(){
		var $v = $(this).data('href'), s;
		s = "<div class='myvideo'><div class='myvideo-for-ifarme'>";
		s += "<iframe allowfullscreen='1' frameborder='0' height='315' src='"+$v+"' width='560'></iframe>";
		s += "</div></div>";
		modalSity(s);
	});
});
function newYoutube(parent){
	var s = "<p><input type='text' class='input' name='namemodul' id='namemodul' placeholder='Название' /></p>";
	s += "<p style='padding: 7px 0'><input type='text' class='input' name='nameYoutube' id='nameYoutube' placeholder='Ссылка видео с Youtube' /></p>";
	s += "<p class='axtung'>Все поля обязательны для заполнения !</p>";

	$("#dialog-form" ).html(s);
	$("#dialog-form").dialog({ buttons: { 
		Сохранить: function() {
			$('#namemodul').removeClass("ui-state-error");
			$('#nameYoutube').removeClass("ui-state-error");
			var name = $('#namemodul').val(), nameT = $('#nameT').val(),
				nameY = $.trim($('#nameYoutube').val()), b = nameY.match(/v=([^&$]+)/i);
			if( name.length < 1 ) { 
				$('#namemodul').addClass("ui-state-error");
				$('#namemodul').focus();
			} else if( null == b || "" == b || "" == b[0] || "" == b[1] )  { 
				$('#nameYoutube').addClass("ui-state-error");
				$('#nameYoutube').focus();
			} else {
				$.post('/admin/ajaxyoutube/youtube-new',{parent:parent, name:name, nameY: b[1],'_token':csrf_token}, function(data){
					if(!data['success']) { alert(data['msg']); return false;}
					window.location.reload(true);
				});
			}
		},
		Отмена: function() { $("#dialog-form").dialog( "close" ); }
	}});
	$("#dialog-form").dialog( "open" );
}


</script>
@endsection
