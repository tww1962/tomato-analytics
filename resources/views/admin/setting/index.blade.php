<?php
use App\Http\Controllers\Admin\AdminSettingController as Setting;
?>
@extends('admin.index')
@section('content')
	<?php
	$ss = "";
	if( $MassParam['type'] == "dann" ) {
		$ss .= "<p class='name-pages'>Глобальные настройки сайта</p>";
		$ss .= Setting::SettingSity($MassParam);
	} elseif ( $MassParam['type'] == "stop" ) {
		$ss .= "<p class='name-pages'>Профилактика</p>";
		$ss .= Setting::SettingStop($MassParam);
	} elseif ( $MassParam['type'] == "err404" ) {
		$ss .= "<p class='name-pages'>Error 404 (страница не существует)</p>";
		$ss .= Setting::SettingErr();
	} elseif ( $MassParam['type'] == "menu" ) {
		$ss .= "<p class='name-pages'>Меню сайта</p>";
		$ss .= Setting::SettingMenu();
	} elseif ( $MassParam['type'] == "anysetting" ) {
		$ss .= "<p class='name-pages'>Цена билета</p>";
		$ss .= Setting::SettingAny((int)$MassParam['id']);
	} elseif ( $MassParam['type'] == "micromarking" ) {
		$ss .= "<p><strong>Используя семантическую разметку, вы можете улучшить представление сниппета вашего сайта в результатах поиска.</strong></p>";
		$ss .= Setting::SettingMicro((int)$MassParam['id']);
	} elseif ( $MassParam['type'] == "seo" ) {
		$ss .= "<p class='name-pages'>Для SEO специалиста</p>";
		$ss .= Setting::Seo();
	} else exit;
	echo $ss;
	?>
<script>
var csrf_token = "{{ csrf_token() }}";
$( function() {
	$('#frmDannStop').submit(function(){
		let o=$(this),o1=o.serialize();
		$.post('/admin/ajaxsetting/stop',o1,function(data){
			if(!data['success']) alert(data['msg']);
			else {
				if(data['return'].length<1) MessageAct(data['msg']);
				else location.href=data['return'];
			}
		});
		return false;
	});

});
</script>
@endsection
