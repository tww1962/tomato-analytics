<?php
use App\Http\Controllers\Admin\AdminGlobalController as General;
use App\Http\Controllers\Admin\AdminUploadController as Photo;
?>
@extends('admin.index')
@section('content')
	<?php 
	$sysid = isset($MassParam['sysid'])?(int)$MassParam['sysid']:(int)$MassParam['id']; 
	echo General::MenuSheet($MassParam,$sysid,$url);
	?>
	<p class='message'>Максимальный размер фотографии {{ config('photoupload.SIZE_MAX')/1024/1024 }}М. Разовая загрузка не более {{ config('photoupload.KOLMAXFILE') }} файлов!</p>
	<form name='newfoto' id='newfoto' enctype='multipart/form-data' action='/admin/ajaxuploadfile/photogallery' method='POST'>
		<p style='text-align:center'>
			<input type='file' name='foto[]' id='kolFile' multiple accept='image/png,image/jpeg,image/webp' onchange="newFoto({{ config('photoupload.KOLMAXFILE') }})" />
		</p>
		<input type='hidden' name='parent' value="{{ $sysid }}"/>
		@csrf
	</form>
	<?php echo Photo::PhotogalDann($sysid);?>
<script>
var csrf_token = "{{ csrf_token() }}";
$( function() {

$('form','.photo-list').submit(function(){
	let o=$(this),o1=o.serialize() + "&_token="+csrf_token;
	$.post('/admin/ajaxuploadfile/photogallery-name',o1,function(data){
		if(!data['success']) alert(data['msg']);
		else $('#sp'+data['id']).text('Записано');
	});
	return false;
});
$('.actions-choose').on('click','#choose',function(e){
		var $this = $(this), ch = ( $this.is(':checked') ) ? true : false;
		$("input[type='checkbox']",'.photo-list').each( function(){
			$(this).prop("checked",ch);
		});
});

$('.actions-choose').on('change','select',function(e){
	var $this = $(this), $v = 1 * $this.val(), sid = new Array, i = 0, msg;
	if( $v == 0 ) return false;
	$("input[type='checkbox']:checked",'.photo-list').each( function(){
		sid[i] = $(this).data('sid');
		i++;
	});
	if( i == 0 ) { 
		$this.val('0');
		alert("Нет данных для выполнения действия"); 
		return false; 
	}
	if( $v == 1 ) { 
		msg = 'Отобразить ' + i + ' фотогоафий';
	} else if ( $v == 2 ) { 
		msg = 'Скрыть ' + i + ' фотогоафий';
	} else {
		msg = 'Удалить ' + i + ' фотогоафий';
	}
	var s = sid.join(',');
	console.log(s);
	$( "#dialog-confirm" ).html("<h3>" + msg + " ?</h3>");
	$( "#dialog-confirm" ).dialog({ buttons: { 
		Нет: function() { $( "#dialog-confirm" ).dialog('close'); },
		Да: function() {
			$.post('/admin/ajaxuploadfile/photogallery-bulk-select',{m:s,vid:$v,'_token':csrf_token},function(data){ 
				if(!data['success']) alert(data['msg']);
				else window.location.reload(true);
			}); 
		}
	}});
	$('#dialog-confirm').dialog('open');
});

$('.photo-list').on('click','.turn',function(e){
		e.stopPropagation();
		e.preventDefault();
		var $this = $(this), sid = $this.data('sid'), photo = $this.data('photo');
		$.post('/admin/ajaxuploadfile/photogallery-turn',{sid:sid, photo:photo,'_token':csrf_token},function(data){ 
			if(!data['success']) alert(data['msg']);
			else $('#photo'+sid).css('backgroundImage', 'url('+data['msg']+')');
		});
	});

});
</script>
@endsection
