<?php
use App\Http\Controllers\Admin\AdminGlobalController as General;
use App\Http\Controllers\Admin\AdminUploadController as Photo;
?>
@extends('admin.index')
@section('content')
	<?php 
	$sysid = isset($MassParam['sysid'])?(int)$MassParam['sysid']:(int)$MassParam['id']; 
	echo General::MenuSheet($MassParam,$sysid,$url);
	?>
	<p class='message'>Убедительно рекомендуем архивировать файлы перед загрузкой.</p>
	<form name='newfoto' id='newfoto' class='newfiles' enctype='multipart/form-data' action='/admin/ajaxuploadfile/doc-files' method='POST'>
		<p><sub class='zvezda'>*</sub>Название:</p>
		<input type='text' name='name' value='' class='input' id='nameFile' required /></p>
		<p style='padding: 15px 0 0'>
			<input type='file' name='foto' id='kolFile' required />&nbsp;&nbsp;&nbsp;&nbsp;<input type='button' class='button' value='Сохранить' onclick='newFoto(1)' />
		</p>
		<input type='hidden' name='parent' value='{{ $sysid }}'/>
		@csrf
	</form>
	<?= Photo::DocFilesDann($sysid);?>
<script>
var csrf_token = "{{ csrf_token() }}";
$( function() {
	$('form','.slider-list').submit(function(){
		let o=$(this),o1=o.serialize() + "&_token="+csrf_token;
		$.post('/admin/ajaxuploadfile/slider-info',o1,function(data){
			if(!data['success']) alert(data['msg']);
			else $('#sp'+data['id']).text('Записано');
		});
		return false;
	});
	$('.filesupload').on('click','.fa-pencil',function(){
		var $this = $(this), papa = $this.closest('div'), sid = papa.data('sid'), name = papa.attr('name'), div = papa.data('parent');
		var s = "<p><input type='text' class='input' name='name' id='name' data-sid='"+sid+"' value='" + name + "' /></p>";
		$( "#dialog-form" ).html(s);
		$("#dialog-form").dialog({ buttons: { 
			Сохранить: function() {
				name = $('#name').val();
				if( name.length < 1 ) { 
					$('#name').addClass("ui-state-error");
					$('#name').focus();
					return false;
				} else {
					$.post('/admin/ajaxuploadfile/doc-name',{sid:sid, name:name,'_token':csrf_token}, function(data){
						if(data['success']){
							$(div).text(data['msg']);
							papa.attr('name',data['msg']);
							$("#dialog-form").dialog( "close" );
						}
					});
				}
			},
			Отмена: function() { $("#dialog-form").dialog( "close" ); }
		}});
		$("#dialog-form").dialog( "open" );
	});

});
</script>
@endsection
