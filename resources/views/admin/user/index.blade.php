<?php
use App\Http\Controllers\Admin\AdministratorController as UserAdmin;
?>
@extends('admin.index')
@section('content')
	<?php
	$ss = "";
	if( !isset($MassParam['edit']) ){
		$ss .= "<p style='padding-bottom: 7px;'><a href='/admin/content/user?edit=0&"
			.http_build_query($MassParam) ."' class='button'>Добавить администратора</a></p>";
		$ss .= UserAdmin::RoleUserDann($MassParam);
	}else{
		$idUser = (int)$MassParam['edit'];
		unset($MassParam['edit']);
		$ss .= "<p class='name-pages'>";
			$ss .= ( $idUser == 0 ) ? "Новый администратор" : "Корректировка данных администратора";
		$ss .= "</p>";
		$ss .= UserAdmin::RoleUserDopKorr( $idUser,$MassParam );
	}
	echo $ss;
	?>
<script>
var csrf_token = "{{ csrf_token() }}";
$( function() {
	$('.administrator').on('click','.role', function(){
		var s = $(this), s1 = s.val(), s2 = s.closest('li'), s3 = s.closest('ul');
		if( s.hasClass('children') ) { 
			if ( s.is(':checked') ){
				s3.closest('li').find('.parent-role').prop('checked',true);
				console.log( $(this).val() );
			}
		} else {
			if ( s.is(':checked') ){
				$('ul',s2).find('.children').prop('checked',true);
			} else {
				$('ul',s2).find('.children').prop('checked',false);
			}
		}
		return true;
	});
	$('#frmUser').submit(function(){
		let o=$(this),o1=o.serialize();
		$.post('/admin/ajaxadministrator',o1,function(data){
			if(!data['success']) alert(data['msg']);
			else {
				if(data['return'].length<1){
					MessageAct(data['msg']);
					$('#'+data['idUser']).val(data['idUser']);
				} else location.href=data['return'];
			}
		});
		return false;
	});
});
</script>
@endsection
