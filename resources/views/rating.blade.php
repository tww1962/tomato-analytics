<?php
use App\Http\Controllers\HomeController as General;
use App\Http\Controllers\RatingController as Main;
$checked = $courier = "";
$d1 = $d2 = date('d.m.Y');
if(!is_null(session('param'))){
    $param = json_decode(session('param'),true);
    if(is_array($param['point_multi'])) $checked=$param['point_multi'][0];
    else $checked=$param['point_multi'];
    $d1 = $param['date1'];
    $d2 = $param['date2'];
    $courier = isset($param['courier']) ? $param['courier'] : "";
}
?>
@extends('index')
@section('content')
<header class='flex rating'>
    <section class='header-left'>
        <h1 class='namePages'>{{ General::DecodeTitle($cmsmenu['nameh1']) }}</h1>
        <form id='analinik_form' method='POST'>
            <div class='delivery-point flex flex-wrap flex-align-center'>
                <span><strong>Точки продаж:</strong></span>
                <select name='point_multi' id='list_full_point'>
                    <option value=''>Выбрать</option>
                    @foreach ($points as $point)
                    <?php $check = $point->Id==$checked ? 'selected' : ''; ?>
                    <option value='{{ $point->Id }}' {{ $check }} >{{ General::DecodeTitle($point->OrganizationName) }}</option>
                    @endforeach
                </select>
            </div>
            <section class='period-filter-submit  flex flex-align-center'>
                <div class='delivery-period flex flex-align-center'>
                    <label>Период:</label>
                    <span>с</span><div class='delivery-date1'>
                        <input type='text' readonly name='date1' id='delivery_date1' value="{{ $d1 }}" />
                    </div>
                    <span>по</span><div class='delivery-date2'>
                        <input type='text' readonly name='date2' id='delivery_date2' value="{{ $d2 }}" />
                    </div>
                </div>
                <div class='rating-courier'>
                    {!! Main::Rating_Courier_List($checked,$d1,$d2,$courier) !!}
                </div>
                <div class='rating-reload'>Получить данные</div>
            </section>
            @if(empty($checked))
                <p><strong style='font-style:italic;color:#F52341'>Для получения списка курьеров укажите период и точку продаж</strong></p>
            @endif
            @csrf
        </form>
    </section>
    <section class='header-right'>{!! General::User($user) !!}</section>
</header>
@if(!empty($checked))
<div class='rating-rezult flex'>
    <div class='rating-rezult-left'>
        <div class='rating-tr_head'>
			<div class='rating-tr'>
				<div class='rating-td'>Дата</div>
				<div class='rating-td'>Курьер</div>
				<div class='rating-td'>Количсетво<br />заказов</div>
				<div class='rating-td'>Скорость</div>
			</div>
		</div>
        {!! Main::Rating_Left_Courier($checked,$d1,$d2,$courier) !!}
    </div>
    <div class='rating-rezult-right'>
        <div class='rating-top'><div>ТОП курьеров</div>
        {!! Main::Rating_Rezult_Right($checked,$d1,$d2,$courier) !!}
        </div>
    </div>
</div>
@endif
@endsection