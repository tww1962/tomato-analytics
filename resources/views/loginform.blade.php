<?php
use App\Http\Controllers\HomeController as General;
?>
@extends('index')
@section('content')
<section class='content'>
<header class='home-header flex flex-align-center'>
	<div><img src='/img/logo.png?<?= filemtime("./img/logo.png");?>"' alt='Аналитика ТиЧ' /></div>
</header>
<div class='home-content flex'>
	<div class='home-content_left flex flex-align-center flex-justify-center'>
		<form class='home-form'>
			<div class='home-form_name'>Вход<br />для персонала</div>
			<div class='home-form_input'>
				<label>Логин</label><br />
				<input type='text' name='name' required placeholder='Введите логин' />
			</div>
			<div class='home-form_input for-password'><span></span>
				<label>Пароль</label><br />
				<input type='password' name='password' required placeholder='Введите пароль' />
			</div>
			<input type='submit' value='Войти в систему' />
			<?php /*
			<div class='for-capt'>
				<div class='capt' id='capt'>
					<div class='capt-drag flex flex-justify-center flex-align-center' id='draggable'>
					<span class='icon-angle-double-right'></span></div>
					<div id='droppable' class='capt-drop'></div>
				</div>
				<p>для отправки переместите кнопку вправо</p>
			</div>
			*/ ?>
			@csrf
		</form>
	</div>
	<div class='home-content_right' style='{{ General::Background(1) }}'></div>
</div>
</section>
@endsection
