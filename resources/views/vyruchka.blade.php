<?php
use App\Http\Controllers\HomeController as General;
use App\Http\Controllers\VyruchkaController as Main;
$checked = "";
$d1 = $d2 = date('d.m.Y');
if(!is_null(session('param'))){
    $param = json_decode(session('param'),true);
    if(!is_array($param['point_multi'])) $checked=[$param['point_multi']];
    else $checked=$param['point_multi'];
    $d1 = $param['date1'];
    $d2 = $param['date2'];
}
if(count($user_points)>0) $checked = $user_points;
?>
@extends('index')
@section('content')
<header class='flex proceeds' style='margin-bottom:30px'>
    <section class='header-left'>
        <h1 class='namePages'>{{ General::DecodeTitle($cmsmenu['nameh1']) }}</h1>
        <form id='analinik_form' method='POST'>
            <div class='delivery-point flex flex-wrap flex-align-center'>
            <p style='width:100%'><strong>Точки продаж:</strong></p>
                @foreach ($points as $point)
                <?php $check = in_array($point->Id,$checked) ? 'checked' : ''; ?>
                <div class='flex flex-align-center'>
                    <input type='checkbox' name='point_multi[]' value='{{ $point->Id }}' id='multi_point_{{ $point->sysid }}' {{ $check }} />
                    <label for='multi_point_{{ $point->sysid }}'>{{ General::DecodeTitle($point->OrganizationName) }}</label>
                </div>
                @endforeach
                <?php /*
                <span><strong>Точки продаж:</strong></span>
                <select name='point_multi' id='list_full_point'>
                    <option value=''>Выбрать</option>
                    @foreach ($points as $point)
                    <?php $check = $point->Id==$checked ? 'selected' : ''; ?>
                    <option value='{{ $point->Id }}' {{ $check }} >{{ General::DecodeTitle($point->OrganizationName) }}</option>
                    @endforeach
                </select>
                */ ?>
            </div>
            <section class='period-filter-submit  flex flex-align-center'>
                <div class='delivery-period flex flex-align-center'>
                    <label>Период:</label>
                    <span>с</span><div class='delivery-date1'>
                        <input type='text' readonly name='date1' id='delivery_date1' value="{{ $d1 }}" />
                    </div>
                    <span>по</span><div class='delivery-date2'>
                        <input type='text' readonly name='date2' id='delivery_date2' value="{{ $d2 }}" />
                    </div>
                </div>
                <div class='proceeds-reload'>Получить данные</div>
            </section>
            @csrf
        </form>
    </section>
    <section class='header-right'>{!! General::User($user) !!}</section>
</header>
<div class='proceeds-tr proceeds-tr_head'>
    <div class='proceeds-td'>Период</div>
    <div class='proceeds-td'>Выручка</div>
    <div class='proceeds-td'>Доставка</div>
    <div class='proceeds-td'>Самовывоз</div>
    <div class='proceeds-td'>Ресторан</div>
</div>
<?php
if(isset($param)) echo Main::show($param,$checked);
?>
@endsection