<?php
use App\Http\Controllers\HomeController as General;
use App\Http\Controllers\AnalyticsController as Main;
$checked = "";
$d1 = $d2 = date('d.m.Y');
if(!is_null(session('param'))){
    $param = json_decode(session('param'),true);
    if(is_array($param['point_multi'])) $checked=$param['point_multi'][0];
    else $checked=$param['point_multi'];
    $d1 = $param['date1'];
    $d2 = $param['date2'];
}
?>
@extends('index')
@section('content')
<header class='flex analinik'>
    <section class='header-left'>
        <h1 class='namePages'>{{ General::DecodeTitle($cmsmenu['nameh1']) }}</h1>
        <form id='analinik_form'>
            <div class='delivery-point flex flex-wrap flex-align-center'>
                <p style='width:100%'><strong>Точки продаж:</strong></p>
                @foreach ($points as $point)
                <?php $check = $point->Id==$checked ? 'checked' : ''; ?>
                <div class='flex flex-align-center'>
                    <input type='checkbox' name='multi_point[]' value='{{ $point->Id }}' id='multi_point_{{ $point->sysid }}' {{ $check }} />
                    <label for='multi_point_{{ $point->sysid }}'>{{ General::DecodeTitle($point->OrganizationName) }}</label>
                </div>
                @endforeach
            </div>
            <section class='period-filter-submit  flex flex-align-center'>
                <div class='delivery-period flex flex-align-center'>
                    <label>Период:</label>
                    <span>с</span><div class='delivery-date1'>
                        <input type='text' readonly name='date1' id='delivery_date1' value="{{ $d1 }}" />
                    </div>
                    <span>по</span><div class='delivery-date2'>
                        <input type='text' readonly name='date2' id='delivery_date2' value="{{ $d2 }}" />
                    </div>
                </div>
                <div class='analitik-reload'>Получить данные</div>
            </section>
        </form>
    </section>
    <section class='header-right'>{!! General::User($user) !!}</section>
</header>
<div class='analitik-rezult flex flex-wrap analitik-rezult-ajax'>
    <?php if(isset($param)) echo Main::show_reload($param); ?>
</div>
<div class='analitik-rezult' style='padding:30px 10px 0'>{!! Main::accumulatively((array)$points) !!}</div>

@endsection