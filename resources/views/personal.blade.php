<?php
use App\Http\Controllers\HomeController as General;
use App\Http\Controllers\PersonalController as Main;
?>
@extends('index')
@section('content')
<h1 class='namePages persona'>{{ General::DecodeTitle($cmsmenu['nameh1']) }}</h1>
<section class='persona-menu flex'>
    <span class='persona-menu_ active' data-sid='1'>Личные данные</span>
    <span class='persona-menu_' data-sid='2'>Аватарка</span>
    <span class='persona-menu_' data-sid='3'>Сменить пароль</span>
</section>

<div class='persona-for-form' id='form1'>
    <form class='flex flex-wrap person-dann' id='f1'>
        <h2>Ваш логин: {{ $user->email }}</h2>
        <div class='person-dann-left'>
            <div><label>Имя:<strong class='zvezda'>*</strong></label><br />
                <input type='text' name='name' value="{{ General::DecodeTitle($user->name) }}" required />
            </div>
            <div><label>Фамилия:<strong class='zvezda'>*</strong></label><br />
                <input type='text' name='surname' value="{{ General::DecodeTitle($user->surname) }}" required />
            </div>
            <div><label>Роль:<strong class='zvezda'>*</strong></label><br />
                <input type='text' name='nik' value="{{ General::DecodeTitle($user->nik) }}" required />
            </div>
        </div>
        <div class='person-dann-right'>
            <div><label>Телефон:</label><br />
                <input type='tel' name='phone' value="{{ $user->phone }}" style='width:130px' />
            </div>
            <div><label>Дата рождения:</label><br />
                <input type='text' style='width:130px;text-align:center;padding:0' id='persona_datepicker' name='bdate' value=" {{ $user->bdate }}" readonly />
            </div>
            <?php
            $m = [0 => "", 1 => "", 2 => ""];
            $m[$user->sex] = "checked";
            ?>
            <div>
                <div class='flex flex-align-center'>
                    <span style='margin-right:10px;'>Пол:&nbsp;</span>
                    <span style='margin-right:10px;'>"
                        <input name='sex' value='1' id='men' type='radio' {{ $m[1] }} />
                        <label for='men'>Мужской</label>
                    </span>
                    <span style='margin-right:10px;'>
                        <input name='sex' value='2' id='women' type='radio' {{ $m[2] }} />
                        <label for='women'>Женский</label>
                    </span>
                    <span>
                        <input name='sex' value='0' id='nosex' type='radio' {{ $m[0] }} />
                        <label for='nosex'>Неважно</label>"
                    </span>
                </div>
            </div>
        </div>
        <input type='hidden' name='flagMain' value='851' />
        <input type='hidden' name='sid' value="{{ $user->sysid }}" />
        <div class='for-submit flex flex-align-center flex-justify-center'><button type='submit'>Сохранить</button></div>
        @csrf
    </form>
</div>

<?php
$wh = [165, 165];
$photo = Main::Photo($user->sysid, $wh);
?>
<div class='persona-for-form' id='form2' style='display:none'>
    <form name='frmLogo' enctype='multipart/form-data' id='f2'>
        <input style='display:none' type='file' accept='image/png, image/jpeg' name='foto' id='fotoInput' />
        <p>Аватарка (рекомендуемый размер {{ $wh[0] }} px на {{ $wh[1] }} px)</p>
        <p>&nbsp;</p>
        <div id='fotoImg' class='persona-logo' style="width:{{ $wh[0] }}px; height:{{ $wh[1] }}px;" data-w="{{ $wh[0] }}" data-h="{{ $wh[1] }}">
            {!! $photo !!}
            <span onclick="$('#fotoInput').trigger('click');"><span>Загрузить фото</span></span>
        </div>
        <input type='hidden' name='sid' value="{{ $user->sysid }}" />
        <input type='hidden' name='flagMain' value='852' />
        @csrf
    </form>
</div>

<div class='persona-for-form' id='form3' style='display:none'>
    <form class='flex flex-wrap person-passwod' id='f3'>
        <div class='person-dann-left'>
            <div>
                <label>Текущий пароль:<strong class='zvezda'>*</strong></label><br />
                <input type='password' name='pass' required />
            </div>
            <div>
                <label>Новый пароль:<strong class='zvezda'>*</strong></label><br />
                <input type='password' name='pass1' required />
            </div>
            <div>
                <label>Повторите новый пароль:<strong class='zvezda'>*</strong></label><br />
                <input type='password' name='pass2' required />
            </div>
        </div>
        <div class='for-submit flex flex-align-center flex-justify-center'><button type='submit'>Сохранить</button></div>
        <input type='hidden' name='flagMain' value='853' /><input type='hidden' name='sid' value="{{ $user->sysid }}" />
        @csrf
    </form>
</div>
@endsection